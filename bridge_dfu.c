/*
 * Copyright (C) 2016 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "bridge.h"
#include "command.h"

int mbridge_dfu_init(struct usb_composite_dev *cdev, struct usb_function **interface,
	mbridge_usb_disconnect *disconn_cb);
void mbridge_dfu_destroy(struct usb_function *interface);

//----------------------------------------------------------------------------
// DFU mode
// EP4(intf0) - MORPHEUS_COMMAND_ENDPOINT
// EP5(intf1) - MORPHEUS_DFU_ENDPOINT

struct usb_string strings_dev_dfu[] = {
	[STRING_MANUFACTURER_IDX].s  = MORPHEUS_MANUFACTURER,
	[STRING_PRODUCT_IDX].s       = MORPHEUS_PRODUCT,
	[STRING_DFU_COMMAND_IDX].s   = MORPHEUS_COMMAND_INTERFACE_NAME,
	[STRING_DFU_IDX].s           = MORPHEUS_DFU_INTERFACE_NAME,
	{ }
};

static struct usb_gadget_strings stringtab_dev_dfu = {
	.language = 0x0409, /* en-us */
	.strings  = strings_dev_dfu,
};

static struct usb_gadget_strings *dev_strings_dfu[] = {
	&stringtab_dev_dfu,
	NULL,
};

static struct usb_device_descriptor device_desc_dfu = {
	.bLength             = sizeof (device_desc_dfu),
	.bDescriptorType     = USB_DT_DEVICE,
	.bcdUSB              = cpu_to_le16(0x0200), /* USB 2.0 */
	.bDeviceClass        = USB_CLASS_PER_INTERFACE,
	.idVendor            = cpu_to_le16(MORPHEUS_VENDOR_ID),
	.idProduct           = cpu_to_le16(MORPHEUS_DFU_PRODUCT_ID),
	// bcdDevice is ignored by composite framework.
	// See update_unchanged_dev_desc()
	//.bcdDevice           = cpu_to_le16(MORPHEUS_DFU_BCDDEVICE),
	.bNumConfigurations  = 1,
};

static const struct usb_composite_overwrite covr = {
	.bcdDevice           = cpu_to_le16(MORPHEUS_DFU_BCDDEVICE),
};

static struct usb_configuration config_dfu = {
	.label               = "DFU Configuration",
	.strings             = dev_strings_dfu,
	.bConfigurationValue = 1,
	.bmAttributes        = USB_CONFIG_ATT_SELFPOWER,
};

static mbridge_usb_disconnect g_disconnect_cbs[MORPHEUS_NUM_DFU_INTERFACES];

static int interfaces_init(struct usb_composite_dev *cdev, struct usb_function **interfaces,
			   unsigned int num_interfaces)
{
	struct usb_function *cmd_interface = NULL;
	struct usb_function *dfu_interface = NULL;
	int i, ret;

	GADGET_LOG(LOG_DBG, "initializing interfaces...\n");

	i = 0;
	ret = mbridge_cmd_init(cdev, &cmd_interface, &g_disconnect_cbs[i], NULL);
	if (ret) {
		GADGET_LOG(LOG_ERR, "failed to initialize hid interface: %d\n", ret);
		goto fail_init;
	}
	interfaces[i++] = cmd_interface;

	ret = mbridge_dfu_init(cdev, &dfu_interface, &g_disconnect_cbs[i]);
	if (ret) {
		GADGET_LOG(LOG_ERR, "failed to initialize dfu interface: %d\n", ret);
		goto fail_init;
	}
	interfaces[i++] = dfu_interface;

	return i;

fail_init:
	if (dfu_interface)
		mbridge_dfu_destroy(dfu_interface);
	if (cmd_interface)
		mbridge_cmd_destroy(cmd_interface);
	return ret;
}

static void interfaces_destroy_one(struct usb_function *interface)
{
	struct usb_interface_descriptor *desc;
	desc = (struct usb_interface_descriptor *)interface->fs_descriptors[0];

	GADGET_LOG(LOG_DBG, "destroying interface %d...\n", desc->bInterfaceNumber);

	switch (desc->bInterfaceNumber)
	{
	case MORPHEUS_DFU_COMMAND_INTERFACE:
		mbridge_cmd_destroy(interface);
		break;
	case MORPHEUS_DFU_INTERFACE:
		mbridge_dfu_destroy(interface);
		break;
	default:
		ASSERT(0);
		GADGET_LOG(LOG_ERR, "unknown interface. %d\n", desc->bInterfaceNumber);
		break;
	}
}

//----------------------------------------------------------------------------
static int mbridge_bind_config(struct usb_configuration *c)
{
	struct usb_function *interfaces[MAX_CONFIG_INTERFACES];
	int num_interfaces, i, ret;

	GADGET_LOG(LOG_DBG, "initializing configuration...\n");

	memset(interfaces, 0, sizeof (interfaces));

	/* Initialize the descriptors for all interfaces. */
	num_interfaces = interfaces_init(c->cdev, interfaces, MAX_CONFIG_INTERFACES);
	if (num_interfaces < 0)
		return num_interfaces;

	/* Register the interfaces. */
	for (i = 0; i < num_interfaces; i++) {
		ret = usb_add_function(c, interfaces[i]);
		if (ret) {
			GADGET_LOG(LOG_ERR, "unable to add interface: %d\n", ret);
			interfaces_destroy_one(interfaces[i]);
			return ret;
		}

		/* Don't forget to add it to the configuration descriptor. */
		c->interface[i] = interfaces[i];
		c->next_interface_id++;
	}

	return 0;
}

static void mbridge_unbind_config(struct usb_configuration *c)
{
	int i;

	/* Destroy the interfaces */
	for(i = 0; i < c->next_interface_id; i++) {
		ASSERT(c->interface[i] != NULL);
		interfaces_destroy_one( c->interface[i] );
	}
}

static int mbridge_bind(struct usb_composite_dev *cdev)
{
	struct usb_gadget *gadget = cdev->gadget;
	int id, ret;

	GADGET_LOG(LOG_DBG, "initializing gadget...name=%s\n", gadget->name);

	/* Check for High-Speed support. */
	if (!gadget_is_dualspeed(gadget)) {
		GADGET_LOG(LOG_ERR, "gadget does not support high-speed\n");
		return -ENODEV;
	}

	/* Allocate strings. */
	id = usb_string_id(cdev);
	if (id < 0)
		return id;

	strings_dev_dfu[STRING_MANUFACTURER_IDX].id = id;
	device_desc_dfu.iManufacturer = id;

	id = usb_string_id(cdev);
	if (id < 0)
		return id;

	strings_dev_dfu[STRING_PRODUCT_IDX].id = id;
	device_desc_dfu.iProduct = id;

	config_dfu.unbind = mbridge_unbind_config;

	/* With unknown reason, this is required in order to set bcdDevice */
	usb_composite_overwrite_options(cdev, (struct usb_composite_overwrite *)&covr);

	/* Add the default configuration. */
	ret = usb_add_config(cdev, &config_dfu, mbridge_bind_config);
	if (ret) {
		GADGET_LOG(LOG_ERR, "unable to add configuration: %d\n", ret);
		return ret;
	}

	return 0;
}

static int mbridge_unbind(struct usb_composite_dev *cdev)
{
	ASSERT(cdev != NULL);

	GADGET_LOG(LOG_DBG, "destroying gadget...\n");

	return 0;
}

static void mbridge_disconnect(struct usb_composite_dev *cdev)
{
	enum mbridge_dfu_interface_no i;

	for (i = 0; i < ARRAY_SIZE(g_disconnect_cbs); i++) {
		if (g_disconnect_cbs[i])
			(*g_disconnect_cbs[i])(config_dfu.interface[i]);
	}
}

//----------------------------------------------------------------------------
static struct usb_composite_driver mbridge_driver_dfu = {
	.name        = "mbridge_dfu",
	.dev         = &device_desc_dfu,
	.strings     = dev_strings_dfu,
	.max_speed   = USB_SPEED_HIGH,
	.bind        = mbridge_bind,
	.unbind      = mbridge_unbind,
	.disconnect  = mbridge_disconnect,
};


int RegisterDfuModule(void)
{
	int probe_result;
	GADGET_LOG(LOG_DBG, "registering gadget...\n");
	gbIsDfuMode = true;
	probe_result = usb_composite_probe(&mbridge_driver_dfu);
	if (probe_result) {
		GADGET_LOG(LOG_ERR, "Unable to register composite device, error:%d\n", probe_result);
	}
	return probe_result;

}

void UnregisterDfuModule(void)
{
	GADGET_LOG(LOG_DBG, "cleanup.gadget..\n");

	usb_composite_unregister(&mbridge_driver_dfu);

	gbIsDfuMode = false;
}

