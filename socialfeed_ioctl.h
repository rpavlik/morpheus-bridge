/*
 * Copyright (C) 2015 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef _SOCIALFEED_IOCTL_H
#define _SOCIALFEED_IOCTL_H

typedef enum SOCIALFEED_IOCTL_CMD {
	SOCIALFEED_IOCTL_GET_MBUF_INFO = 400,
	SOCIALFEED_IOCTL_GET_MBUF_ACTUAL,
	SOCIALFEED_IOCTL_GET_MBUF_ACTUAL_CANCEL,
	/* for debug */
	SOCIALFEED_IOCTL_GET_COMPLETE_INTERVALS = 450,
} _SOCIALFEED_IOCTL_CMD;

// SOCIALFEED_IOCTL_GET_MBUF_INFO
struct mbridge_socialfeed_mbuf_info {
	int buff_size;
	int block_size;
};

// SOCIALFEED_IOCTL_GET_MBUF_ACTUAL
struct mbridge_socialfeed_mbuf_actual {
	int rp;
	int actual;
	int wait_msec;
};

#endif	/* _SOCIALFEED_IOCTL_H */
