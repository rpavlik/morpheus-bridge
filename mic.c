/*
 * Copyright (C) 2016 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <linux/usb/audio.h>

//#define ENABLE_MBRIDGE_DEBUG

#include "bridge.h"
#include "hostdef.h"
#include "mic.h"
#include "stat.h"

//#define MIC_AUDIO_16KHZ_ENABLE

#define MIC_ENDPOINT_SOURCE_NUM_REQUESTS 16

#if defined(MIC_AUDIO_16KHZ_ENABLE)
#define MIC_NORMAL_REQUEST_SIZE 32
#define MIC_ENDPOINT_SOURCE_REQUEST_SIZE 64
#else
#define MIC_NORMAL_REQUEST_SIZE 96
#define MIC_ENDPOINT_SOURCE_REQUEST_SIZE 192
#endif

//----------------------------------------------------------------------------
#define IN_EP_MAX_PACKET_SIZE (MIC_ENDPOINT_SOURCE_REQUEST_SIZE)
#define OUTPUT_TERMINAL_ID (3)

#define MIC_INTERFACE_RESET_TIMEOUT (200)

/* Standard AS Interface Descriptor */
static struct usb_interface_descriptor as_interface_alt_0_desc = {
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bAlternateSetting = 0,
	.bNumEndpoints = 0,
	.bInterfaceClass = USB_CLASS_AUDIO,
	.bInterfaceSubClass = USB_SUBCLASS_AUDIOSTREAMING,
	.bInterfaceNumber = MORPHEUS_MIC_INTERFACE,
};

static struct usb_interface_descriptor as_interface_alt_1_desc = {
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bAlternateSetting = 1,
	.bNumEndpoints = 1,
	.bInterfaceClass = USB_CLASS_AUDIO,
	.bInterfaceSubClass = USB_SUBCLASS_AUDIOSTREAMING,
	.bInterfaceNumber = MORPHEUS_MIC_INTERFACE,
};

/* B.4.2  Class-Specific AS Interface Descriptor */
static struct uac1_as_header_descriptor as_header_desc = {
	.bLength = UAC_DT_AS_HEADER_SIZE,
	.bDescriptorType = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_AS_GENERAL,
	.bTerminalLink = OUTPUT_TERMINAL_ID, // INPUT_TERMINAL_ID,
	.bDelay = 0,
	.wFormatTag = UAC_FORMAT_TYPE_I_PCM,
};

DECLARE_UAC_FORMAT_TYPE_I_DISCRETE_DESC(1);

static struct uac_format_type_i_discrete_descriptor_1 as_type_i_desc = {
	.bLength = UAC_FORMAT_TYPE_I_DISCRETE_DESC_SIZE(1),
	.bDescriptorType = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_FORMAT_TYPE,
	.bFormatType = UAC_FORMAT_TYPE_I,
	.bSubframeSize = 2,
	.bBitResolution = 16, // 16bit
	.bSamFreqType = 1,
#if defined(MIC_AUDIO_16KHZ_ENABLE)
	// 16kHz
	.bNrChannels = 1, // 1ch
	.tSamFreq[0][0] = 0x80,
	.tSamFreq[0][1] = 0x3E,
	.tSamFreq[0][2] = 0x00
#else
// 48kHz
	.bNrChannels = 1, // 1ch
	.tSamFreq[0][0] = 0x80,
	.tSamFreq[0][1] = 0xBB,
	.tSamFreq[0][2] = 0x00
#endif
};

/* Standard ISO IN Endpoint Descriptor for highspeed */
static struct usb_endpoint_descriptor hs_as_in_ep_desc = {
	.bLength = USB_DT_ENDPOINT_AUDIO_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_MIC_ENDPOINT_SOURCE | USB_DIR_IN,
	.bmAttributes = USB_ENDPOINT_SYNC_ASYNC | USB_ENDPOINT_XFER_ISOC,
	.wMaxPacketSize = __constant_cpu_to_le16(IN_EP_MAX_PACKET_SIZE),
	.bInterval = 4, /* poll 1 per millisecond */
};

/* Standard ISO IN Endpoint Descriptor for fullhspeed */
static struct usb_endpoint_descriptor fs_as_in_ep_desc = {
	.bLength = USB_DT_ENDPOINT_AUDIO_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_MIC_ENDPOINT_SOURCE | USB_DIR_IN,
	.bmAttributes = USB_ENDPOINT_SYNC_ASYNC | USB_ENDPOINT_XFER_ISOC,
	.wMaxPacketSize = __constant_cpu_to_le16(IN_EP_MAX_PACKET_SIZE),
	.bInterval = 1, /* poll 1 per millisecond */
};

/* Class-specific AS ISO IN Endpoint Descriptor */
static struct uac_iso_endpoint_descriptor as_iso_in_desc = {
	.bLength = UAC_ISO_ENDPOINT_DESC_SIZE,
	.bDescriptorType = USB_DT_CS_ENDPOINT,
	.bDescriptorSubtype = UAC_EP_GENERAL,
	.bmAttributes = 0x80, // 1,
	.bLockDelayUnits = 0,
	.wLockDelay = 0, //__constant_cpu_to_le16(1),
};

static struct usb_descriptor_header *fs_mic_descs[] = {
	(struct usb_descriptor_header *)&as_interface_alt_0_desc,
	(struct usb_descriptor_header *)&as_interface_alt_1_desc,
	(struct usb_descriptor_header *)&as_header_desc,
	(struct usb_descriptor_header *)&as_type_i_desc,
	(struct usb_descriptor_header *)&fs_as_in_ep_desc,
	(struct usb_descriptor_header *)&as_iso_in_desc,
	NULL,
};

static struct usb_descriptor_header *hs_mic_descs[] = {
	(struct usb_descriptor_header *)&as_interface_alt_0_desc,
	(struct usb_descriptor_header *)&as_interface_alt_1_desc,
	(struct usb_descriptor_header *)&as_header_desc,
	(struct usb_descriptor_header *)&as_type_i_desc,
	(struct usb_descriptor_header *)&hs_as_in_ep_desc,
	(struct usb_descriptor_header *)&as_iso_in_desc,
	NULL,
};

struct mbridge_mic_interface
{
	struct usb_function interface;

	struct usb_ep *source_ep;
	int alt;

	// members for Tx
	struct usb_request *requests[MIC_ENDPOINT_SOURCE_NUM_REQUESTS];
	atomic_t request_avails;
	int request_avail[MIC_ENDPOINT_SOURCE_NUM_REQUESTS];
	spinlock_t lock;
	int ready;

	uint32_t buffer_filled;
	uint8_t buffer[MIC_ENDPOINT_SOURCE_REQUEST_SIZE];
};

static struct mbridge_mic_interface *sMic = NULL;
static struct mbridge_mic_stat sStat = {0};
static void mbridge_mic_disable(struct usb_function *f);
static int mbridge_mic_enable(struct usb_function *f);

void mbridge_mic_get_stat(struct mbridge_mic_stat* stat)
{
	*stat = sStat;
}
EXPORT_SYMBOL(mbridge_mic_get_stat);

static int find_available(void)
{
	int i;
	for (i = 0; i < MIC_ENDPOINT_SOURCE_NUM_REQUESTS; i++) {
		if (sMic->request_avail[i] != 0)
			break;
	}
	return i;
}

static int mic_request(const void *buf, size_t buflen)
{
	int index = find_available();
	int ret;

	if (index == MIC_ENDPOINT_SOURCE_NUM_REQUESTS)
		return -ENOENT;

	memcpy(sMic->requests[index]->buf, buf, buflen);
	sMic->requests[index]->length = buflen;

	ret = usb_ep_queue(sMic->source_ep, sMic->requests[index], GFP_ATOMIC);
	if (ret)
		return ret;

	ASSERT(sMic->request_avail[index] != 0);
	sMic->request_avail[index] = 0;
	atomic_dec(&sMic->request_avails);
	sStat.request_avails = atomic_read(&sMic->request_avails);
	sStat.sent++;
	return 0;
}

int mbridge_mic_send(const void *buf, size_t buflen)
{
	static int consecutive_normal_packets = 0;
	int ret = 0;

	if (buflen > MIC_ENDPOINT_SOURCE_REQUEST_SIZE)
	{
		sStat.send_error_inval++;
		return -EINVAL;
	}

	if (sMic == NULL)
	{
		sStat.send_error_nodev++;
		return -ENODEV;
	}

	spin_lock(&sMic->lock);
	if (!sMic->ready) {
		spin_unlock(&sMic->lock);
		sStat.send_error_again++;
		return -EAGAIN;
	}

	if (sMic->buffer_filled > 0)
	{
		if (buflen + sMic->buffer_filled <= MIC_ENDPOINT_SOURCE_REQUEST_SIZE)
		{
			memcpy(sMic->buffer + sMic->buffer_filled, buf, buflen);
			ret = mic_request(sMic->buffer, buflen + sMic->buffer_filled);
			consecutive_normal_packets = 0;
			sMic->buffer_filled = 0;
			sStat.merged++;
			sStat.large++;
		}
		else
		{
			ret = mic_request(sMic->buffer, sMic->buffer_filled);
			consecutive_normal_packets++;
			sMic->buffer_filled = 0;
			if (!ret)
			{
				ret = mic_request(buf, buflen);
				consecutive_normal_packets = 0;
				sStat.large++;
			}
		}
	}
	// when some requests are queued and large packet is not sent recently, try to merge buffer to reduce latency.
	else if (consecutive_normal_packets >= 2 && atomic_read(&sMic->request_avails) < MIC_ENDPOINT_SOURCE_NUM_REQUESTS - 2 && buflen <= MIC_NORMAL_REQUEST_SIZE)
	{
		memcpy(sMic->buffer, buf, buflen);
		sMic->buffer_filled = buflen;
	}
	else
	{
		if (buflen <= MIC_NORMAL_REQUEST_SIZE)
		{
			ret = mic_request(buf, buflen);
			consecutive_normal_packets++;
		}
		// large packets should not be sent frequently
		else if (consecutive_normal_packets >= 2)
		{
			ret = mic_request(buf, buflen);
			consecutive_normal_packets = 0;
			sStat.large++;
		}
		else
		{
			// divide data to avoid overflow of host buffer.
			ret = mic_request(buf, MIC_NORMAL_REQUEST_SIZE);
			consecutive_normal_packets++;

			memcpy(sMic->buffer, buf + MIC_NORMAL_REQUEST_SIZE, buflen - MIC_NORMAL_REQUEST_SIZE);
			sMic->buffer_filled = buflen - MIC_NORMAL_REQUEST_SIZE;
		}
	}

	if (ret) {
		spin_unlock(&sMic->lock);
		if (ret == -EINVAL)
			sStat.send_error_inval++;
		else if (ret == -ENODEV)
			sStat.send_error_nodev++;
		else if (ret == -EAGAIN)
			sStat.send_error_again++;
		else if (ret == -ENOENT)
			sStat.send_error_noent++;
		else
			sStat.send_error_others++;
		return ret;
	}
	spin_unlock(&sMic->lock);

	return 0;
}
EXPORT_SYMBOL(mbridge_mic_send);

//----------------------------------------------------------------------------

static void mbridge_mic_complete(struct usb_ep *ep, struct usb_request *req)
{
	struct mbridge_mic_interface *mic_interface = ep->driver_data;

	ASSERT(ep == mic_interface->source_ep);

	switch (req->status) {
	case 0:
		if (req->actual == 0)
			sStat.complete_zero++;
		else
			sStat.complete++;
		break;

	case -ECONNRESET:	/* request dequeued */
	case -ESHUTDOWN:	/* disconnect from host */
	default:
		sStat.complete_error++;
		GADGET_LOG(LOG_ERR, "error request on '%s' %x\n", ep->name, req->status);
		break;
	}

	ASSERT(mic_interface->request_avail[(int)req->context] == 0);
	mic_interface->request_avail[(int)req->context] = 1;
	atomic_inc(&mic_interface->request_avails);
	sStat.request_avails = atomic_read(&sMic->request_avails);
}

static int mbridge_mic_bind(struct usb_configuration *c, struct usb_function *f)
{
	struct mbridge_mic_interface *mic_interface = (struct mbridge_mic_interface *)f;

	ASSERT(c != NULL);
	ASSERT(mic_interface != NULL);

	/* Configure endpoint from the descriptor. */
	mic_interface->source_ep = usb_ep_autoconfig(c->cdev->gadget, &hs_as_in_ep_desc);
	if (mic_interface->source_ep == NULL) {
		GADGET_LOG(LOG_ERR, "can't autoconfigure mic endpoint\n");
		return -ENODEV;
	}

	ASSERT((mic_interface->source_ep->address & USB_ENDPOINT_NUMBER_MASK) ==
	       MORPHEUS_MIC_ENDPOINT_SOURCE);
	GADGET_LOG(LOG_DBG, "name=%s address=0x%x\n", mic_interface->source_ep->name,
		   mic_interface->source_ep->address);

	/* Need to set this during setup only. */
	mic_interface->source_ep->driver_data = c->cdev;

	return 0;
}

static int mbridge_mic_enable(struct usb_function *f)
{
	struct mbridge_mic_interface *mic_interface = (struct mbridge_mic_interface *)f;
	int ret, i;

	ASSERT(mic_interface != NULL);

	GADGET_LOG(LOG_DBG, "enabling mic interface...\n");

	/* Enable the endpoint. */
	ret = config_ep_by_speed(f->config->cdev->gadget, f, mic_interface->source_ep);
	if (ret) {
		GADGET_LOG(LOG_ERR, "error configuring mic endpoint\n");
		return ret;
	}
	ret = usb_ep_enable(mic_interface->source_ep);
	if (ret) {
		GADGET_LOG(LOG_ERR, "error starting mic endpoint\n");
		return ret;
	}

	/* Data for the completion callback. */
	mic_interface->source_ep->driver_data = mic_interface;

	/* Prepare descriptors for TX. */
	for (i = 0; i < MIC_ENDPOINT_SOURCE_NUM_REQUESTS; i++) {
		struct usb_request *req;

		req = alloc_ep_req(mic_interface->source_ep, MIC_ENDPOINT_SOURCE_REQUEST_SIZE,
				   GFP_ATOMIC);
		if (req == NULL) {
			GADGET_LOG(LOG_ERR, "failed allocation usb request\n");
			disable_ep(mic_interface->source_ep);
			return -ENOMEM;
		}

		req->context = (void *)i;
		req->complete = mbridge_mic_complete;

		mic_interface->requests[i] = req;
		mic_interface->request_avail[i] = 1;
		atomic_inc(&mic_interface->request_avails);
	}
	sStat.request_avails = atomic_read(&sMic->request_avails);

	mic_interface->buffer_filled = 0;
	mic_interface->ready = 1;
	return 0;
}

static void mbridge_mic_disable(struct usb_function *f)
{
	struct mbridge_mic_interface *mic_interface = (struct mbridge_mic_interface *)f;
	unsigned long flags;
	int i;

	ASSERT(mic_interface != NULL);

	GADGET_LOG(LOG_DBG, "disabling mic interface, ready:%d...\n", mic_interface->ready);

	spin_lock_irqsave(&mic_interface->lock, flags);
	if (mic_interface->source_ep->driver_data != NULL) {
		mic_interface->buffer_filled = 0;
		mic_interface->ready = 0;
		disable_ep(mic_interface->source_ep);

		for (i = 0; i < MIC_ENDPOINT_SOURCE_NUM_REQUESTS; i++) {
			if (mic_interface->request_avail[i]) {
				mic_interface->request_avail[i] = 0;
				atomic_dec(&mic_interface->request_avails);
				free_ep_req(mic_interface->source_ep, mic_interface->requests[i]);
			}
		}
		sStat.request_avails = atomic_read(&sMic->request_avails);
	}
	spin_unlock_irqrestore(&mic_interface->lock, flags);
}

static void mbridge_mic_usb_quiescent(struct usb_function *f)
{
	struct mbridge_mic_interface *mic_interface =
		(struct mbridge_mic_interface *)f;
	unsigned long flags;

	spin_lock_irqsave(&mic_interface->lock, flags);
	mic_interface->ready = 0;
	spin_unlock_irqrestore(&mic_interface->lock, flags);
}

static int mbridge_mic_reset(struct usb_function *f, unsigned intf, unsigned alt)
{
	struct mbridge_mic_interface *mic_interface = (struct mbridge_mic_interface *)f;

	GADGET_LOG(LOG_DBG, "intf(%d)\n", intf);

	ASSERT(mic_interface != NULL);

	mic_interface->alt = alt;

	mbridge_mic_disable(f);
	return mbridge_mic_enable(f);
}

static int mbridge_mic_get_alt(struct usb_function *f, unsigned intf)
{
	struct mbridge_mic_interface *mic_interface = (struct mbridge_mic_interface *)f;
	ASSERT(mic_interface != NULL);

	GADGET_LOG(LOG_DBG, "intf(%d) alt =(%d)\n", intf, mic_interface->alt);
	return mic_interface->alt;
}

//----------------------------------------------------------------------------

int mbridge_mic_is_ready(void)
{
	int result = 0;
	if (sMic != NULL) {
		result = sMic->ready;
	}
	return result;
}
EXPORT_SYMBOL(mbridge_mic_is_ready);

int mbridge_mic_init(struct usb_composite_dev *cdev, struct usb_function **interface,
		     mbridge_usb_quiescent *quescent_cb)
{
	struct mbridge_mic_interface *mic_interface;
	int id;

	ASSERT(cdev != NULL);
	ASSERT(interface != NULL);

	GADGET_LOG(LOG_DBG, "\n");

	id = usb_string_id(cdev);
	if (id < 0)
		return id;

	strings_dev[STRING_MIC_IDX].id = id;
	as_interface_alt_0_desc.iInterface = id;
	as_interface_alt_1_desc.iInterface = id;

	sMic = mic_interface = kzalloc(sizeof(*mic_interface), GFP_KERNEL);
	if (sMic == NULL)
		return -ENOMEM;

	spin_lock_init(&mic_interface->lock);

	mic_interface->interface.name = "mic";
	mic_interface->interface.fs_descriptors = fs_mic_descs;
	mic_interface->interface.hs_descriptors = hs_mic_descs;
	mic_interface->interface.bind = mbridge_mic_bind;
	mic_interface->interface.set_alt = mbridge_mic_reset;
	mic_interface->interface.disable = mbridge_mic_disable;
	mic_interface->interface.get_alt = mbridge_mic_get_alt;

	*interface = &mic_interface->interface;
	*quescent_cb = mbridge_mic_usb_quiescent;
	return 0;
}

void mbridge_mic_destroy(struct usb_function *interface)
{
	struct mbridge_mic_interface *mic_interface = (struct mbridge_mic_interface *)interface;
	GADGET_LOG(LOG_DBG, "\n");

	ASSERT(mic_interface != NULL);
	kfree(mic_interface);
	sMic = NULL;
}
