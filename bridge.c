/*
 * Copyright (C) 2014 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#include "bridge.h"
#include "hostdef.h"

#include "sensor.h"
#include "command.h"
#include "audio3d.h"
#include "chat.h"
#include "mic.h"
#include "chatmicctrl.h"
#include "socialfeed.h"
#include "hmusys.h"
#include "authentication.h"
#include "bulkin.h"

int gbIsDfuMode = 0;
EXPORT_SYMBOL(gbIsDfuMode);

//----------------------------------------------------------------------------
// 1 - MORPHEUS_AUDIO3D_ENDPOINT
// 2 - MORPHEUS_CHATMIC_ENDPOINT
// 3 - MORPHEUS_SENSOR_ENDPOINT
// 4 - MORPHEUS_COMMAND_ENDPOINT
// 5 - MORPHEUS_SOCIALFEED_ENDPOINT
// 6 - MORPHEUS_AUTHENTICATION_ENDPOINT
// 7 - MORPHEUS_BULKIN_ENDPOINT
struct usb_string strings_dev[] = {
	[STRING_MANUFACTURER_IDX].s  = MORPHEUS_MANUFACTURER,
	[STRING_PRODUCT_IDX].s       = MORPHEUS_PRODUCT,
	[STRING_AUDIO3D_IDX].s       = MORPHEUS_AUDIO3D_INTERFACE_NAME,
	[STRING_CONTROL_IDX].s       = MORPHEUS_CONTROL_INTERFACE_NAME,
	[STRING_MIC_IDX].s           = MORPHEUS_MIC_INTERFACE_NAME,
	[STRING_CHAT_IDX].s          = MORPHEUS_CHAT_INTERFACE_NAME,
	[STRING_SENSOR_IDX].s        = MORPHEUS_SENSOR_INTERFACE_NAME,
	[STRING_COMMAND_IDX].s       = MORPHEUS_COMMAND_INTERFACE_NAME,
	[STRING_SOCIALFEED_IDX].s    = MORPHEUS_SOCIALFEED_INTERFACE_NAME,
	[STRING_AUTHENTICATION_IDX].s    = MORPHEUS_AUTHENTICATION_INTERFACE_NAME,
	[STRING_BULKIN_IDX].s        = MORPHEUS_BULKIN_INTERFACE_NAME,
	{ }
};

static struct usb_gadget_strings stringtab_dev = {
	.language    = 0x0409, /* en-us */
	.strings     = strings_dev,
};

static struct usb_gadget_strings *dev_strings[] = {
	&stringtab_dev,
	NULL,
};

static struct usb_device_descriptor device_desc = {
	.bLength             = sizeof (device_desc),
	.bDescriptorType     = USB_DT_DEVICE,
	.bcdUSB              = cpu_to_le16(0x0200), /* USB 2.0 */
	.bDeviceClass        = USB_CLASS_PER_INTERFACE,
	.idVendor            = cpu_to_le16(MORPHEUS_VENDOR_ID),
	.idProduct           = cpu_to_le16(MORPHEUS_PRODUCT_ID),
	// bcdDevice is ignored by composite framework.
	// See update_unchanged_dev_desc()
	//.bcdDevice           = cpu_to_le16(MORPHEUS_BCDDEVICE),
	.bNumConfigurations  = 1,
};

static const struct usb_composite_overwrite covr = {
	.bcdDevice           = cpu_to_le16(MORPHEUS_BCDDEVICE),
};

static struct usb_configuration bridge_config = {
	.label               = "Default Configuration",
	.strings             = dev_strings,
	.bConfigurationValue = 1,
	.bmAttributes        = USB_CONFIG_ATT_SELFPOWER,
};


static mbridge_usb_disconnect g_disconnect_cbs[MORPHEUS_NUM_INTERFACES];
static mbridge_usb_quiescent g_quiescent_cbs[MORPHEUS_NUM_INTERFACES];

static int interfaces_init(struct usb_composite_dev *cdev, struct usb_function **interfaces,
			   unsigned int num_interfaces)
{
	struct usb_function *audio3d_interface = NULL;
	struct usb_function *chatmicctrl_interface = NULL;
	struct usb_function *mic_interface = NULL;
	struct usb_function *chat_interface = NULL;
	struct usb_function *hid_interface = NULL;
	struct usb_function *cmd_interface = NULL;
	struct usb_function *socialfeed_interface = NULL;
	struct usb_function *bulkin_interface = NULL;
	struct usb_function *authentication_interface = NULL;
	int i, ret;

	GADGET_LOG(LOG_DBG, "initializing interfaces...\n");

	i = 0;
	ret = mbridge_audio3d_init(cdev, &audio3d_interface,
				   &g_disconnect_cbs[i], &g_quiescent_cbs[i]);
	if (ret) {
		GADGET_LOG(LOG_ERR, "failed to initialize audio3d interface: %d\n", ret);
		goto fail_init;
	}

	interfaces[i++] = audio3d_interface;

	ret = mbridge_chatmicctrl_init(cdev, &chatmicctrl_interface);
	if (ret) {
		GADGET_LOG(LOG_ERR, "failed to initialize chatmicctrl interface: %d\n", ret);
		goto fail_init;
	}
	interfaces[i++] = chatmicctrl_interface;

	ret = mbridge_mic_init(cdev, &mic_interface, &g_quiescent_cbs[i]);
	if (ret) {
		GADGET_LOG(LOG_ERR, "failed to initialize mic interface: %d\n", ret);
		goto fail_init;
	}
	interfaces[i++] = mic_interface;

	ret = mbridge_chat_init(cdev, &chat_interface,
				&g_disconnect_cbs[i], &g_quiescent_cbs[i]);
	if (ret) {
		GADGET_LOG(LOG_ERR, "failed to initialize chat interface: %d\n", ret);
		goto fail_init;
	}
	interfaces[i++] = chat_interface;

	ret = mbridge_hid_init(cdev, &hid_interface);
	if (ret) {
		GADGET_LOG(LOG_ERR, "failed to initialize hid interface: %d\n", ret);
		goto fail_init;
	}
	interfaces[i++] = hid_interface;

	ret = mbridge_cmd_init(cdev, &cmd_interface,
			       &g_disconnect_cbs[i], &g_quiescent_cbs[i]);
	if (ret) {
		GADGET_LOG(LOG_ERR, "failed to initialize hid interface: %d\n", ret);
		goto fail_init;
	}
	interfaces[i++] = cmd_interface;

	ret = mbridge_socialfeed_init(cdev, &socialfeed_interface, &g_quiescent_cbs[i]);
	if (ret) {
		GADGET_LOG(LOG_ERR, "failed to initialize social feed interface: %d\n", ret);
		goto fail_init;
	}
	interfaces[i++] = socialfeed_interface;

	ret = mbridge_bulkin_init(cdev, &bulkin_interface, &g_disconnect_cbs[i]);
	if (ret) {
		GADGET_LOG(LOG_ERR, "failed to initialize BulkIn interface: %d\n", ret);
		goto fail_init;
	}
	interfaces[i++] = bulkin_interface;

	// must be last
	ret = mbridge_auth_init(cdev, &authentication_interface);
	if (ret) {
		GADGET_LOG(LOG_ERR, "failed to initialize Authentication interface: %d\n", ret);
		goto fail_init;
	}
	interfaces[i++] = authentication_interface;

	return i;

fail_init:
	if (authentication_interface)
		mbridge_auth_destroy(authentication_interface);
	if (bulkin_interface)
		mbridge_bulkin_destroy(bulkin_interface);
	if (socialfeed_interface)
		mbridge_socialfeed_destroy(socialfeed_interface);
	if (cmd_interface)
		mbridge_cmd_destroy(cmd_interface);
	if (hid_interface)
		mbridge_hid_destroy(hid_interface);
	if (mic_interface)
		mbridge_mic_destroy(mic_interface);
	if (chat_interface)
		mbridge_chat_destroy(chat_interface);
	if (chatmicctrl_interface)
		mbridge_chatmicctrl_destroy(chatmicctrl_interface);
	if (audio3d_interface)
		mbridge_audio3d_destroy(audio3d_interface);
	return ret;
}

static void interfaces_destroy_one(struct usb_function *interface)
{
	struct usb_interface_descriptor *desc;
	desc = (struct usb_interface_descriptor *)interface->fs_descriptors[0];

	GADGET_LOG(LOG_DBG, "destroying interface %d...\n", desc->bInterfaceNumber);

	switch (desc->bInterfaceNumber) {
	case MORPHEUS_AUTHENTICATION_INTERFACE:
		mbridge_auth_destroy(interface);
		break;
	case MORPHEUS_BULKIN_INTERFACE:
		mbridge_bulkin_destroy(interface);
		break;
	case MORPHEUS_SOCIALFEED_INTERFACE:
		mbridge_socialfeed_destroy(interface);
		break;
	case MORPHEUS_COMMAND_INTERFACE:
		mbridge_cmd_destroy(interface);
		break;
	case MORPHEUS_SENSOR_INTERFACE:
		mbridge_hid_destroy(interface);
		break;
	case MORPHEUS_CHAT_INTERFACE:
		mbridge_chat_destroy(interface);
		break;
	case MORPHEUS_MIC_INTERFACE:
		mbridge_mic_destroy(interface);
		break;
	case MORPHEUS_CONTROL_INTERFACE:
		mbridge_chatmicctrl_destroy(interface);
		break;
	case MORPHEUS_AUDIO3D_INTERFACE:
		mbridge_audio3d_destroy(interface);
		break;
	default:
		ASSERT(0);
		GADGET_LOG(LOG_ERR, "unknown interface.\n");
		break;
	}
}


//----------------------------------------------------------------------------
static int mbridge_bind_config(struct usb_configuration *c)
{
	struct usb_function *interfaces[MAX_CONFIG_INTERFACES];
	int num_interfaces, i, ret;

	memset(interfaces, 0, sizeof (interfaces));

	/* Initialize the descriptors for all interfaces. */
	num_interfaces = interfaces_init(c->cdev, interfaces, ARRAY_SIZE(interfaces));
	if (num_interfaces < 0)
		return num_interfaces;

	/* Register the interfaces. */
	for (i = 0; i < num_interfaces; i++) {
		ret = usb_add_function(c, interfaces[i]);
		if (ret) {
			GADGET_LOG(LOG_ERR, "unable to add interface: %d\n", ret);
			interfaces_destroy_one(interfaces[i]);
			return ret;
		}

		/* Don't forget to add it to the configuration descriptor. */
		c->interface[i] = interfaces[i];
		c->next_interface_id++;
	}

	return 0;
}

static void mbridge_unbind_config(struct usb_configuration *c)
{
	int i;

	/* Destroy the interfaces */
	for(i = 0; i < c->next_interface_id; i++) {
		ASSERT(c->interface[i] != NULL);
		interfaces_destroy_one( c->interface[i] );
	}
}

static int mbridge_bind(struct usb_composite_dev *cdev)
{
	struct usb_gadget *gadget = cdev->gadget;
	int id, ret;

	GADGET_LOG(LOG_DBG, "initializing gadget...name=%s\n", gadget->name);

	/* Check for High-Speed support. */
	if (!gadget_is_dualspeed(gadget)) {
		GADGET_LOG(LOG_ERR, "gadget does not support high-speed\n");
		return -ENODEV;
	}

	/* Allocate strings. */
	id = usb_string_id(cdev);
	if (id < 0)
		return id;

	strings_dev[STRING_MANUFACTURER_IDX].id = id;
	device_desc.iManufacturer = id;

	id = usb_string_id(cdev);
	if (id < 0)
		return id;

	strings_dev[STRING_PRODUCT_IDX].id = id;
	device_desc.iProduct = id;

	bridge_config.unbind = mbridge_unbind_config;

	/* With unknown reason, this is required in order to set bcdDevice */
	usb_composite_overwrite_options(cdev,
					(struct usb_composite_overwrite *)&covr);

	/* Add the default configuration. */
	ret = usb_add_config(cdev, &bridge_config, mbridge_bind_config);
	if (ret) {
		GADGET_LOG(LOG_ERR, "unable to add configuration: %d\n", ret);
		return ret;
	}

	return 0;
}

static int mbridge_unbind(struct usb_composite_dev *cdev)
{
	GADGET_LOG(LOG_DBG, "destroying gadget...\n");

	return 0;
}

static void mbridge_disconnect(struct usb_composite_dev *cdev)
{
	enum mbridge_interface_no i;

	// if interface defined disconnect callback, call it
	for (i = 0; i < ARRAY_SIZE(g_disconnect_cbs); i++) {
		if (g_disconnect_cbs[i])
			(*g_disconnect_cbs[i])(bridge_config.interface[i]);
	}
}

//----------------------------------------------------------------------------
static struct usb_composite_driver mbridge_driver = {
	.name        = "mbridge",
	.dev         = &device_desc,
	.strings     = dev_strings,
	.max_speed   = USB_SPEED_HIGH,
	.bind        = mbridge_bind,
	.unbind      = mbridge_unbind,
	.disconnect  = mbridge_disconnect,
};

int RegisterNormalModule()
{
	int probe_result;

	GADGET_LOG(LOG_DBG, "registering gadget...\n");

	probe_result = usb_composite_probe(&mbridge_driver);
	if (probe_result) {
		GADGET_LOG(LOG_ERR, "Unable to register composite device, error:%d\n", probe_result);
	}
	return probe_result;
}

void UnregisterNormalModule()
{
	GADGET_LOG(LOG_DBG, "cleanup.gadget..\n");

	usb_composite_unregister(&mbridge_driver);

	GADGET_LOG(LOG_DBG, "goodbye.\n");
}

//----------------------------------------------------------------------------
enum {
	MBRIDGE_MODE_UNINITIALIZED = 0,
	MBRIDGE_MODE_NORMAL,
	MBRIDGE_MODE_DFU,
};

struct mbridge_device {
	int mode;
	struct mutex mode_lock;

	int major; /* userspace device node */
	struct class *class;
	struct device *nodedev;
};
static struct mbridge_device s_mbridge_device = {
	.mode = MBRIDGE_MODE_UNINITIALIZED,
	.mode_lock = __MUTEX_INITIALIZER(s_mbridge_device.mode_lock),
};

static long mbridge_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int ret;
	enum mbridge_interface_no ifno;

	switch (cmd) {
	case HOST_IOCTL_GET_RUNNING_MODE:
		mutex_lock(&s_mbridge_device.mode_lock);

		if(s_mbridge_device.mode == MBRIDGE_MODE_DFU)
			put_user(1, (uint32_t *)arg);
		else // NORMAL, UNINITIALIZED
			put_user(0, (uint32_t *)arg);

		mutex_unlock(&s_mbridge_device.mode_lock);

		ret = 0;
		break;

	case HOST_IOCTL_SET_NORMAL_MODE:
		mutex_lock(&s_mbridge_device.mode_lock);

		if (s_mbridge_device.mode == MBRIDGE_MODE_DFU)
			UnregisterDfuModule();
		if (s_mbridge_device.mode != MBRIDGE_MODE_NORMAL)
			RegisterNormalModule();
		s_mbridge_device.mode = MBRIDGE_MODE_NORMAL;

		mutex_unlock(&s_mbridge_device.mode_lock);

		ret = 0;
		break;

	case HOST_IOCTL_SET_DFU_MODE:
		mutex_lock(&s_mbridge_device.mode_lock);

		if (s_mbridge_device.mode == MBRIDGE_MODE_NORMAL)
			UnregisterNormalModule();
		if (s_mbridge_device.mode != MBRIDGE_MODE_DFU)
			RegisterDfuModule();
		s_mbridge_device.mode = MBRIDGE_MODE_DFU;

		mutex_unlock(&s_mbridge_device.mode_lock);

		ret = 0;
		break;
	case HOST_IOCTL_QUIESCENT_EP:
		pr_info("%s: Make quiescent EPs\n", __func__);
		for (ifno = 0; ifno < ARRAY_SIZE(g_quiescent_cbs); ifno++) {
			if (g_quiescent_cbs[ifno])
				(*g_quiescent_cbs[ifno])(bridge_config.interface[ifno]);
		}
		pr_info("%s: done\n", __func__);
		ret = 0;
		break;
	default:
		printk(KERN_ERR "%s() Error: invalid cmd: %02X\n", __FUNCTION__, cmd);
		ret = -EINVAL;
		break;
	}

	return ret;
}

static struct file_operations mbridge_fops = {
	.unlocked_ioctl = mbridge_ioctl,
};

static int __init mbridge_init(void)
{
	int ret;

	/* register devnode */
	s_mbridge_device.class = class_create(THIS_MODULE, "mbridge");
	if (IS_ERR(s_mbridge_device.class)) {
		printk(KERN_ERR "%s() failed to create class\n", __FUNCTION__);
		ret = PTR_ERR(s_mbridge_device.class);
		goto rewind0;
	}
	ret = register_chrdev(0, s_mbridge_device.class->name,
			      &mbridge_fops);
	if (ret < 0) {
		printk(KERN_ERR "%s() failed to register chrdev\n", __FUNCTION__);
		goto rewind1;
	}
	s_mbridge_device.major = ret;
	s_mbridge_device.nodedev = device_create(s_mbridge_device.class, NULL,
						 MKDEV(ret, 0), &s_mbridge_device,
						 s_mbridge_device.class->name);
	if (IS_ERR(s_mbridge_device.nodedev)) {
		printk(KERN_ERR "%s() failed to create device\n", __FUNCTION__);
		ret = PTR_ERR(s_mbridge_device.nodedev);
		goto rewind2;
	}

	mbridge_cmd_module_init();

	return 0;
rewind2:
	unregister_chrdev(s_mbridge_device.major,
			  s_mbridge_device.class->name);
	s_mbridge_device.major = 0;
rewind1:
	class_destroy(s_mbridge_device.class);
	s_mbridge_device.class = NULL;
rewind0:

	return ret;
}
module_init(mbridge_init);

static void __exit mbridge_cleanup(void)
{
	if (s_mbridge_device.nodedev) {
		device_destroy(s_mbridge_device.class,
			       MKDEV(s_mbridge_device.major,0));
		s_mbridge_device.class = NULL;
	}
	if (s_mbridge_device.major) {
		unregister_chrdev(s_mbridge_device.major,
				  s_mbridge_device.class->name);
		s_mbridge_device.major = 0;
	}
	if (s_mbridge_device.class) {
		class_destroy(s_mbridge_device.class);
		s_mbridge_device.class = NULL;
	}
}
module_exit(mbridge_cleanup);

MODULE_AUTHOR("Sony Interactive Entertainment Inc.");
MODULE_LICENSE("GPL");
