/*
 * Copyright (C) 2016 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 **/

#include "bridge.h"
#include "mgbuf.h"
#include "bulkin.h"

//#define BULKIN_DEBUG_LOG_ENABLE

#if defined(BULKIN_DEBUG_LOG_ENABLE)
#define GADGET_BULKIN_LOG(LEVEL, FORMAT, ...)				\
	if ((LEVEL) <= LOG_LEVEL) {					\
		const char *file = __FILE__;				\
		if (strlen(file) > 20) {				\
			file = file + strlen(file) - 20;		\
		}							\
		printk("%s:%d (%s) - " FORMAT,				\
		       file, __LINE__, __FUNCTION__, ##__VA_ARGS__);	\
	}
#else
#define GADGET_BULKIN_LOG(LEVEL, FORMAT, ...)
#endif

#define BULKIN_ENDPOINT_SOURCE_NUM_REQUESTS 16
#define BULKIN_ENDPOINT_SOURCE_REQUEST_SIZE 512

static struct usb_interface_descriptor bulkin_intf = {
	.bLength = sizeof(bulkin_intf),
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = MORPHEUS_BULKIN_INTERFACE,
	.bAlternateSetting = 0,
	.bNumEndpoints = 1,
	.bInterfaceClass = USB_CLASS_VENDOR_SPEC,
	.bInterfaceSubClass = 1,
	.bInterfaceProtocol = 0,
};

static struct usb_endpoint_descriptor fs_bulkin_source_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_BULKIN_ENDPOINT_SOURCE | USB_DIR_IN,
	.bmAttributes = USB_ENDPOINT_XFER_BULK,
	.wMaxPacketSize = cpu_to_le16(BULKIN_ENDPOINT_SOURCE_REQUEST_SIZE),
};

static struct usb_endpoint_descriptor hs_bulkin_source_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_BULKIN_ENDPOINT_SOURCE | USB_DIR_IN,
	.bmAttributes = USB_ENDPOINT_XFER_BULK,
	.wMaxPacketSize = cpu_to_le16(BULKIN_ENDPOINT_SOURCE_REQUEST_SIZE),
};

static struct usb_descriptor_header *fs_bulkin_descs[] = {
	(struct usb_descriptor_header *)&bulkin_intf,
	(struct usb_descriptor_header *)&fs_bulkin_source_desc,
	NULL,
};

static struct usb_descriptor_header *hs_bulkin_descs[] = {
	(struct usb_descriptor_header *)&bulkin_intf,
	(struct usb_descriptor_header *)&hs_bulkin_source_desc,
	NULL,
};

struct mbridge_bulkin_interface
{
	struct usb_function interface;

	struct usb_ep *source_ep;

	struct mgbuf mgbuf;

	unsigned long flags;
#define EP_ENABLED (0)

	int major; /* userspace device node */
	struct class *class;
	struct device *nodedev;
};
#define to_mbridge_bulkin_interface(f) \
	container_of(f, struct mbridge_bulkin_interface, interface)

struct mbridge_bulkin_interface s_bulkin_interface;

//----------------------------------------------------------------------------
static int device_bulkin_open(struct inode *inode, struct file *filp)
{
	struct mgbuf *mgbuf = &s_bulkin_interface.mgbuf;
	// mgbuf: set mgbuf* to file->private_data
	filp->private_data = mgbuf;

	return mgbuf_open(mgbuf, inode, filp);
}

static int device_bulkin_release(struct inode *inode, struct file *filp)
{
	struct mgbuf *mgbuf = filp->private_data;

	return mgbuf_release(mgbuf, inode, filp);
}

static ssize_t device_bulkin_write(struct file *filp, const char *buf, size_t size, loff_t *offs)
{
	struct mgbuf *mgbuf = filp->private_data;

	return mgbuf_write(mgbuf, filp, buf, size, offs);
}

static unsigned int device_bulkin_poll(struct file *filp, poll_table *wait)
{
	struct mgbuf *mgbuf = filp->private_data;

	return mgbuf_poll(mgbuf, filp, wait);
}

static int device_bulkin_fsync(struct file *filp, loff_t start, loff_t end, int datasync)
{
	struct mgbuf *mgbuf = filp->private_data;

	return mgbuf_fsync(mgbuf, filp, start, end, datasync);
}

static int mbridge_bulkin_bind(struct usb_configuration *c, struct usb_function *f)
{
	struct mbridge_bulkin_interface *bulkin_interface =
		to_mbridge_bulkin_interface(f);
	struct mgbuf *mgbuf = &bulkin_interface->mgbuf;

	GADGET_BULKIN_LOG(LOG_DBG, "\n");

	/* Configure endpoint from the descriptor. */
	bulkin_interface->source_ep = usb_ep_autoconfig(c->cdev->gadget, &fs_bulkin_source_desc);
	if (bulkin_interface->source_ep == NULL) {
		GADGET_BULKIN_LOG(LOG_ERR, "can't autoconfigure bulkin endpoint\n");
		return -ENODEV;
	}
	/* Need to set this during setup only. */
	bulkin_interface->source_ep->driver_data = c->cdev;

	GADGET_BULKIN_LOG(LOG_DBG, "epname=%s address=0x%x\n",
			   bulkin_interface->source_ep->name,
			   bulkin_interface->source_ep->address);

	if (mgbuf_init(mgbuf, "bulkin",
		       NULL, 0, 0, 0,
		       bulkin_interface->source_ep,
		       BULKIN_ENDPOINT_SOURCE_REQUEST_SIZE,
		       BULKIN_ENDPOINT_SOURCE_NUM_REQUESTS,
		       usb_endpoint_xfer_isoc(&fs_bulkin_source_desc))) {
		GADGET_BULKIN_LOG(LOG_ERR, "can't initialize mgbuf\n");
		return -ENODEV;
	}
	mgbuf_set_usbconnected(&bulkin_interface->mgbuf, 0);

	return 0;
}

static void mbridge_bulkin_unbind(struct usb_configuration *c, struct usb_function *f)
{
	struct mbridge_bulkin_interface *bulkin_interface =
		to_mbridge_bulkin_interface(f);
	struct mgbuf *mgbuf = &bulkin_interface->mgbuf;

	mgbuf_deinit(mgbuf);
}

static int mbridge_bulkin_enable(struct usb_function *f)
{
	struct mbridge_bulkin_interface *bulkin_interface =
		to_mbridge_bulkin_interface(f);
	struct mgbuf *mgbuf = &bulkin_interface->mgbuf;
	int ret;

	GADGET_BULKIN_LOG(LOG_DBG, "enabling bulkin interface...\n");

	/* Enable the endpoint. */
	ret = config_ep_by_speed(f->config->cdev->gadget, f, bulkin_interface->source_ep);
	if (ret) {
		GADGET_BULKIN_LOG(LOG_ERR, "error configuring bulkin endpoint\n");
		return ret;
	}

	if (!test_and_set_bit(EP_ENABLED, &bulkin_interface->flags)) {
		ret = usb_ep_enable(bulkin_interface->source_ep);
		if (ret) {
			clear_bit(EP_ENABLED, &bulkin_interface->flags);
			GADGET_BULKIN_LOG(LOG_ERR, "error starting bulkin endpoint\n");
			return ret;
		}
		ret = mgbuf_pre_queue(mgbuf, 0, 1);
		if (ret) {
			clear_bit(EP_ENABLED, &bulkin_interface->flags);
			usb_ep_disable(bulkin_interface->source_ep);
			return ret;
		}
		mgbuf_set_usbconnected(&bulkin_interface->mgbuf, 1);
	}

	return 0;
}

static void mbridge_bulkin_disable(struct usb_function *f)
{
	struct mbridge_bulkin_interface *bulkin_interface =
		to_mbridge_bulkin_interface(f);

	GADGET_BULKIN_LOG(LOG_DBG, "disabling bulkin interface...\n");

	if (test_and_clear_bit(EP_ENABLED, &bulkin_interface->flags))
		usb_ep_disable(bulkin_interface->source_ep);
}

static int mbridge_bulkin_reset(struct usb_function *f, unsigned intf, unsigned alt)
{
	int ret;

	// we have only one interface
	if (alt)
		return -EINVAL;

	mbridge_bulkin_disable(f);
	ret = mbridge_bulkin_enable(f);
	GADGET_BULKIN_LOG(LOG_DBG, "intf(%d), ret=%d\n", intf, ret);
	return ret;
}

static void mbridge_bulkin_usb_disconnect(struct usb_function *f)
{
	struct mbridge_bulkin_interface *bulkin_interface =
		to_mbridge_bulkin_interface(f);

	mgbuf_set_usbconnected(&bulkin_interface->mgbuf, 0);
}

//----------------------------------------------------------------------------

static struct file_operations bulkin_fops = {
	.write = device_bulkin_write,
	.poll = device_bulkin_poll,
	.open = device_bulkin_open,
	.release = device_bulkin_release,
	.fsync = device_bulkin_fsync,
};

int mbridge_bulkin_init(struct usb_composite_dev *cdev, struct usb_function **interface,
	mbridge_usb_disconnect *disconn_cb)
{
	int ret;

	GADGET_BULKIN_LOG("%s: start\n", __func__);

	ret = usb_string_id(cdev);
	if (ret < 0)
		return ret;
	strings_dev[STRING_BULKIN_IDX].id = ret;
	bulkin_intf.iInterface = ret;

	memset(&s_bulkin_interface, 0, sizeof(s_bulkin_interface));

	/* register devnode */
	s_bulkin_interface.class = class_create(THIS_MODULE, "pu_bulkin");
	if (IS_ERR(s_bulkin_interface.class)) {
		dev_err(&cdev->gadget->dev, "failed to create class\n");
		ret = PTR_ERR(s_bulkin_interface.class);
		goto rewind0;
	}
	ret = register_chrdev(0, s_bulkin_interface.class->name, &bulkin_fops);
	if (ret < 0) {
		dev_err(&cdev->gadget->dev, "failed to register chrdev\n");
		goto rewind1;
	}
	s_bulkin_interface.major = ret;
	s_bulkin_interface.nodedev = device_create(s_bulkin_interface.class, NULL,
						    MKDEV(ret, 0), &s_bulkin_interface,
						    s_bulkin_interface.class->name);
	if (IS_ERR(s_bulkin_interface.nodedev)) {
		dev_err(&cdev->gadget->dev, "failed to create device\n");
		ret = PTR_ERR(s_bulkin_interface.nodedev);
		goto rewind2;
	}

	s_bulkin_interface.interface.name = "bulkin";
	s_bulkin_interface.interface.fs_descriptors = fs_bulkin_descs;
	s_bulkin_interface.interface.hs_descriptors = hs_bulkin_descs;
	s_bulkin_interface.interface.bind = mbridge_bulkin_bind;
	s_bulkin_interface.interface.unbind = mbridge_bulkin_unbind;
	s_bulkin_interface.interface.set_alt = mbridge_bulkin_reset;
	s_bulkin_interface.interface.disable = mbridge_bulkin_disable;

	*interface = &s_bulkin_interface.interface;
	*disconn_cb = mbridge_bulkin_usb_disconnect;

	GADGET_BULKIN_LOG(LOG_INFO, "Initialized.\n");
	return 0;

rewind2:
	unregister_chrdev(s_bulkin_interface.major, s_bulkin_interface.class->name);
	s_bulkin_interface.major = 0;
rewind1:
	class_destroy(s_bulkin_interface.class);
	s_bulkin_interface.class = NULL;
rewind0:
	return ret;
}

void mbridge_bulkin_destroy(struct usb_function *interface)
{
	if (s_bulkin_interface.nodedev) {
		device_destroy(s_bulkin_interface.class,
			       MKDEV(s_bulkin_interface.major,0));
		s_bulkin_interface.nodedev = NULL;
	}
	if (s_bulkin_interface.major) {
		unregister_chrdev(s_bulkin_interface.major,
				  s_bulkin_interface.class->name);
		s_bulkin_interface.major = 0;
	}
	if (s_bulkin_interface.class) {
		class_destroy(s_bulkin_interface.class);
		s_bulkin_interface.class = NULL;
	}
}
