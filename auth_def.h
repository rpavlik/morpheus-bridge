/*
 * Copyright (C) 2015 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef _SIE_AUTH_DEF_H_
#define _SIE_AUTH_DEF_H_

#define AUTH_SUCCESS				0
#define AUTH_ERROR_NOT_INITIALIZED		0x80010001
#define AUTH_ERROR_INVALID_ARGS			0x80020001
#define AUTH_ERROR_INVALID_REQUEST		0x80020002
#define AUTH_ERROR_INVALID_BLOCK_NUMBER		0x80030001
#define AUTH_ERROR_NOT_CREATED			0x80030002
#define AUTH_ERROR_NOT_SUPPORTED		0x80030003
#define AUTH_ERROR_NO_DATA			0x80030004

// internal
#define AUTH_STATE_READY                   0
#define AUTH_STATE_AUTH1_RECEIVING          1
#define AUTH_STATE_AUTH1_ALL_RECEIVED       2
#define AUTH_STATE_AUTH2_CREATING           3
#define AUTH_STATE_AUTH2_CREATE_DONE        4
#define AUTH_STATE_AUTH2_SENDING            5
#define AUTH_STATE_CANCEL                   6
#define AUTH_STATE_CANCEL_WAIT              7

#define HAUTH_PACKET_LENGTH                 (64)
#define HAUTH_MSG_LENGTH                    (56)

#define HAUTH_TOTAL_BLOCK_NUM_HELLO         (2)
#define HAUTH_TOTAL_BLOCK_NUM_START         (2)
#define HAUTH_TOTAL_BLOCK_NUM_MSG1          (6)
#define HAUTH_TOTAL_BLOCK_NUM_MSG2          (2)
#define HAUTH_TOTAL_BLOCK_NUM_DATA          (2)
#define HAUTH_TOTAL_BLOCK_NUM_STATUS        (2)

#define HAUTH_BUFSIZE_HELLO                 (HAUTH_MSG_LENGTH * HAUTH_TOTAL_BLOCK_NUM_HELLO)
#define HAUTH_BUFSIZE_START                 (HAUTH_MSG_LENGTH * HAUTH_TOTAL_BLOCK_NUM_START)
#define HAUTH_BUFSIZE_MSG1                  (HAUTH_MSG_LENGTH * HAUTH_TOTAL_BLOCK_NUM_MSG1)
#define HAUTH_BUFSIZE_MSG2                  (HAUTH_MSG_LENGTH * HAUTH_TOTAL_BLOCK_NUM_MSG2)
#define HAUTH_BUFSIZE_DATA                  (HAUTH_MSG_LENGTH * HAUTH_TOTAL_BLOCK_NUM_DATA)
#define HAUTH_BUFSIZE_STATUS                (HAUTH_MSG_LENGTH * HAUTH_TOTAL_BLOCK_NUM_STATUS)

#endif
