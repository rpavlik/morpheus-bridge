/*
 * Copyright (C) 2016 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <asm/uaccess.h>
#include <linux/fs.h>
#include <linux/device.h>

#include "bridge.h"
#include "hostdef.h"
#include "audio3d.h"
#include "mgbuf.h"

//----------------------------------------------------------------------------
static struct usb_interface_descriptor audio3d_intfs[] = {
	{
		.bLength             = sizeof(struct usb_interface_descriptor),
		.bDescriptorType     = USB_DT_INTERFACE,
		.bNumEndpoints       = 0,
		.bAlternateSetting   = 0,
		.bInterfaceClass     = USB_CLASS_VENDOR_SPEC,
		.bInterfaceNumber    = MORPHEUS_AUDIO3D_INTERFACE,
	},
	{
		.bLength             = sizeof(struct usb_interface_descriptor),
		.bDescriptorType     = USB_DT_INTERFACE,
		.bNumEndpoints       = 1,
		.bAlternateSetting   = 1,
		.bInterfaceClass     = USB_CLASS_VENDOR_SPEC,
		.bInterfaceNumber    = MORPHEUS_AUDIO3D_INTERFACE,
	},
};

static const struct usb_interface_descriptor audio3d_alt1_intf = {
	.bLength             = sizeof (audio3d_alt1_intf),
	.bDescriptorType     = USB_DT_INTERFACE,
	.bNumEndpoints       = 1,
	.bAlternateSetting   = 1,
	.bInterfaceClass     = USB_CLASS_VENDOR_SPEC,
	.bInterfaceNumber    = MORPHEUS_AUDIO3D_INTERFACE,
};

// dummy interface, we never support FS
static const struct usb_endpoint_descriptor fs_audio3d_sink_desc = {
	.bLength             = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType     = USB_DT_ENDPOINT,
	.bEndpointAddress    = MORPHEUS_AUDIO3D_ENDPOINT_SINK | USB_DIR_OUT,
	.bmAttributes        = USB_ENDPOINT_XFER_ISOC | USB_ENDPOINT_SYNC_ASYNC,
	.wMaxPacketSize      = cpu_to_le16(1023),
	.bInterval           = 1,
};

static const struct usb_endpoint_descriptor hs_audio3d_sink_desc = {
	.bLength             = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType     = USB_DT_ENDPOINT,
	.bEndpointAddress    = MORPHEUS_AUDIO3D_ENDPOINT_SINK | USB_DIR_OUT,
	.bmAttributes        = USB_ENDPOINT_XFER_ISOC | USB_ENDPOINT_SYNC_ASYNC,
	.wMaxPacketSize      = cpu_to_le16(1024),
	.bInterval           = 1, /* 125 uS */
	/* 1 transaction * 8 times per millisecond * 1024 bytes = 8MB/s */
};

static struct usb_descriptor_header const *fs_audio3d_descs[] = {
	(struct usb_descriptor_header *)&audio3d_intfs[0],
	(struct usb_descriptor_header *)&audio3d_intfs[1],
	(struct usb_descriptor_header *)&fs_audio3d_sink_desc,
	NULL,
};

static struct usb_descriptor_header const *hs_audio3d_descs[] = {
	(struct usb_descriptor_header *)&audio3d_intfs[0],
	(struct usb_descriptor_header *)&audio3d_intfs[1],
	(struct usb_descriptor_header *)&hs_audio3d_sink_desc,
	NULL,
};

struct mbridge_audio3d_function {
	struct usb_function  func;
	struct usb_ep        *sink_ep;
	struct mgbuf         mgbuf;
	unsigned long        flags;
#define EP_ENABLED (0)
#define MGBUF_INITIALIZED (1)
	int                  alt;
	/* userspace device node */
	int                  major;
	struct class        *class;
	struct device       *nodedev;
};

//
// usb_function operations
//
static int _audio3d_func_bind(struct usb_configuration *cfg, struct usb_function *f)
{
	struct mbridge_audio3d_function *func =
		container_of(f, struct mbridge_audio3d_function, func);

	// find endpoint suitable for our descriptor
	func->sink_ep = usb_ep_autoconfig(cfg->cdev->gadget,
					  (struct usb_endpoint_descriptor *)
					  &hs_audio3d_sink_desc);
	if (!func->sink_ep) {
		pr_err("%s: EP not configured!\n", __func__);
		return -ENODEV;
	}

	// initialize mgbuf
	if (test_and_set_bit(MGBUF_INITIALIZED, &func->flags)) {
		pr_err("%s: already initialized\n", __func__);
		return -EBUSY;
	}

	if (mgbuf_init(&func->mgbuf, "audio3d",
		       func->sink_ep,
		       func->sink_ep->maxpacket_limit,
		       256, usb_endpoint_xfer_isoc(&hs_audio3d_sink_desc),
		       NULL, 0, 0, 0)) { // IN EP is not used
		pr_err("%s: failed to initialize mgbuf\n", __func__);
		return -ENODEV;
	}
	mgbuf_set_usbconnected(&func->mgbuf, 0);

	// no need this?
	func->sink_ep->driver_data = cfg->cdev;

	return 0;
} /* func_bind */

static void _audio3d_func_unbind(struct usb_configuration *cfg, struct usb_function *f)
{
	struct mbridge_audio3d_function *func =
		container_of(f, struct mbridge_audio3d_function, func);

	if (!func->sink_ep)
		return;
	if (!test_and_clear_bit(MGBUF_INITIALIZED, &func->flags)) {
		pr_err("%s: already initialized\n", __func__);
		return;
	}
	mgbuf_deinit(&func->mgbuf);
} /* func_unbind */

static int _audio3d_func_set_alt(struct usb_function *f, unsigned intf, unsigned alt)
{
	struct mbridge_audio3d_function *func =
		container_of(f, struct mbridge_audio3d_function, func);
	int ret = 0;

	// host is requesting alt-setting out of range?
	if (ARRAY_SIZE(audio3d_intfs) <= alt)
		return -EINVAL;

	// the default(0) alt-setting has no EP
	// and has no functionality
	if (!alt) {
		func->alt = 0;
		return 0;
	}

	// always re-initialize EP when SET_CONFIG or SET_ALT_INTF
	if (test_and_clear_bit(EP_ENABLED, &func->flags)) {
		mgbuf_set_usbconnected(&func->mgbuf, 0);
		ret = usb_ep_disable(func->sink_ep);
	}

	// configure the endpoint
	ret = config_ep_by_speed(f->config->cdev->gadget, f,
				 func->sink_ep);
	if (ret) {
		pr_err("%s:faile to confiture EP\n", __func__);
		return ret;
	}

	if (!test_and_set_bit(EP_ENABLED, &func->flags)) {
		ret = usb_ep_enable(func->sink_ep);
		if (ret) {
			clear_bit(EP_ENABLED, &func->flags);
			pr_err("%s: failed to enable EP %d\n", __func__, ret);
			return ret;
		}
		ret = mgbuf_pre_queue(&func->mgbuf, 1, 0);
		// say mgbuf host starts to communicate
		mgbuf_set_usbconnected(&func->mgbuf, 1);
	}

	func->alt = alt;

	return ret;
} /* func_set_alt */

static int _audio3d_func_get_alt(struct usb_function *f, unsigned intf)
{
	struct mbridge_audio3d_function *func =
		container_of(f, struct mbridge_audio3d_function, func);

	return func->alt;
} /* func_get_alt */

static void _audio3d_func_disable(struct usb_function *f)
{
	struct mbridge_audio3d_function *func =
		container_of(f, struct mbridge_audio3d_function, func);

	if (test_and_clear_bit(EP_ENABLED, &func->flags))
		usb_ep_disable(func->sink_ep);

} /* func_disable */

//
// the instance
//
static struct mbridge_audio3d_function g_func = {
	.func = {
		.name = "audio3d",
		.fs_descriptors = (struct usb_descriptor_header **)fs_audio3d_descs,
		.hs_descriptors = (struct usb_descriptor_header **)hs_audio3d_descs,
		.bind = _audio3d_func_bind,
		.unbind = _audio3d_func_unbind,
		.set_alt = _audio3d_func_set_alt,
		.disable = _audio3d_func_disable,
		.get_alt = _audio3d_func_get_alt,
	}
};

//
// file operations
//
static int _audio3d_file_open(struct inode *inode, struct file *file)
{
	struct mgbuf *mgbuf = &g_func.mgbuf;
	// mgbuf: set mgbuf* to file->private_data
	file->private_data = mgbuf;

	return mgbuf_open(mgbuf, inode, file);
} /* file_open */

static int _audio3d_file_release(struct inode *inode, struct file *file)
{
	struct mgbuf *mgbuf = file->private_data;

	return mgbuf_release(mgbuf, inode, file);
} /* file_release */

static ssize_t _audio3d_file_read(struct file *file, char *buffer,
				   size_t count, loff_t *ppos)
{
	struct mgbuf *mgbuf = file->private_data;
	int ret;

	ret = mgbuf_read(mgbuf, file, buffer, count, ppos);

	return ret;
} /* file_read */

static struct file_operations audio3d_fops = {
	.open = _audio3d_file_open,
	.release = _audio3d_file_release,
	.read = _audio3d_file_read,
};

static void audio3d_usb_disconnect(struct usb_function *f)
{
	struct mbridge_audio3d_function *func =
		container_of(f, struct mbridge_audio3d_function, func);

	mgbuf_set_usbconnected(&func->mgbuf, 0);
}

static void audio3d_usb_quiescent(struct usb_function *f)
{
	struct mbridge_audio3d_function *func =
		container_of(f, struct mbridge_audio3d_function, func);

	mgbuf_make_quiescent(&func->mgbuf);
}

int mbridge_audio3d_init(struct usb_composite_dev *cdev, struct usb_function **f,
			 mbridge_usb_disconnect *disconn_cb,
			 mbridge_usb_quiescent *quiescent_cb)
{
	struct mbridge_audio3d_function *func = &g_func;
	int i, ret;

	ret = usb_string_id(cdev);
	if (ret < 0) {
		pr_err("%s: failed to allocate string %d\n", __func__, ret);
		return ret;
	}

	strings_dev[STRING_AUDIO3D_IDX].id = ret;
	for (i = 0; i < ARRAY_SIZE(audio3d_intfs); i++)
		audio3d_intfs[i].iInterface = ret;

	/* register devnode */
	func->class = class_create(THIS_MODULE, "pu_3daudio");
	if (IS_ERR(func->class)) {
		dev_err(&cdev->gadget->dev, "failed to create class\n");
		ret = PTR_ERR(func->class);
		return ret;
	}

	ret = register_chrdev(0, func->class->name, &audio3d_fops);
	if (ret < 0) {
		dev_info(&cdev->gadget->dev, "failed to register chrdev\n");
		goto rewind0;
	}
	func->major = ret;
	func->nodedev = device_create(func->class, NULL,
				      MKDEV(ret, 0), func,
				      func->class->name);
	if (IS_ERR(func->nodedev)) {
		dev_err(&cdev->gadget->dev, "failed to create device\n");
		ret = PTR_ERR(func->nodedev);
		goto rewind1;
	}

	*disconn_cb = audio3d_usb_disconnect;
	*quiescent_cb = audio3d_usb_quiescent;
	*f = &func->func;
	return 0;

rewind1:
	unregister_chrdev(func->major, func->class->name);
rewind0:
	class_destroy(func->class);
	return ret;
} /* init */

void mbridge_audio3d_destroy(struct usb_function *f)
{
	struct mbridge_audio3d_function *func =
		(struct mbridge_audio3d_function *)f;

	if (func->nodedev)
		device_destroy(func->class,
			       MKDEV(func->major,0));
	if (func->major)
		unregister_chrdev(func->major,
				  func->class->name);
	if (func->class)
		class_destroy(func->class);
} /* destroy */
