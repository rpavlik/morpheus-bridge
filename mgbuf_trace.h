#undef TRACE_SYSTEM
#define TRACE_SYSTEM mgbuf

#if !defined(__MGBUF_TRACE_H) || defined(TRACE_HEADER_MULTI_READ)
#define __MGBUF_TRACE_H

#include <linux/types.h>
#include <linux/tracepoint.h>
// usb_request callback
#define EP_NAME_LEN  16
TRACE_EVENT(mgbuf_complete,
	    TP_PROTO(struct mgbuf_dmabuf *dmabuf, struct usb_ep *ep,
		     struct usb_request *req),
	    TP_ARGS(dmabuf, ep, req),
	    TP_STRUCT__entry(
		    __dynamic_array(char, name, MGBUF_NAME_LEN)
		    __dynamic_array(char, epname, EP_NAME_LEN)
		    __field(struct mgbuf_dmabuf *, dmabuf)
		    __field(struct usb_ep *, ep)
		    __field(struct usb_request *, req)
		    ),
	    TP_fast_assign(
		    snprintf(__get_str(name), MGBUF_NAME_LEN, "%s",
			     dmabuf->mgbuf->name);
		    snprintf(__get_str(epname), EP_NAME_LEN, "%s",
			     ep->name);
		    __entry->dmabuf = dmabuf;
		    __entry->ep = ep;
		    __entry->req = req;
		    ),
	    TP_printk("%s: %s dmabuf %p req %p status %d actual %d",
		      __get_str(name),
		      __get_str(epname),
		      __entry->dmabuf, __entry->req,
		      __entry->req->status, __entry->req->actual
		    )
	);
// read/write
DECLARE_EVENT_CLASS(mgbuf_rw_in,
		    TP_PROTO(struct mgbuf *mgbuf, size_t count),
		    TP_ARGS(mgbuf, count),
		    TP_STRUCT__entry(
			    __dynamic_array(char, name, MGBUF_NAME_LEN)
			    __field(size_t, count)
			    ),
		    TP_fast_assign(
			    snprintf(__get_str(name), MGBUF_NAME_LEN, "%s",
				     mgbuf->name);
			    __entry->count = count;
			    ),
		    TP_printk("%s: count %d", __get_str(name), __entry->count)
	);
DEFINE_EVENT(mgbuf_rw_in, mgbuf_readin,
	     TP_PROTO(struct mgbuf *mgbuf, size_t count),
	     TP_ARGS(mgbuf, count)
	);
DEFINE_EVENT(mgbuf_rw_in, mgbuf_writein,
	     TP_PROTO(struct mgbuf *mgbuf, size_t count),
	     TP_ARGS(mgbuf, count)
	);

DECLARE_EVENT_CLASS(mgbuf_rw_out,
		    TP_PROTO(struct mgbuf *mgbuf, struct mgbuf_dmabuf *dmabuf, int ret),
		    TP_ARGS(mgbuf, dmabuf, ret),
		    TP_STRUCT__entry(
			    __dynamic_array(char, name, MGBUF_NAME_LEN)
			    __field(struct mgbuf_dmabuf *, dmabuf)
			    __field(int, ret)
			    ),
		    TP_fast_assign(
			    snprintf(__get_str(name), MGBUF_NAME_LEN, "%s",
				     mgbuf->name);
			    __entry->dmabuf = dmabuf;
			    __entry->ret = ret;
			    ),
		    TP_printk("%s: dmabuf %p ret %d", __get_str(name),
			      __entry->dmabuf, __entry->ret)
	);
DEFINE_EVENT(mgbuf_rw_out, mgbuf_readout,
	     TP_PROTO(struct mgbuf *mgbuf, struct mgbuf_dmabuf *dmabuf, int ret),
	     TP_ARGS(mgbuf, dmabuf, ret)
	);
DEFINE_EVENT(mgbuf_rw_out, mgbuf_writeout,
	     TP_PROTO(struct mgbuf *mgbuf, struct mgbuf_dmabuf *dmabuf, int ret),
	     TP_ARGS(mgbuf, dmabuf, ret)
	);
// poll
TRACE_EVENT(mgbuf_poll,
	    TP_PROTO(struct mgbuf *mgbuf, unsigned int mask),
	    TP_ARGS(mgbuf, mask),
	    TP_STRUCT__entry(
		    __dynamic_array(char, name, MGBUF_NAME_LEN)
		    __field(struct mgbuf *, mgbuf)
		    __field(unsigned int, mask)
		    ),
	    TP_fast_assign(
		    snprintf(__get_str(name), MGBUF_NAME_LEN, "%s",
			     mgbuf->name);
		    __entry->mgbuf = mgbuf;
		    __entry->mask = mask;
		    ),
	    TP_printk("%s: mask %#x",  __get_str(name), __entry->mask)
	);
// ioctl get/put_block
TRACE_EVENT(mgbuf_ioctl_blkin,
	    TP_PROTO(struct mgbuf *mgbuf, unsigned int cmd, struct mgbuf_block_param *param),
	    TP_ARGS(mgbuf, cmd, param),
	    TP_STRUCT__entry(
		    __dynamic_array(char, name, MGBUF_NAME_LEN)
		    __field(unsigned int, cmd)
		    __field(void *, bufaddr)
		    __field(size_t, validlen)
		    ),
	    TP_fast_assign(
		    snprintf(__get_str(name), MGBUF_NAME_LEN, "%s",
			     mgbuf->name);
		    __entry->cmd = cmd;
		    __entry->bufaddr = param->bufaddr;
		    __entry->validlen = param->validlen;
		    ),
	    TP_printk("%s: ioctl(block_%s) buf %p valid %d",
		      __get_str(name),
		      __entry->cmd == MGBUF_IOCTL_GET_MEM_BLOCK ? "get" : "put",
		      __entry->bufaddr, __entry->validlen
		    )
	);
// open/release/deinit
DECLARE_EVENT_CLASS(mgbuf_open_close,
		    TP_PROTO(struct mgbuf *mgbuf),
		    TP_ARGS(mgbuf),
		    TP_STRUCT__entry(
			    __dynamic_array(char, name, MGBUF_NAME_LEN)
			    __field(int, refcount)
			    __field(unsigned int, flags)
			    ),
		    TP_fast_assign(
			    snprintf(__get_str(name), MGBUF_NAME_LEN, "%s",
				     mgbuf->name);
			    __entry->refcount = mgbuf->ref.counter;
			    __entry->flags = mgbuf->flags;
			    ),
		    TP_printk("%s: ref=%d flag=%x",
			      __get_str(name),
			      __entry->refcount,
			      __entry->flags
			    )
	);
// set_usb_connected
TRACE_EVENT(mgbuf_set_usbconnected,
	    TP_PROTO(struct mgbuf *mgbuf, int connected),
	    TP_ARGS(mgbuf, connected),
	    TP_STRUCT__entry(
		    __dynamic_array(char, name, MGBUF_NAME_LEN)
		    __field(unsigned int, flags)
		    __field(int, connected)
		    __field(unsigned int, refcount)
		    ),
	    TP_fast_assign(
		    snprintf(__get_str(name), MGBUF_NAME_LEN, "%s",
			     mgbuf->name);
		    __entry->flags = mgbuf->flags;
		    __entry->connected = connected;
		    __entry->refcount = mgbuf->ref.counter;
		    ),
	    TP_printk("%s: connect %d flags %x refcount %d",
		      __get_str(name),
		      !!__entry->connected,
		      __entry->flags,
		      __entry->refcount
		    )
	);
// open
DEFINE_EVENT(mgbuf_open_close, mgbuf_open,
	     TP_PROTO(struct mgbuf *mgbuf),
	     TP_ARGS(mgbuf)
	);
// release
DEFINE_EVENT(mgbuf_open_close, mgbuf_release,
	     TP_PROTO(struct mgbuf *mgbuf),
	     TP_ARGS(mgbuf)
	);
// deinit
DEFINE_EVENT(mgbuf_open_close, mgbuf_deinit,
	     TP_PROTO(struct mgbuf *mgbuf),
	     TP_ARGS(mgbuf)
	);
// free_res
TRACE_EVENT(mgbuf_free_res,
	    TP_PROTO(struct mgbuf *mgbuf),
	    TP_ARGS(mgbuf),
	    TP_STRUCT__entry(
		    __dynamic_array(char, name, MGBUF_NAME_LEN)
		    ),
	    TP_fast_assign(
		    snprintf(__get_str(name), MGBUF_NAME_LEN, "%s",
			     mgbuf->name);
		    ),
	    TP_printk("%s:", __get_str(name))
	);
// init
TRACE_EVENT(mgbuf_init,
	    TP_PROTO(struct mgbuf *mgbuf, const char *hostname,
		     struct usb_ep *oep, size_t oplen, unsigned int onum,
		     struct usb_ep *iep, size_t iplen, unsigned int inum),
	    TP_ARGS(mgbuf, hostname, oep, oplen, onum, iep, iplen, inum),
	    TP_STRUCT__entry(
		    __dynamic_array(char, name, MGBUF_NAME_LEN)
		    __field(struct usb_ep *, oep)
		    __field(struct usb_ep *, iep)
		    __field(size_t, oplen)
		    __field(size_t, iplen)
		    __field(unsigned int, onum)
		    __field(unsigned int, inum)
		    __field(unsigned int, flags)
		    __field(unsigned int, refcount)
		    ),
	    TP_fast_assign(
		    snprintf(__get_str(name), MGBUF_NAME_LEN, "%s",
			     hostname);
		    __entry->oep = oep;
		    __entry->iep = iep;
		    __entry->oplen = oplen;
		    __entry->iplen = iplen;
		    __entry->onum = onum;
		    __entry->inum = inum;
		    __entry->flags = mgbuf->flags;
		    __entry->refcount = mgbuf->ref.counter;
		    ),
	    TP_printk("%s: oep %p iep %p oplen %d iplen %d onum %d inum %d flag %x refcount=%d",
		      __get_str(name),
		      __entry->oep, __entry->iep, __entry->oplen, __entry->iplen,
		      __entry->onum, __entry->inum,
		      __entry->flags, __entry->refcount)
	);
// ioctl
TRACE_EVENT(mgbuf_ioctl_blkout,
	    TP_PROTO(struct mgbuf *mgbuf, struct mgbuf_dmabuf *dmabuf,
		     unsigned int cmd, struct mgbuf_block_param *param, int ret),
	    TP_ARGS(mgbuf, dmabuf, cmd, param, ret),
	    TP_STRUCT__entry(
		    __dynamic_array(char, name, MGBUF_NAME_LEN)
		    __field(struct mgbuf *, mgbuf)
		    __field(struct mgbuf_dmabuf *, dmabuf)
		    __field(unsigned int, cmd)
		    __field(int, status)
		    __field(size_t, validlen)
		    __field(void *, private)
		    __field(int, ret)
		    ),
	    TP_fast_assign(
		    snprintf(__get_str(name), MGBUF_NAME_LEN, "%s",
			     mgbuf->name);
		    __entry->mgbuf = mgbuf;
		    __entry->dmabuf = dmabuf;
		    __entry->cmd = cmd;
		    __entry->status = param->status;
		    __entry->validlen = param->validlen;
		    __entry->private = param->priv;
		    __entry->ret = ret;
		    ),
	    TP_printk("%s: ioctl(block_%s) dmabuf %p stat %d valid %d priv %p ret %d",
		      __get_str(name),
		      __entry->cmd == MGBUF_IOCTL_GET_MEM_BLOCK ? "get" : "put",
		      __entry->dmabuf,
		      __entry->status, __entry->validlen,
		      __entry->private, __entry->ret
		    )
	);

// generic string log
#define MGBUF_LOG_LEN  128
DECLARE_EVENT_CLASS(mgbuf_log,
		    TP_PROTO(struct va_format *vaf),
		    TP_ARGS(vaf),
		    TP_STRUCT__entry(
			    __dynamic_array(char, msg, MGBUF_LOG_LEN)
			    ),
		    TP_fast_assign(
			    vsnprintf(__get_str(msg), MGBUF_LOG_LEN, vaf->fmt, *vaf->va);
			    ),
		    TP_printk("%s", __get_str(msg))
	);

DEFINE_EVENT(mgbuf_log, mgbuf_msg,
	     TP_PROTO(struct va_format *vaf),
	     TP_ARGS(vaf)
	);

#endif // __MGBUF_TRACE_H

#undef TRACE_INCLUDE_PATH
#define TRACE_INCLUDE_PATH .

#undef TRACE_INCLUDE_FILE
#define TRACE_INCLUDE_FILE ../../../cinema_player/kernel_module/morpheus_bridge/mgbuf_trace

#include <trace/define_trace.h>
