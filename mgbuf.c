/*
 * Copyright (C) 2016 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <linux/debugfs.h>
#include <linux/delay.h>
#include "mgbuf.h"
#include "mgbuf_trace.h"
/*
 * TODO:
 * - more descriptive queue status
 */
#if 0
// enable if mgbuf_log trace used
static void mgbuf_trace(void (*trace)(struct va_format *), const char *fmt, ...)
{
	struct va_format vaf;
	va_list args;

	va_start(args, fmt);
	vaf.fmt = fmt;
	vaf.va = &args;
	trace(&vaf);
	va_end(args);
}
#endif

static int list_count(struct list_head *l)
{
	int ret = 0;
	struct list_head *p;
	list_for_each(p, l)
		ret++;
	return ret;
}

#ifdef CONFIG_MBRIDGE_DEBUG
static int mgbuf_queuestat_show(struct seq_file *s, void *unused)
{
	struct mgbuf *mgbuf = s->private;
	enum mgbuf_dir dir;
	unsigned long flags;
	int l1, l2, l3;

	for (dir = MGBUF_OUT; dir <= MGBUF_IN; dir++) {
		if (!mgbuf->ep[dir])
			continue;
		spin_lock_irqsave(&mgbuf->lock[dir], flags);
		l1 = list_count(&mgbuf->filledlist[dir]);
		l2 = list_count(&mgbuf->freelist[dir]);
		l3 = list_count(&mgbuf->busylist[dir]);
		spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
		seq_printf(s,"%s:comp=%d zlp=%d fil=%d free=%d busy=%d "
			   "wake=%d woken=%d call=%d "
			   "bfill=%d time=%lld \n",
			   (dir == MGBUF_OUT)? "OUT":"IN",
			   mgbuf->completed[dir],
			   mgbuf->zlp[dir],
			   l1, l2, l3,
			   mgbuf->wakeup[dir], mgbuf->woken[dir], mgbuf->irw[dir],
			   mgbuf->busyfill[dir],
			   ktime_to_ns(mgbuf->lastcb[dir]));
	}
	return 0;
} // queustat_show

static int mgbuf_queuestat_open(struct inode *inode, struct file *file)
{
	return single_open(file, mgbuf_queuestat_show, inode->i_private);
}

static const struct file_operations mgbuf_queuestat_fops = {
	.open = mgbuf_queuestat_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};
#endif // CONFIG_MBRIDGE_DEBUG

/* move a dmabuf to other list */
static void _mgbuf_move_dmabuf(struct mgbuf_dmabuf *dmabuf, struct list_head *tolist)
{
	/* remove it from the list */
	list_move_tail(&dmabuf->list, tolist);
} /* _mgbuf_move_dmabuf */

static void mgbuf_move_dmabuf(struct mgbuf_dmabuf *dmabuf, struct list_head *tolist)
{
	struct mgbuf *mgbuf;
	unsigned long flags;
	enum mgbuf_dir dir;

	mgbuf = dmabuf->mgbuf;
	dir = dmabuf->dir;

	spin_lock_irqsave(&mgbuf->lock[dir], flags);
	_mgbuf_move_dmabuf(dmabuf, tolist);
	spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
} /* mgbuf_move_dmabuf */

/* put an alone dmabuf to the list */
static void _mgbuf_put_dmabuf(struct mgbuf_dmabuf *dmabuf, struct list_head *tolist)
{
	list_add_tail(&dmabuf->list, tolist);
} /* mgbuf_put_dmabuf */

static void mgbuf_put_dmabuf(struct mgbuf_dmabuf *dmabuf, struct list_head *tolist)
{
	struct mgbuf *mgbuf;
	unsigned long flags;
	enum mgbuf_dir dir;

	mgbuf = dmabuf->mgbuf;
	dir = dmabuf->dir;

	spin_lock_irqsave(&mgbuf->lock[dir], flags);
	_mgbuf_put_dmabuf(dmabuf, tolist);
	spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
} /* mgbuf_put_dmabuf */

static struct mgbuf_dmabuf *mgbuf_peek_dmabuf(struct mgbuf *mgbuf, enum mgbuf_dir dir,
					      struct list_head *fromlist)
{
	struct mgbuf_dmabuf *dmabuf;
	unsigned long flags;

	spin_lock_irqsave(&mgbuf->lock[dir], flags);

	/* remove it from the list */
	dmabuf = list_first_entry_or_null(fromlist,
					  struct mgbuf_dmabuf, list);

	spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
	return dmabuf;
} /* mgbuf_peek_dmabuf */

/* get a dmabuf from list */
static struct mgbuf_dmabuf *_mgbuf_get_dmabuf(struct mgbuf *mgbuf,
					     struct list_head *fromlist)
{
	struct mgbuf_dmabuf *dmabuf;

	/* remove it from the list */
	dmabuf = list_first_entry_or_null(fromlist,
					  struct mgbuf_dmabuf, list);
	if (dmabuf)
		list_del(&dmabuf->list);

	return dmabuf;
} /* _mgbuf_get_dmabuf */

static struct mgbuf_dmabuf *mgbuf_get_dmabuf(struct mgbuf *mgbuf, enum mgbuf_dir dir,
					     struct list_head *fromlist)
{
	struct mgbuf_dmabuf *dmabuf;
	unsigned long flags;

	spin_lock_irqsave(&mgbuf->lock[dir], flags);

	/* remove it from the list */
	dmabuf = _mgbuf_get_dmabuf(mgbuf, fromlist);

	spin_unlock_irqrestore(&mgbuf->lock[dir], flags);

	return dmabuf;
} /* mgbuf_get_dmabuf */

static int mgbuf_ep_is_isoc(struct mgbuf *mgbuf, enum mgbuf_dir dir)
{
	return mgbuf->isoc[dir];
}

#define mgbuf_ep_queue(dmabuf, gfp_flags) usb_ep_queue(dmabuf->ep, dmabuf->req, gfp_flags)
#define mgbuf_ep_dequeue(dmabuf) usb_ep_dequeue(dmabuf->ep, dmabuf->req)

/* OUT ep completed */
static void mgbuf_ep_complete_out(struct usb_ep *ep, struct usb_request *req)
{
	struct mgbuf_dmabuf *dmabuf;
	struct mgbuf *mgbuf;
	const enum mgbuf_dir dir = MGBUF_OUT;
	unsigned long flags;
	int qret;

	dmabuf = req->context;
	mgbuf = dmabuf->mgbuf;

	trace_mgbuf_complete(dmabuf, ep, req);

	atomic_dec(&mgbuf->busycount[dir]);

	mgbuf->lastcb[dir] = ktime_get();

	dmabuf->status = req->status;
	if (unlikely(dmabuf->status)) {
		mgbuf->stat_error[dir]++;
		dmabuf->validlen = 0;
	} else {
		dmabuf->validlen = req->actual;
	}

	mgbuf->completed[dir]++;   // debug

	switch (dmabuf->status) {
	case -ECONNRESET:
		// request dequeued
		mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
		break;
	case -ESHUTDOWN:
		// usb_request queued in UDC gets ESHUTDOWN if EP is disabled
		mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
		break;
	default:
		// intentionally fall through
		// errors except disconnection are passed to the user
	case 0:
		if (!dmabuf->validlen)     // debug
			mgbuf->zlp[dir]++; // debug
		// if being quiescent or disconnected, ignore the result of this request
		if (test_bit(MGBUF_FLAG_DISCONN, &mgbuf->flags)) {
			mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
			break;
		}
		if (mgbuf_ep_is_isoc(mgbuf, dir)) {
			struct mgbuf_dmabuf *n, *newdmabuf;

			spin_lock_irqsave(&mgbuf->lock[dir], flags);
			// save the dmabuf given back if not ZLP
			if (dmabuf->validlen) {
				_mgbuf_move_dmabuf(dmabuf, &mgbuf->filledlist[dir]);
				mgbuf->wakeup[dir]++;
				wake_up_interruptible(&mgbuf->waitq[dir]);
			} else {
				_mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
			}

			// To keep underlaying UDC busy as much as possible
			list_for_each_entry_safe(newdmabuf, n,
						 &mgbuf->freelist[dir], list) {
				// dwc3 may get mad if requests over QUEUE_MAX are queued
				if (QUEUE_MAX < atomic_inc_return(&mgbuf->busycount[dir])) {
					atomic_dec(&mgbuf->busycount[dir]);
					break;
				}
				_mgbuf_move_dmabuf(newdmabuf, &mgbuf->busylist[dir]);
				qret = mgbuf_ep_queue(newdmabuf, GFP_ATOMIC);
				if (qret) {
					pr_err("%s:%s can't queue OUT ep request %d\n",
					       __func__, mgbuf->name, qret);
					_mgbuf_move_dmabuf(newdmabuf, &mgbuf->freelist[dir]);
					atomic_dec(&mgbuf->busycount[dir]);
					break;
				}
			}
			// dwc3 may issue END_TRANAFER if the count of queued
			// request gets under 2.
			// To prevent this, sacrifice the oldest valid buffer
			if (atomic_read(&mgbuf->busycount[dir]) <= 2) {
				mgbuf->busyfill[dir]++;
				newdmabuf = _mgbuf_get_dmabuf(mgbuf, &mgbuf->filledlist[dir]);
				BUG_ON(!newdmabuf);
				if (newdmabuf) {
					atomic_inc(&mgbuf->busycount[dir]);
					_mgbuf_put_dmabuf(newdmabuf, &mgbuf->busylist[dir]);
					qret = mgbuf_ep_queue(newdmabuf, GFP_ATOMIC);
					if (qret) {
						pr_err("%s:%s can't queue OUT ep request %d\n",
						       __func__, mgbuf->name, qret);
						_mgbuf_move_dmabuf(newdmabuf, &mgbuf->freelist[dir]);
						atomic_dec(&mgbuf->busycount[dir]);
					}
				}
			}
			spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
		} else {
			// For non iso OUT, no need to keep busy the controller,
			// because if no request queued it will NAK/NRDY
			mgbuf_move_dmabuf(dmabuf, &mgbuf->filledlist[dir]);
			// wake up the reader
			mgbuf->wakeup[dir]++;
			wake_up_interruptible(&mgbuf->waitq[dir]);
		}
		break;
	}
	// callback morpheus USB function
	if (mgbuf->complete[dir])
		mgbuf->complete[dir](mgbuf, ep, req);
} /* mgbuf_ep_complete_out */

static void mgbuf_ep_complete_in(struct usb_ep *ep, struct usb_request *req)
{
	struct mgbuf_dmabuf *dmabuf;
	struct mgbuf *mgbuf;
	const enum mgbuf_dir dir = MGBUF_IN;

	dmabuf = req->context;
	mgbuf = dmabuf->mgbuf;

	trace_mgbuf_complete(dmabuf, ep, req);

	atomic_dec(&mgbuf->busycount[dir]);

	mgbuf->lastcb[dir] = ktime_get();

	mgbuf->completed[dir]++;   // debug

	if (unlikely(req->status))
		mgbuf->stat_error[dir]++;

	switch (req->status) {
	case -ECONNRESET:
		/* request dequeued */
		mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
		wake_up_interruptible(&dmabuf->waitq); /* sync io */
		break;
	case -ESHUTDOWN:
		/* usb_request queued in UDC gets ESHUTDOWN if EP is disabled */
		mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
		wake_up_interruptible(&dmabuf->waitq); /* sync io */
		break;
	default:
		pr_debug("%s: err=%d\n", __func__, dmabuf->status);
		// intentionally fall through
	case 0:
		/* mode it from busy to freelist */
		mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);

		/* wake up waiter */
		mgbuf->wakeup[dir]++;
		wake_up_interruptible(&mgbuf->waitq[dir]); /* freelist */
		wake_up_interruptible(&dmabuf->waitq); /* sync io */
		break;
	};
	/* callback morpheus USB function */
	if (mgbuf->complete[dir])
		mgbuf->complete[dir](mgbuf, ep, req);
} /* mgbuf_complete_in */

// Pre-queue requests to underlaying UDC
// NOTE: dwc3 never queues any requests to hw and
// then transfers never happens after that if
// all the following condition are met:
// - the host has already issued an IN/OUT to the EP
// - EP is isochronous
// - no requests are queued to dwc3
// To avoid this, issue dummy (ZLP) request(s) to dwc3
// before the host starts to issue
// Cf. Synopsys datasheet table6-52 XferNotReady event
int mgbuf_pre_queue(struct mgbuf *mgbuf, int start_out, int start_in)
{
	enum mgbuf_dir dir;
	struct mgbuf_dmabuf *dmabuf, *n;
	unsigned long flags;
	int ret = 0;
	pr_debug("%s:%s start out=%d in=%d\n", __func__, mgbuf->name,
		 start_out, start_in);

	if (!test_bit(MGBUF_FLAG_INIT, &mgbuf->flags))
		return -EPERM;

	// start all OUT requests (if we have)
	dir = MGBUF_OUT;

	mgbuf->completed[dir] = 0;
	mgbuf->zlp[dir] = 0;

	if (mgbuf->ep[dir] && start_out) {
		spin_lock_irqsave(&mgbuf->lock[dir],flags);
		list_for_each_entry_safe(dmabuf, n,
					 &mgbuf->freelist[dir], list) {
			BUG_ON(!dmabuf->req);
			if (QUEUE_MAX < atomic_inc_return(&mgbuf->busycount[dir])) {
				atomic_dec(&mgbuf->busycount[dir]);
				break;
			}
			_mgbuf_move_dmabuf(dmabuf, &mgbuf->busylist[dir]);
			ret = mgbuf_ep_queue(dmabuf, GFP_ATOMIC);
			if (ret) {
				pr_err("%s:%s can't queue OUT ep request %d\n",
				       __func__, mgbuf->name, ret);
				_mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
				spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
				goto rewind;
			}
		} // list_for_each_entry_safe
		spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
	}

	// start one IN request (if we have)
	dir = MGBUF_IN;

	mgbuf->completed[dir] = 0;
	mgbuf->zlp[dir] = 0;

	if (mgbuf->ep[dir] && start_in) {
		spin_lock_irqsave(&mgbuf->lock[dir],flags);
		dmabuf = _mgbuf_get_dmabuf(mgbuf, &mgbuf->freelist[dir]);
		if (dmabuf) {
			dmabuf->req->length = 0; // prequeued request issues ZLP
			atomic_inc(&mgbuf->busycount[dir]);
			_mgbuf_put_dmabuf(dmabuf, &mgbuf->busylist[dir]);
			ret = mgbuf_ep_queue(dmabuf, GFP_ATOMIC);
			if (ret) {
				pr_err("%s:%s can't queue IN ep request!! %d\n",
				       __func__, mgbuf->name, ret);
				_mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
				atomic_dec(&mgbuf->busycount[dir]);
				spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
				goto rewind;
			}
		}
		spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
	}
	return 0;
rewind:
	/* FIXME: rewind enqueued requests if error */
	pr_debug("%s:%s end abnormally %d\n", __func__, mgbuf->name, ret);
	return ret;
} /* mgbuf_pre_queue */

/*
 * should be called from host's open() callback
 */
int mgbuf_open(struct mgbuf *mgbuf, struct inode *inode, struct file *file)
{

	pr_debug("%s:%s start %lx %d\n", __func__, mgbuf->name, mgbuf->flags,
		 atomic_read(&mgbuf->ref));

	trace_mgbuf_open(mgbuf);

	if (!test_bit(MGBUF_FLAG_INIT, &mgbuf->flags))
		return -EPERM;

	if (test_and_set_bit(MGBUF_FLAG_OPEN, &mgbuf->flags))
		return -EBUSY;

	// caller should do already
	// file->private_data = mgbuf;

	atomic_inc(&mgbuf->ref);

	return 0;
} /* mgbuf_open */

// Release memory allocated in mgbuf_init
static void mgbuf_free_res(struct mgbuf *mgbuf)
{
	enum mgbuf_dir dir;
	int i;

	pr_debug("%s:%s start %lx\n", __func__, mgbuf->name, mgbuf->flags);
	trace_mgbuf_free_res(mgbuf);

	// free request and memory
	for (dir = MGBUF_OUT; dir <= MGBUF_IN; dir++) {
		if (mgbuf->dmabufs[dir]) {
			// release usb_request
			for (i = 0; i < mgbuf->items[dir]; i++) {
				if (mgbuf->dmabufs[dir][i].req) {
					usb_ep_free_request(mgbuf->ep[dir],
							    mgbuf->dmabufs[dir][i].req);
					mgbuf->dmabufs[dir][i].req = NULL;
				}
			}
			// release dmabuffer
			kfree(mgbuf->dmabufs[dir]);
			mgbuf->dmabufs[dir] = NULL;
		}
		// release buffer pages for this direction
		if (mgbuf->vaddr[dir]) {
			free_pages((unsigned long)mgbuf->vaddr[dir],
				   get_order(mgbuf->packetlen[dir] * mgbuf->items[dir]));
			mgbuf->vaddr[dir] = NULL;
		}
	}
	pr_debug("%s:%s done\n", __func__, mgbuf->name);
} // mgbuf_free_res

/*
 * get called when the node is closed
 */
int mgbuf_release(struct mgbuf *mgbuf, struct inode *inode, struct file *file)
{
	struct mgbuf_dmabuf *dmabuf, *n;
	enum mgbuf_dir dir;
	int ret = 0, i;
	unsigned long flags;

	pr_debug("%s:%s start %lx\n", __func__, mgbuf->name, mgbuf->flags);
	trace_mgbuf_release(mgbuf);

	if (!test_bit(MGBUF_FLAG_OPEN, &mgbuf->flags)) {
		pr_info("%s: %s release for unopened\n", __func__, mgbuf->name);
		return -EINVAL;
	}

	// Unblock waiters with MGBUF_FLAG_OPEN deleted
	for (dir = MGBUF_OUT; dir <= MGBUF_IN; dir++) {
		if (!mgbuf->ep[dir])
			continue;
		mgbuf->wakeup[dir]++;
		wake_up_interruptible(&mgbuf->waitq[dir]);
		// for O_DSYNC waiters
		if (mgbuf->dmabufs[dir])
			for (i = 0; i < mgbuf->items[dir]; i++)
				wake_up_interruptible(&mgbuf->dmabufs[dir][i].waitq);
	}

	for (dir = MGBUF_OUT; dir <= MGBUF_IN; dir++) {
		int timeout = 100;

		if (!mgbuf->ep[dir])
			continue;

		// wait for O_DSYNC waiters being unblocked
		mutex_lock(&mgbuf->io_mutex[dir]);
		// cancel busy requests
		// they will go to freelist in complete callback
		while ((dmabuf = mgbuf_peek_dmabuf(mgbuf, dir,
						   &mgbuf->busylist[dir])) &&
		       timeout--) {
			mgbuf_ep_dequeue(dmabuf);
			// give UDC chance to give back the request
			msleep(10);
		}

		if (atomic_read(&mgbuf->busycount[dir])) {
			pr_err("%s:%s can't clean busylist %d\n", __func__,
			       mgbuf->name,
			       atomic_read(&mgbuf->busycount[dir]));
		}

		// retrieve blocks which are held by user
		spin_lock_irqsave(&mgbuf->lock[dir], flags);
		list_for_each_entry_safe(dmabuf, n,
					 &mgbuf->uownlist[dir], list) {
			_mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
		}
		spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
		mutex_unlock(&mgbuf->io_mutex[dir]);
	}
	// if refcount reaches zero, release
	if (!atomic_dec_return(&mgbuf->ref))
		mgbuf_free_res(mgbuf);

	clear_bit(MGBUF_FLAG_OPEN, &mgbuf->flags);
	pr_debug("%s:%s end %lx %d\n", __func__, mgbuf->name, mgbuf->flags,
		 atomic_read(&mgbuf->ref));

	return ret;
} /// mgbuf_release

// return true if file IO operation can be done
// i.e. initialized and opened and connected
static bool can_fileio(struct mgbuf *mgbuf)
{
	if (test_bit(MGBUF_FLAG_INIT, &mgbuf->flags) &&
	    test_bit(MGBUF_FLAG_OPEN, &mgbuf->flags) &&
	    !test_bit(MGBUF_FLAG_DISCONN, &mgbuf->flags))
		return true;
	else
		return false;
}
// OUT EP read(2) support
ssize_t mgbuf_read(struct mgbuf *mgbuf, struct file *file, char *buffer,
		   size_t count, loff_t *ppos)
{
	struct mgbuf_dmabuf *dmabuf = NULL;
	int ret, qret;
	size_t chunk;
	const enum mgbuf_dir dir = MGBUF_OUT;
	struct list_head *from;

	trace_mgbuf_readin(mgbuf, count);

	if (unlikely(!mgbuf->ep[dir]))
		return -ENODEV;

	mgbuf->irw[dir]++;
	/* no data required */
	if (!count)
		return 0;

	if (!can_fileio(mgbuf))
		return -EPIPE;

	/* no concurrent readers */
	ret = mutex_lock_interruptible(&mgbuf->io_mutex[dir]);
	if (ret < 0)
		goto exit0;

	atomic_inc(&mgbuf->ref);
	/* Only read the first packet even if more packets available */
	from = &mgbuf->filledlist[dir];
	while (!(dmabuf = mgbuf_get_dmabuf(mgbuf, dir, from))) {
		if (file->f_flags & O_NONBLOCK) {
			ret = -EAGAIN;
			goto exit1;
		}

		/* wait for buffer being filled */
		if (wait_event_interruptible(mgbuf->waitq[dir],
					     (dmabuf = mgbuf_get_dmabuf(mgbuf,
									dir,
									from)) ||
					     !can_fileio(mgbuf))) {
			ret = -ERESTARTSYS;
			goto exit1;
		}
		if (!can_fileio(mgbuf)) {
			ret = -EPIPE;
			goto exit1;
		}
		/* OK, dmabuf has something to be back to user */
		break;
	} /* while (!dataavail) */
	mgbuf->woken[dir]++;

	/* copy data as much as possible and discard the rest */
	if (!dmabuf->status) {
		chunk = min(dmabuf->validlen, count);
		if (copy_to_user(buffer, dmabuf->vaddr, chunk))
			ret = -EFAULT;
		else
			ret = chunk;
	} else {
		ret = dmabuf->status;
	}

	// for ISO OUT, callback keeps UDC busy using free buffer
	if (mgbuf_ep_is_isoc(mgbuf, dir)) {
		mgbuf_put_dmabuf(dmabuf, &mgbuf->freelist[dir]);
	} else {
		dmabuf->req->length = mgbuf->packetlen[dir];
		if (QUEUE_MAX < atomic_inc_return(&mgbuf->busycount[dir])) {
			atomic_dec(&mgbuf->busycount[dir]);
			mgbuf_put_dmabuf(dmabuf, &mgbuf->freelist[dir]);
		} else {
			mgbuf_put_dmabuf(dmabuf, &mgbuf->busylist[dir]);
			qret = mgbuf_ep_queue(dmabuf, GFP_KERNEL);
			if (qret < 0) {
				pr_err("%s: enqueue failed %d\n", __func__, qret);
				mgbuf->error[dir]++;
				ret = qret;
				mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
				atomic_dec(&mgbuf->busycount[dir]);
			}
		}
	}
exit1:
	if (unlikely(!atomic_dec_return(&mgbuf->ref)))
		mgbuf_free_res(mgbuf);
	mutex_unlock(&mgbuf->io_mutex[dir]);
exit0:

	trace_mgbuf_readout(mgbuf, dmabuf, ret);

	return ret;
} /* mgbuf_read */

/* IN EP write(2) support */
ssize_t mgbuf_write(struct mgbuf *mgbuf, struct file *file, const char *buffer,
		    size_t count, loff_t *ppos)
{
	struct mgbuf_dmabuf *dmabuf = NULL;
	int ret, qret;
	size_t chunk;
	const enum mgbuf_dir dir = MGBUF_IN;
	struct list_head *from;

	trace_mgbuf_writein(mgbuf, count);

	if (unlikely(!mgbuf->ep[dir]))
		return -ENODEV;

	if (!can_fileio(mgbuf))
		return -EPIPE;

	mgbuf->irw[dir]++;

	/* no concurrent writers */
	ret = mutex_lock_interruptible(&mgbuf->io_mutex[dir]);
	if (ret < 0)
		goto exit0;

	atomic_inc(&mgbuf->ref);
	/* Only write one packet even if more data available */
	from = &mgbuf->freelist[dir];
	while (!(dmabuf = mgbuf_get_dmabuf(mgbuf, dir, from))) {
		if (file->f_flags & O_NONBLOCK) {
			ret = -EAGAIN;
			goto exit1;
		}

		/* wait for free buffer */
		if (wait_event_interruptible(mgbuf->waitq[dir],
					     (dmabuf = mgbuf_get_dmabuf(mgbuf,
									dir,
									from)) ||
					     !can_fileio(mgbuf))) {
			ret = -ERESTARTSYS;
			goto exit1;
		}

		if (!can_fileio(mgbuf)) {
			ret = -EPIPE;
			goto exit1;
		}
		/* OK, we have dmabuf to submit */
		break;
	} /* while (!free dmabuf) */
	mgbuf->woken[dir]++;
	/* copy data as much as possible and discard the rest */
	chunk = min(mgbuf->packetlen[dir], count);
	if (copy_from_user(dmabuf->vaddr, buffer, chunk)) {
		ret = -EFAULT;
		mgbuf_put_dmabuf(dmabuf, &mgbuf->freelist[dir]);
		goto exit1;
	} else {
		ret = chunk;
	}

	/* Enqueue write request */
	dmabuf->req->length = chunk;
	if (QUEUE_MAX < atomic_inc_return(&mgbuf->busycount[dir])) {
		    atomic_dec(&mgbuf->busycount[dir]);
		    mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
		    mgbuf->error[dir]++;
		    ret = -EBUSY;
		    goto exit1;
	}
	mgbuf_put_dmabuf(dmabuf, &mgbuf->busylist[dir]);
	qret = mgbuf_ep_queue(dmabuf, GFP_KERNEL);
	if (qret < 0) {
		mgbuf->error[dir]++;
		ret = qret;
		mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
		atomic_dec(&mgbuf->busycount[dir]);
		goto exit1;
	}

	/* wait for completion if O_DSYNC is specified */
	if (file->f_flags & O_DSYNC) {
		if (wait_event_interruptible(dmabuf->waitq,
					     (dmabuf->req->status != -EINPROGRESS) ||
					     !can_fileio(mgbuf))) {
				ret = -ERESTARTSYS;
				goto exit1;
		}

		if (!can_fileio(mgbuf)) {
			ret = -EPIPE;
			goto exit1;
		}
		ret = (dmabuf->req->status)?
			dmabuf->req->status : dmabuf->req->actual;
	}

exit1:
	if (unlikely(!atomic_dec_return(&mgbuf->ref)))
		mgbuf_free_res(mgbuf);
	mutex_unlock(&mgbuf->io_mutex[dir]);
exit0:
	trace_mgbuf_writeout(mgbuf, dmabuf, ret);
	return ret;
} /* mgbuf_write */

/*
 * poll support
 */
unsigned int mgbuf_poll(struct mgbuf *mgbuf, struct file *file, poll_table *wait)
{
	unsigned int mask;
	enum mgbuf_dir dir;

	mask = 0;

	if (mgbuf->ep[MGBUF_OUT]) {
		dir = MGBUF_OUT;
		mutex_lock(&mgbuf->io_mutex[dir]);

		poll_wait(file, &mgbuf->waitq[dir], wait);

		if (!list_empty(&mgbuf->filledlist[dir]))
			mask |= POLLIN | POLLRDNORM;

		mutex_unlock(&mgbuf->io_mutex[dir]);
	}

	if (mgbuf->ep[MGBUF_IN]) {
		dir = MGBUF_IN;
		mutex_lock(&mgbuf->io_mutex[dir]);

		poll_wait(file, &mgbuf->waitq[dir], wait);

		if (!list_empty(&mgbuf->freelist[dir]))
			mask |= POLLOUT | POLLWRNORM;

		mutex_unlock(&mgbuf->io_mutex[dir]);
	}

	trace_mgbuf_poll(mgbuf, mask);
	/* TODO: support POLLHUP */
	return mask;
} /* mgbuf_poll */

/*
 * mmap upport
 */
int mgbuf_mmap(struct mgbuf *mgbuf, struct file *filp, struct vm_area_struct *vma)
{
	struct page *page;
	unsigned long vm_start, area_size;
	enum mgbuf_dir dir;
	pgprot_t pgprot;

	if (!can_fileio(mgbuf))
		return -EPIPE;

	vm_start = vma->vm_start;

	vma->vm_flags |= VM_LOCKED | VM_DONTEXPAND; /* pinned avoiding swap and
						     * area move */

	for (dir = MGBUF_OUT; dir <= MGBUF_IN; dir++) {
		if (!mgbuf->ep[dir])
			continue;

		mgbuf->vm_start[dir] = vm_start;
		page = virt_to_page(mgbuf->vaddr[dir]);
		area_size = mgbuf->areasize[dir];

		if (dir == MGBUF_OUT)
			pgprot = PAGE_READONLY;
		else
			pgprot = PAGE_SHARED;

		if (remap_pfn_range(vma, vm_start, page_to_pfn(page), area_size,
				    pgprot)) {
			pr_err("%s: map failed for dir=%d\n", __func__, dir);
			return -EAGAIN;
		}
		vm_start += area_size;
	} /* for */

	return 0;
} /* mgbuf_mmap */

// never fails. use only sanitized safe kernel's data
static unsigned int dmabuf_to_index(struct mgbuf_dmabuf *dmabuf)
{
	return  dmabuf - dmabuf->mgbuf->dmabufs[dmabuf->dir];
}

static struct mgbuf_dmabuf *index_to_dmabuf(struct mgbuf *mgbuf,
					    enum mgbuf_dir dir,
					    unsigned int ix)
{
	struct mgbuf_dmabuf *ret;

	if (mgbuf->items[dir] <= ix)
		ret = NULL;
	else
		ret = mgbuf->dmabufs[dir] + ix;

	return ret;
}

/*
 * ioctl
 */
static long mgbuf_ioctl_handle_block(struct mgbuf *mgbuf, struct file *file,
				     unsigned int cmd, unsigned long arg)
{
	long ret, qret;
	enum mgbuf_dir dir;
	struct mgbuf_block_param param;
	struct list_head *waitlist;
	struct mgbuf_dmabuf *dmabuf;

	BUILD_BUG_ON(sizeof(arg) < sizeof(void *));

	ret = copy_from_user(&param, (void *)arg, sizeof(param));
	if (ret) {
		pr_err("%s: failed to read ioctl(%#x) parameter\n", __func__, cmd);
		return ret;
	}

	/* some sanity checks */
	if (param.version != sizeof(param)) {
		pr_err("%s: mgbuf_param size mismatched %d %d\n", __func__,
		       sizeof(param), param.version);
		return -EINVAL;
	}

	if ((param.dir != MGBUF_OUT) && (param.dir != MGBUF_IN)) {
		pr_err("%s: EP direction wrong %d\n", __func__, param.dir);
		return -EINVAL;
	}

	dir = param.dir;
	if (!mgbuf->ep[dir]) {
		pr_err("%s: EP is not associated %d\n", __func__, param.dir);
		return -EINVAL;
	}

	trace_mgbuf_ioctl_blkin(mgbuf, cmd, &param);
	ret = mutex_lock_interruptible(&mgbuf->io_mutex[dir]);
	if (ret < 0)
		return ret;

	/* Do IOCTL */
	if (cmd == MGBUF_IOCTL_GET_MEM_BLOCK) {
		if (dir == MGBUF_OUT)
			waitlist = &mgbuf->filledlist[dir];
		else
			waitlist = &mgbuf->freelist[dir];

		while (!(dmabuf = mgbuf_get_dmabuf(mgbuf, dir, waitlist))) {
			if (file->f_flags & O_NONBLOCK) {
				ret = -EAGAIN;
				goto exit;
			}

			/* wait for being filled buffer filed */
			if (wait_event_interruptible(mgbuf->waitq[dir],
						     (dmabuf = mgbuf_get_dmabuf(mgbuf,
										dir,
										waitlist)) ||
						     !can_fileio(mgbuf))) {
				ret = -ERESTARTSYS;
				goto exit;
			};
			// file closed?
			if (!can_fileio(mgbuf)) {
				ret = -EPIPE;
				goto exit;
			}
			/* Rent dmabuf to user */
			break;
		} /* while (!dataavail) */

		/* calc user space addr */
		param.bufaddr = (void *)mgbuf->vm_start[dir] + dmabuf->vm_off;
		if (dir == MGBUF_OUT) {
			param.status = dmabuf->req->status;
			param.validlen = dmabuf->validlen;
		} else {
			param.validlen = mgbuf->packetlen[dir];
		}
		// remember dmabuf
		param.priv = (void *)dmabuf_to_index(dmabuf);
		// change buffer state to 'user owned'
		mgbuf_put_dmabuf(dmabuf, &mgbuf->uownlist[dir]);

		if (copy_to_user((void *)arg, &param, sizeof(param)))
			ret = -EFAULT;
	} else if (cmd == MGBUF_IOCTL_PUT_MEM_BLOCK) {
		struct mgbuf_dmabuf *e;
		bool found = false;

		dmabuf = index_to_dmabuf(mgbuf,
					 dir, // checked userdata
					 (unsigned int)param.priv);
		if (!dmabuf) {
			pr_err("%s: specified dmabuf is broken\n", __func__);
			goto exit_put;
		}

		param.priv = NULL;
		param.status = 0;
		// check dmabuf is really in the uownlist.
		// irq code never puts dmabuf into uownlist
		// and single thread is guranteed by io_mutex,
		// so it's safe to traverse uwonlist without lock
		list_for_each_entry(e, &mgbuf->uownlist[dir], list) {
			if (e == dmabuf) {
				found =true;
				break;
			}
		} // list_for_each_entry

		if (!found) {
			pr_err("%s: specified dmabuf is not in uownlist!\n", __func__);
			ret = -EBUSY;/* TODO: or other code? */
			param.status = ret;
			goto exit_put;
		}

		if (dir == MGBUF_OUT) {
			dmabuf->req->length = mgbuf->packetlen[dir];
			mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
		} else {
			dmabuf->req->length = min(param.validlen, mgbuf->packetlen[dir]);
			// enqueue dmabuf
			if (QUEUE_MAX < atomic_inc_return(&mgbuf->busycount[dir])) {
				atomic_dec(&mgbuf->busycount[dir]);
				ret = -EBUSY;
				goto exit_put;
			}
			mgbuf_move_dmabuf(dmabuf, &mgbuf->busylist[dir]);
			qret = mgbuf_ep_queue(dmabuf, GFP_KERNEL);
			if (qret < 0) {
				mgbuf->error[dir]++;
				ret = qret;
				mgbuf_move_dmabuf(dmabuf, &mgbuf->freelist[dir]);
				atomic_dec(&mgbuf->busycount[dir]);
			}
		}

		if (dir == MGBUF_IN && (file->f_flags & O_DSYNC)) {
			/* wait for completion if O_DSYNC is specified */
			if (wait_event_interruptible(dmabuf->waitq,
						     (dmabuf->req->status != -EINPROGRESS) ||
						     !can_fileio(mgbuf))) {
				ret = -ERESTARTSYS;
				goto exit_put;
			}
			// file closed or disconnected from USB host?
			if (!can_fileio(mgbuf)) {
				ret = -EPIPE;
				goto exit_put;
			}

			param.status = dmabuf->req->status;
			param.validlen = dmabuf->req->actual;
		}
	exit_put:
		if (copy_to_user((void *)arg, &param, sizeof(param)))
			ret = -EFAULT;
	} else {
		ret = -EINVAL;
	} // if (cmd == GET/PUT_MEMBLOCK)

exit:
	mutex_unlock(&mgbuf->io_mutex[dir]);
	trace_mgbuf_ioctl_blkout(mgbuf, dmabuf, cmd, &param, ret);

	return ret;
} /* mgbuf_ioctl_hanele_block */

long mgbuf_ioctl(struct mgbuf *mgbuf, struct file *file, unsigned int cmd,
		 unsigned long arg)
{
	long ret;
	struct mgbuf_info info;
	enum mgbuf_dir dir;

	if (!can_fileio(mgbuf))
		return -EPIPE;

	atomic_inc(&mgbuf->ref);
	/* Do IOCTL */
	switch (cmd) {
	case MGBUF_IOCTL_GET_MEM_BLOCK:
	case MGBUF_IOCTL_PUT_MEM_BLOCK:
		ret = mgbuf_ioctl_handle_block(mgbuf, file, cmd, arg);
		break;
	case MGBUF_IOCTL_GET_INFO:
		ret = copy_from_user(&info, (void *)arg, sizeof(info));
		if (ret)
			goto exit;
		if (info.version != sizeof(info)) {
			ret = -EINVAL;
			goto exit;
		}
		for (dir = MGBUF_OUT; dir <= MGBUF_IN; dir++) {
			info.packetlen[dir] = mgbuf->packetlen[dir];
			info.items[dir] = mgbuf->items[dir];
			info.areasize[dir] = mgbuf->areasize[dir];
		}
		ret = copy_to_user((void *)arg, &info, sizeof(info));
		break;
	default:
		ret = -EINVAL;
	}

exit:
	if (unlikely(!atomic_dec_return(&mgbuf->ref)))
		mgbuf_free_res(mgbuf);
	return ret;
} /* mgbuf_ioctl */

int mgbuf_fsync(struct mgbuf *mgbuf, struct file *file, loff_t start, loff_t end,
		int datasync)
{
	int ret = 0;
	const enum mgbuf_dir dir = MGBUF_IN;
	const int timeout = 1000;

	if (unlikely(!mgbuf->ep[dir]))
		return -ENODEV;

	if (mgbuf_ep_is_isoc(mgbuf, dir))
		return -EOPNOTSUPP; // FIXME: what to do ?

	if (!can_fileio(mgbuf))
		return -EPIPE;

	// can't write concurrent
	ret = mutex_lock_interruptible(&mgbuf->io_mutex[dir]);
	if (ret < 0)
		return ret;

	// wait for completion all with timeout
	ret = wait_event_interruptible_timeout(mgbuf->waitq[dir],
					       (atomic_read(&mgbuf->busycount[dir]) <= 0) ||
					       !can_fileio(mgbuf),
					       msecs_to_jiffies(timeout) );
	if (!can_fileio(mgbuf))
		ret = -EPIPE;
	else if(0 < ret)
		ret = 0; // success
	else if(ret == 0)
		pr_warn("%s:%s wait busylist completion timed out. remain %d\n",
			__func__, mgbuf->name, atomic_read(&mgbuf->busycount[dir]));
	else
		pr_err("%s:%s wait busylist completion failed. %d\n",
		       __func__, mgbuf->name, ret);

	mutex_unlock(&mgbuf->io_mutex[dir]);

	return ret;
} /// mgbuf_fsync

// sudden USB disconnect support
extern void mgbuf_set_usbconnected(struct mgbuf *mgbuf, int connected)
{
	enum mgbuf_dir dir;

	pr_debug("%s:%s %s\n", __func__, mgbuf->name,
		 connected? "conn":"disconn");

	trace_mgbuf_set_usbconnected(mgbuf, connected);

	if (connected) {
		clear_bit(MGBUF_FLAG_DISCONN, &mgbuf->flags);
		return;
	}

	set_bit(MGBUF_FLAG_DISCONN, &mgbuf->flags);
	// Unblock waiters with MGBUF_CONNECTED deleted
	// O_DSYNC waiters will be woken by UDC's callback
	// (complete with usb_request.status == -ESHUTDOWN)
	for (dir = MGBUF_OUT; dir <= MGBUF_IN; dir++) {
		if (!mgbuf->ep[dir])
			continue;
		mgbuf->wakeup[dir]++;
		wake_up_interruptible(&mgbuf->waitq[dir]);
	}
};

extern int mgbuf_get_usbconnected(struct mgbuf *mgbuf)
{
	return !test_bit(MGBUF_FLAG_DISCONN, &mgbuf->flags);
};

//
// Let mgbuf queue usb_request no more
//
extern void mgbuf_make_quiescent(struct mgbuf *mgbuf)
{
	enum mgbuf_dir dir;

	set_bit(MGBUF_FLAG_DISCONN, &mgbuf->flags);

	for (dir = MGBUF_OUT; dir <= MGBUF_IN; dir++) {
		struct mgbuf_dmabuf *dmabuf, *n;
		unsigned long flags;

		// for ISO EPs, assume userspace wait for
		// completions of all outgoing usb_requests
		// as ISO requests should get completed after
		// their intervals elapsed
		if (mgbuf->dmabufs[dir] && !mgbuf_ep_is_isoc(mgbuf, dir)) {
			spin_lock_irqsave(&mgbuf->lock[dir], flags);
			list_for_each_entry_safe_reverse(dmabuf, n,
							 &mgbuf->busylist[dir], list) {
				spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
				mgbuf_ep_dequeue(dmabuf);
				spin_lock_irqsave(&mgbuf->lock[dir], flags);
			}
			spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
		}
	}

	// Wakeup waiters (with error).
	for (dir = MGBUF_OUT; dir <= MGBUF_IN; dir++) {
		if (!mgbuf->ep[dir])
			continue;
		mgbuf->wakeup[dir]++;
		wake_up_interruptible(&mgbuf->waitq[dir]);
	}
};

// make usb quiece and free usb_request
// usually call this  in function's unbind()
void mgbuf_deinit(struct mgbuf *mgbuf)
{
	enum mgbuf_dir dir;
	struct mgbuf_dmabuf *dmabuf, *n;
	unsigned long flags;

	pr_debug("%s:%s start %lx\n", __func__, mgbuf->name, mgbuf->flags);
	trace_mgbuf_deinit(mgbuf);

	if (!test_and_clear_bit(MGBUF_FLAG_INIT, &mgbuf->flags)) {
		pr_info("%s:%s Not initialized instance\n", __func__, mgbuf->name);
		return;
	}

	// dequeue all
	for (dir = MGBUF_OUT; dir <= MGBUF_IN; dir++) {
		if (mgbuf->dmabufs[dir]) {
			spin_lock_irqsave(&mgbuf->lock[dir], flags);
			list_for_each_entry_safe(dmabuf, n,
						 &mgbuf->busylist[dir], list) {
				spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
				mgbuf_ep_dequeue(dmabuf);
				spin_lock_irqsave(&mgbuf->lock[dir], flags);
			}
			spin_unlock_irqrestore(&mgbuf->lock[dir], flags);
		}
	}

#ifdef CONFIG_MBRIDGE_DEBUG
	if (mgbuf->dbgroot)
		debugfs_remove_recursive(mgbuf->dbgroot);
#endif

	// free usb_request and memory if there are no file users
	if (!atomic_dec_return(&mgbuf->ref))
		mgbuf_free_res(mgbuf);

	pr_debug("%s:%s end\n", __func__, mgbuf->name);
	strncpy(mgbuf->name, "(DEL)", sizeof(mgbuf->name));
} /* mgbuf_deinit */

// initialize and allocate usb_request
// usually call this in bind() or set_alt()
static int mgbuf_init_one(struct mgbuf *mgbuf, struct usb_ep *ep, size_t packetlen,
			  unsigned num, enum mgbuf_dir dir, int isoc, bool do_alloc)
{
	int i;
	int ret;

	BUG_ON(!ep);

	if (!packetlen || ep->maxpacket_limit < packetlen ||
	    4096 < packetlen)
		return -EINVAL;

	/* need at least one buffer */
	if (!num)
		return -EINVAL;

	/*
	 * ensure packetlen is a power of 2 so that
	 * any single buffer can not cross 4K boundary
	 * due to the dwc3 restriction
	 */
	for (i = 1; i < sizeof(size_t) * 8; i++) {
		if (packetlen & ((size_t)1 << i))
			break;
	}
	if (packetlen & ~((size_t)1 << i))
		return -EINVAL;

	mgbuf->packetlen[dir] = packetlen;
	mgbuf->items[dir] = num;

	spin_lock_init(&mgbuf->lock[dir]);
	INIT_LIST_HEAD(&mgbuf->freelist[dir]);
	INIT_LIST_HEAD(&mgbuf->filledlist[dir]); // OUT only
	INIT_LIST_HEAD(&mgbuf->busylist[dir]);
	INIT_LIST_HEAD(&mgbuf->uownlist[dir]);
	atomic_set(&mgbuf->busycount[dir], 0);
	mutex_init(&mgbuf->io_mutex[dir]);
	mgbuf->isoc[dir] = isoc;

	/* need page aligned memories to avoid complicated handling for mmap */
	mgbuf->order[dir] = get_order(packetlen * num);
	mgbuf->areasize[dir] = (1 << PAGE_SHIFT) * (1 << mgbuf->order[dir]);
	if (do_alloc)
		mgbuf->vaddr[dir] = (void *)__get_free_pages(GFP_KERNEL,
							     mgbuf->order[dir]);
	if (!mgbuf->vaddr[dir]) {
		pr_err("%s: failed allocating for 2**%d pages (%d bytes)\n", __func__,
		       get_order(packetlen * num), packetlen * num);
		return -ENOMEM;
	}

	/* allocate management structs for DMA buffer segments */
	if (do_alloc)
		mgbuf->dmabufs[dir] = kzalloc(sizeof(struct mgbuf_dmabuf) * num,
					      GFP_KERNEL);
	if (!mgbuf->dmabufs[dir]) {
		pr_err("%s: failed allocating memoryn", __func__);
		return -ENOMEM;
		goto rewind;
	}

	init_waitqueue_head(&mgbuf->waitq[dir]);

	for (i = 0; i < num; i++) {
		struct usb_request *req;
		struct mgbuf_dmabuf *dmabuf;

		dmabuf = &(mgbuf->dmabufs[dir][i]);
		dmabuf->mgbuf = mgbuf;
		INIT_LIST_HEAD(&dmabuf->list);
		init_waitqueue_head(&dmabuf->waitq);

		/* allocate usb_request for this chunk */
		if (do_alloc)
			req = usb_ep_alloc_request(ep, GFP_KERNEL);
		else
			req = dmabuf->req;

		if (!req) {
			pr_err("%s: failed allocating usb_req\n", __func__);
			ret = -ENOMEM;
			goto rewind;
		}

		dmabuf->dir = dir;
		dmabuf->req = req;
		dmabuf->ep = ep;
		req->context = dmabuf;
		req->length = mgbuf->packetlen[dir];// OUT only?

		if (dir == MGBUF_OUT)
			req->complete = mgbuf_ep_complete_out;
		else
			req->complete = mgbuf_ep_complete_in;

		/* buffer pointer */
		dmabuf->vm_off = (unsigned long)(packetlen * i);
		dmabuf->vaddr = mgbuf->vaddr[dir] + dmabuf->vm_off;
		req->buf = dmabuf->vaddr;
		pr_debug("%s:%s dmabuf=%p req=%p iobuff=%p\n", __func__, mgbuf->name,
			 dmabuf, req, dmabuf->vaddr);
		/* OK insert it freelist */
		mgbuf_put_dmabuf(dmabuf, &mgbuf->freelist[dir]);
	} /* for each mgbuf_dmabuf */

	/* this direction is initialized */
	mgbuf->ep[dir] = ep;
	return 0;

rewind:
	// TODO: rewind in safe way
	// mgbuf_deinit(mgbuf);
	return ret;
} /* mgbuf_init_one */

/*
 * allocate resources and initialize
 * mgbuf: struct mgbuf to be initialized
 * name: ID name for this instance. Just for debugging
 * iep: EP for IN. Can be NULL if OUT only function
 * oep: EP for OUT. Can be NULL if IN only function
 * ipacketlen: data size of transfer unit in bytes
 * opacketlen: "
 * inum: number of transfer unit to be allocated
 * onum: "
 */
int mgbuf_init(struct mgbuf *mgbuf, const char *name,
	       struct usb_ep *oep, size_t opacketlen, unsigned int onum, int oisoc,
	       struct usb_ep *iep, size_t ipacketlen, unsigned int inum, int iisoc)
{
	int ret = 0;
	bool do_alloc;
#ifdef CONFIG_MBRIDGE_DEBUG
	char dbgname[256];
	struct dentry *file;
#endif

	BUG_ON(!mgbuf);

	trace_mgbuf_init(mgbuf, name,
			 oep, opacketlen, onum, iep, ipacketlen, inum);

	pr_debug("%s:%s start %lx\n", __func__, name, mgbuf->flags);
	if (iep)
		pr_debug("%s:%s iplen=%d num=%d\n", __func__, name, ipacketlen, inum);
	if (oep)
		pr_debug("%s:%s oplen=%d num=%d\n", __func__, name, opacketlen, onum);

	// init twice?
	if (test_bit(MGBUF_FLAG_INIT, &mgbuf->flags)) {
		pr_err("%s:%s already initialized instance\n", __func__, mgbuf->name);
		return -EBUSY;
	}

	if (atomic_inc_return(&mgbuf->ref) == 1) {
		do_alloc = true;
	} else {
		do_alloc = false;
	}

	if (name)
		strncpy(mgbuf->name, name, sizeof(mgbuf->name) - 1);

	if (oep)
		ret = mgbuf_init_one(mgbuf, oep, opacketlen, onum,
				     MGBUF_OUT, oisoc, do_alloc);
	if (iep && !ret)
		ret = mgbuf_init_one(mgbuf, iep, ipacketlen, inum,
				     MGBUF_IN, iisoc, do_alloc);

	if (ret)
		pr_err("%s:%s mgbuf init failed %d\n", __func__, mgbuf->name, ret);
	else
		set_bit(MGBUF_FLAG_INIT, &mgbuf->flags);

#ifdef CONFIG_MBRIDGE_DEBUG
	// debugfs
	snprintf(dbgname, sizeof(dbgname), "mgbuf.%s", mgbuf->name);
	mgbuf->dbgroot = debugfs_create_dir(dbgname, NULL);
	if (mgbuf->dbgroot) {
		file = debugfs_create_file("queuestat", S_IRUGO | S_IWUSR,
					   mgbuf->dbgroot, mgbuf,
					   &mgbuf_queuestat_fops);
	}
#endif
	pr_debug("%s:%s ended with %d\n", __func__, mgbuf->name, ret);
	return ret;
} /* mgbuf_init */

/*
 * return the number of dmabuf in the specified queue
 */
int mgbuf_get_queue_count(struct mgbuf *mgbuf, enum mgbuf_dir dir,
				 enum mgbuf_qkind qkind)
{
	unsigned long flags;
	struct list_head *qlist;
	int ret = -EINVAL;

	if (!mgbuf->ep[dir])
		return -EINVAL;

	spin_lock_irqsave(&mgbuf->lock[dir], flags);
	switch (qkind) {
	case MGBUF_Q_BUSY:
		qlist = &mgbuf->busylist[dir];
		break;
	default:
		qlist = NULL;
	}
	if (qlist)
		ret = list_count(qlist);
	spin_unlock_irqrestore(&mgbuf->lock[dir], flags);

	return ret;
} // mgbuf_get_queue_count

#define CREATE_TRACE_POINTS
#include "mgbuf_trace.h"
