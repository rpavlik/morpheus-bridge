/*
 * Copyright (C) 2016 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <asm/uaccess.h>
#include <linux/hid.h>
#include <linux/crc32.h>

#include "bridge.h"
#include "authentication.h"
#include "auth_def.h"
#include "hostdef.h"

//#define ENABLE_HAUTH_CRC_CHECK
#define ENABLE_HAUTH_DEBUG

//----------------------------------------------------------------------------
#if defined(ENABLE_MBRIDGE_AUTH_DEBUG)
#define LOG_LEVEL_AUTH (LOG_DBG)
#else
#define LOG_LEVEL_AUTH (LOG_INFO)
#endif
#define GADGET_AUTH_LOG(LEVEL, FORMAT, ...)				\
	if ((LEVEL) <= LOG_LEVEL_AUTH)					\
	{								\
		const char* file=__FILE__;				\
		if( strlen(file)>20)					\
		{							\
			file = file+strlen(file)-20;			\
		}							\
		printk("%s:%d (%s) -"FORMAT,  file, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
	}
//----------------------------------------------------------------------------

// for PS4
#define AUTH_STATUS_COMPLETE_AUTH2_DATA_CREATE	   0x00 // auth2 data generate is accomplished(enabled to response for get report)
#define AUTH_STATUS_WAIT_AUTH1_DATA_RECEIVE	   0x01 // waiting auth1 data received
#define AUTH_STATUS_CREATING_AUTH2_DATA		   0x10 // generating auth2 data
#define AUTH_STATUS_ERROR_INVALID_AUTH1_CRC	   0xF0 // auth1 CRC error
#define AUTH_STATUS_ERROR_CONTROL_ERROR		   0xF1 // error(something other reason)
#define AUTH_STATUS_ERROR_AUTH2_DATA_CREATE_FAILED 0xF2 // failed generating auth2 data

#define AUTH1_DATA_LENGTH 256
#define AUTH2_DATA_LENGTH 1040

#define REPORT_ID_SET_AUTH1_DATA  0xF0
#define REPORT_ID_GET_AUTH2_DATA  0xF1
#define REPORT_ID_GET_AUTH_STATUS 0xF2
#define REPORT_ID_GET_AUTH_CONFIG 0xF3

#define SET_AUTH_DATA_BLOCK_SIZE 56
#define GET_AUTH_DATA_BLOCK_SIZE 56
#define MAX_AUTH1_BLOCK_NUMBER	  4
#define MAX_AUTH2_BLOCK_NUMBER	 18
#define AUTHENTICATION_CONFIG_LENGTH 0x08

// Host Auth
#define REPORT_ID_HAUTH_SET_HOST_HELLO	    0xD0
#define REPORT_ID_HAUTH_SET_START	    0xD1
#define REPORT_ID_HAUTH_SET_MSG2	    0xD2
#define REPORT_ID_HAUTH_SET_DATA	    0xD3

#define REPORT_ID_HAUTH_GET_DEV_HELLO_0	    0xE0
#define REPORT_ID_HAUTH_GET_DEV_HELLO_1	    0xE1
#define REPORT_ID_HAUTH_GET_MSG1_0	    0xE2
#define REPORT_ID_HAUTH_GET_MSG1_1	    0xE3
#define REPORT_ID_HAUTH_GET_MSG1_2	    0xE4
#define REPORT_ID_HAUTH_GET_MSG1_3	    0xE5
#define REPORT_ID_HAUTH_GET_MSG1_4	    0xE6
#define REPORT_ID_HAUTH_GET_MSG1_5	    0xE7
#define REPORT_ID_HAUTH_GET_STATUS_0	    0xE8
#define REPORT_ID_HAUTH_GET_STATUS_1	    0xE9
// yaranai
#define REPORT_ID_HAUTH_GET_ACK		    0xEF

#define HAUTH_CRC_TARGET_LENGTH		    60
#define HAUTH_NAK			    0x00
#define HAUTH_ACK			    0xFF

#define HAUTH_FLAG_MASK_HELLO		    0x03
#define HAUTH_FLAG_MASK_START		    0x03
#define HAUTH_FLAG_MASK_MSG2		    0x03
#define HAUTH_FLAG_MASK_DATA		    0x03

// for descriptor
#define AUTHENTICATION_ENDPOINT_SOURCE_REQUEST_SIZE        64
#define AUTHENTICATION_ENDPOINT_SINK_REQUEST_SIZE          64

#define AUTHENTICATION_REPORT_DESC_LENGTH (159)

/* USB Descriptors */
static struct usb_interface_descriptor auth_intf = {
	.bLength	     = sizeof (auth_intf),
	.bDescriptorType     = USB_DT_INTERFACE,
	.bAlternateSetting   = 0,
	.bNumEndpoints	     = 2,
	.bInterfaceClass     = USB_CLASS_HID,
	.bInterfaceNumber    = MORPHEUS_AUTHENTICATION_INTERFACE,
};

static struct hid_descriptor auth_hid_desc = {
	.bLength	     = sizeof (auth_hid_desc),
	.bDescriptorType     = HID_DT_HID,
	.bcdHID		     = 0x0111,
	.bCountryCode	     = 0x00,
	.bNumDescriptors     = 0x1,
};

static struct usb_endpoint_descriptor fs_auth_source_desc = {
	.bLength	     = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType     = USB_DT_ENDPOINT,
	.bEndpointAddress    = MORPHEUS_AUTHENTICATION_ENDPOINT_SOURCE | USB_DIR_IN,
	.bmAttributes	     = USB_ENDPOINT_XFER_INT,
	.wMaxPacketSize	     = cpu_to_le16(AUTHENTICATION_ENDPOINT_SOURCE_REQUEST_SIZE),
	.bInterval	     = 1, /* 1ms */
};

static struct usb_endpoint_descriptor hs_auth_source_desc = {
	.bLength	     = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType     = USB_DT_ENDPOINT,
	.bEndpointAddress    = MORPHEUS_AUTHENTICATION_ENDPOINT_SOURCE | USB_DIR_IN,
	.bmAttributes	     = USB_ENDPOINT_XFER_INT,
	.wMaxPacketSize	     = cpu_to_le16(AUTHENTICATION_ENDPOINT_SOURCE_REQUEST_SIZE),
	.bInterval	     = 4, /* 1ms */
};

static struct usb_endpoint_descriptor fs_auth_sink_desc = {
	.bLength	     = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType     = USB_DT_ENDPOINT,
	.bEndpointAddress    = MORPHEUS_AUTHENTICATION_ENDPOINT_SINK | USB_DIR_OUT,
	.bmAttributes	     = USB_ENDPOINT_XFER_INT,
	.wMaxPacketSize	     = cpu_to_le16(AUTHENTICATION_ENDPOINT_SINK_REQUEST_SIZE),
	.bInterval	     = 1, /* 1ms */
};

static struct usb_endpoint_descriptor hs_auth_sink_desc = {
	.bLength	     = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType     = USB_DT_ENDPOINT,
	.bEndpointAddress    = MORPHEUS_AUTHENTICATION_ENDPOINT_SINK | USB_DIR_OUT,
	.bmAttributes	     = USB_ENDPOINT_XFER_INT,
	.wMaxPacketSize	     = cpu_to_le16(AUTHENTICATION_ENDPOINT_SINK_REQUEST_SIZE),
	.bInterval	     = 4, /* 1ms */
};

static struct usb_descriptor_header *fs_auth_descs[] = {
	(struct usb_descriptor_header *)&auth_intf,
	(struct usb_descriptor_header *)&auth_hid_desc,
	(struct usb_descriptor_header *)&fs_auth_source_desc,
	(struct usb_descriptor_header *)&fs_auth_sink_desc,
	NULL
};

static struct usb_descriptor_header *hs_auth_descs[] = {
	(struct usb_descriptor_header *)&auth_intf,
	(struct usb_descriptor_header *)&auth_hid_desc,
	(struct usb_descriptor_header *)&hs_auth_source_desc,
	(struct usb_descriptor_header *)&hs_auth_sink_desc,
	NULL
};

static const char AuthenticationReportDescriptor[AUTHENTICATION_REPORT_DESC_LENGTH] =
{
	0x06, 0xf0, 0xff,
	0x15, 0x00,
	0x26, 0xff, 0x00,
	0x75, 0x08,
	0x95, 0x3f,

	0x09, 0x01,
	0xa1, 0x01,
	0x85, 0xd0,
	0x09, 0xd0,
	0xb1, 0x02,

	0x85, 0xd1,
	0x09, 0xd1,
	0xb1, 0x02,

	0x85, 0xd2,
	0x09, 0xd2,
	0xb1, 0x02,

	0x85, 0xd3,
	0x09, 0xd3,
	0xb1, 0x02,
	0xc0,

	0x09, 0x02,
	0xa1, 0x01,
	0x85, 0xe0,
	0x09, 0xe0,
	0xb1, 0x02,

	0x85, 0xe1,
	0x09, 0xe1,
	0xb1, 0x02,

	0x85, 0xe2,
	0x09, 0xe2,
	0xb1, 0x02,

	0x85, 0xe3,
	0x09, 0xe3,
	0xb1, 0x02,

	0x85, 0xe4,
	0x09, 0xe4,
	0xb1, 0x02,

	0x85, 0xe5,
	0x09, 0xe5,
	0xb1, 0x02,

	0x85, 0xe6,
	0x09, 0xe6,
	0xb1, 0x02,

	0x85, 0xe7,
	0x09, 0xe7,
	0xb1, 0x02,

	0x85, 0xe8,
	0x09, 0xe8,
	0xb1, 0x02,

	0x85, 0xe9,
	0x09, 0xe9,
	0xb1, 0x02,

	0x85, 0xef,
	0x09, 0xef,
	0x95, 0x04,
	0xb1, 0x02,
	0xc0,

	0x09, 0x40,
	0xa1, 0x01,
	0x85, 0xf0,
	0x09, 0x47,
	0x15, 0x00,
	0x26, 0xff, 0x00,
	0x75, 0x08,
	0x95, 0x3f,
	0xb1, 0x02,

	0x85, 0xf1,
	0x09, 0x48,
	0x95, 0x3f,
	0xb1, 0x02,

	0x85, 0xf2,
	0x09, 0x49,
	0x95, 0x0f,
	0xb1, 0x02,

	0x85, 0xf3,
	0x0a, 0x01, 0x47,
	0x95, 0x07,
	0xb1, 0x02,
	0xc0
};
/* USB Descriptors */

#pragma pack(1)
struct mbridge_auth_config {
	uint8_t report_id;
	uint8_t reserved1;
	uint8_t block_size_for_set_auth;
	uint8_t block_size_for_get_auth;
	uint8_t reserved2[4];
};

static struct mbridge_auth_config s_auth_config = {
	.report_id = REPORT_ID_GET_AUTH_CONFIG,
	.reserved1 = 0x00,
	.block_size_for_set_auth = SET_AUTH_DATA_BLOCK_SIZE,
	.block_size_for_get_auth = GET_AUTH_DATA_BLOCK_SIZE,
	.reserved2 = {0, 0, 0, 0},
};

struct mbridge_auth_status {
	uint8_t report_id;
	uint8_t sequence_number;
	uint8_t status;
	uint8_t reserved[9];
	uint8_t crc[4];
};

struct mbridge_auth1_data {
	uint8_t report_id;
	uint8_t sequence_number;
	uint8_t block_number;
	uint8_t reserved;
	uint8_t data[SET_AUTH_DATA_BLOCK_SIZE];
	uint8_t crc[4];
};

struct mbridge_auth2_data {
	uint8_t report_id;
	uint8_t sequence_number;
	uint8_t block_number;
	uint8_t reserved;
	uint8_t data[GET_AUTH_DATA_BLOCK_SIZE];
	uint8_t crc[4];
};

// Host Authentication
struct hauth_header
{
	uint8_t r_id;
	uint8_t block_no;
	uint8_t block_total;
	uint8_t reserved;
};

struct hauth_packet
{
	struct hauth_header header;
	uint8_t msg[HAUTH_MSG_LENGTH];
	uint32_t crc32;
};

struct hauth_ack_packet
{
	uint8_t r_id;
	uint8_t block_no;
	uint8_t intended_r_id;
	uint8_t ack_status;
};
#pragma pack()

struct mbridge_auth_interface {
	struct usb_function  interface;

	struct usb_ep	     *source_ep;
	struct usb_ep	     *sink_ep;

	spinlock_t	     lock;

	uint8_t		     sequence_number;
	uint8_t		     auth1_block_number;
	uint8_t		     auth2_block_number;
	uint8_t		     status;
	uint8_t		     auth1_data[AUTH1_DATA_LENGTH+24];
	uint8_t		     auth2_data[AUTH2_DATA_LENGTH];
	int		     num_of_received;
	int		     num_of_sent;
	int		     auth_status;
	int		     error;
	int		     cancel;

	struct hauth_ack_packet hauth_ack;
	uint8_t		     hauth_host_hello_work_flag;
	uint8_t		     hauth_dev_hello_work_flag;
	uint8_t		     hauth_start_work_flag;
	uint8_t		     hauth_msg1_work_flag;
	uint8_t		     hauth_msg2_work_flag;
	uint8_t		     hauth_set_data_work_flag;
	uint8_t		     hauth_status_work_flag;

	int                  major; /* userspace device node */
	struct class        *class;
	struct device       *nodedev;
};

static struct mbridge_auth_interface *sAuth = NULL;

static uint8_t sHAuthHostHelloBuffer[HAUTH_BUFSIZE_HELLO]   = {0};
static uint8_t sHAuthDevHelloBuffer[HAUTH_BUFSIZE_HELLO]    = {0};
static uint8_t sHAuthStartBuffer[HAUTH_BUFSIZE_START]	    = {0};
static uint8_t sHAuthMsg1Buffer[HAUTH_BUFSIZE_MSG1]	    = {0};
static uint8_t sHAuthMsg2Buffer[HAUTH_BUFSIZE_MSG2]	    = {0};
static uint8_t sHAuthSetDataBuffer[HAUTH_BUFSIZE_DATA]	    = {0};
static uint8_t sHAuthStatusBuffer[HAUTH_BUFSIZE_STATUS]	    = {0};

static void mbridge_auth_init_internal(struct mbridge_auth_interface *auth_interface)
{
	// Device Authentication
	auth_interface->status = AUTH_STATUS_WAIT_AUTH1_DATA_RECEIVE;
	auth_interface->sequence_number = 0;
	auth_interface->auth1_block_number = 0;
	auth_interface->auth2_block_number = 0;
	auth_interface->num_of_received = 0;
	auth_interface->num_of_sent = 0;
	auth_interface->auth_status = AUTH_STATE_READY;
} // mbridge_auth_init_internal

static void mbridge_hauth_init_internal(struct mbridge_auth_interface *auth_interface)
{
	if (auth_interface != NULL) {
		memset((void *)&sHAuthHostHelloBuffer[0], 0, HAUTH_BUFSIZE_HELLO);
		memset((void *)&sHAuthDevHelloBuffer[0], 0, HAUTH_BUFSIZE_HELLO);
		memset((void *)&sHAuthStartBuffer[0], 0, HAUTH_BUFSIZE_START);
		memset((void *)&sHAuthMsg1Buffer[0], 0, HAUTH_BUFSIZE_MSG1);
		memset((void *)&sHAuthMsg2Buffer[0], 0, HAUTH_BUFSIZE_MSG2);
		memset((void *)&sHAuthSetDataBuffer[0], 0, HAUTH_BUFSIZE_DATA);
		memset((void *)&sHAuthStatusBuffer[0], 0, HAUTH_BUFSIZE_STATUS);

		auth_interface->hauth_host_hello_work_flag  = 0;
		auth_interface->hauth_dev_hello_work_flag   = 0;
		auth_interface->hauth_start_work_flag	    = 0;
		auth_interface->hauth_msg1_work_flag	    = 0;
		auth_interface->hauth_msg2_work_flag	    = 0;
		auth_interface->hauth_set_data_work_flag    = 0;
		auth_interface->hauth_status_work_flag	    = 0;

		auth_interface->hauth_ack.r_id		    = REPORT_ID_HAUTH_GET_ACK;
		auth_interface->hauth_ack.block_no	    = 0;
		auth_interface->hauth_ack.intended_r_id	    = 0;
		auth_interface->hauth_ack.ack_status	    = HAUTH_ACK;
	}
} //mbridge_hauth_init_internal

static long device_auth_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int ret = AUTH_SUCCESS;

	if (sAuth == NULL) {
		if (!gbIsDfuMode)
			GADGET_AUTH_LOG(LOG_ERR, "Device is not Initialized.\n");
		ret = AUTH_ERROR_NOT_INITIALIZED;
		return ret;
	}

	switch (cmd) {
	case HOST_IOCTL_GET_AUTH_STATE:
		if (arg == 0) {
			GADGET_AUTH_LOG(LOG_ERR, "arg is NULL.\n");
			ret = -EINVAL;
			break;
		}

		ret = copy_to_user((int*)arg, &sAuth->auth_status, sizeof(sAuth->auth_status));
		if (ret)
			GADGET_AUTH_LOG(LOG_ERR, "copy_to_user error. ret=[%#X(%d)]\n", ret, ret);
		break;

	case HOST_IOCTL_GET_AUTH1_DATA:
		GADGET_AUTH_LOG(LOG_DBG, "Auth1 data copy start.\n");
		if (arg == 0) {
			GADGET_AUTH_LOG(LOG_ERR, "arg is NULL.\n");
			ret = -EINVAL;
			break;
		}
		ret = copy_to_user((uint8_t*)arg, &sAuth->auth1_data, AUTH1_DATA_LENGTH);
		if (ret)
			GADGET_AUTH_LOG(LOG_ERR, "copy_to_user error. ret=[%#X(%d)]\n", ret, ret);
		break;

	case HOST_IOCTL_NOTIFY_GET_AUTH1_DATA_DONE:
		GADGET_AUTH_LOG(LOG_DBG, "Auth1 data copy done.\n");
		sAuth->auth_status = AUTH_STATE_AUTH2_CREATING;
		sAuth->status = AUTH_STATUS_CREATING_AUTH2_DATA;
		break;

	case HOST_IOCTL_SET_AUTH2_DATA:
		GADGET_AUTH_LOG(LOG_DBG, "Auth2 data copy done.\n");
		if (arg == 0) {
			GADGET_AUTH_LOG(LOG_ERR, "arg is NULL.\n");
			ret = -EINVAL;
			break;
		}
		ret = copy_from_user(&sAuth->auth2_data, (uint8_t*)arg, AUTH2_DATA_LENGTH);
		if (ret)
			GADGET_AUTH_LOG(LOG_ERR, "copy_from_user error. ret=[%#X(%d)]\n", ret, ret);
		break;

	case HOST_IOCTL_NOTIFY_SET_AUTH2_DATA_DONE:
		GADGET_AUTH_LOG(LOG_DBG, "Auth2 data create done.\n");
		sAuth->status = AUTH_STATUS_COMPLETE_AUTH2_DATA_CREATE;
		sAuth->auth_status = AUTH_STATE_READY;
		sAuth->auth2_block_number = 0;
		break;

	case HOST_IOCTL_AUTH_SET_ERROR:
		GADGET_AUTH_LOG(LOG_DBG, "Set Auth error.\n");
		ret = copy_from_user(&sAuth->error, (int*)arg, sizeof(int));
		if (ret)
			GADGET_AUTH_LOG(LOG_ERR, "copy_from_user error. ret=[%#X(%d)]\n", ret, ret);
		sAuth->status = AUTH_STATUS_ERROR_AUTH2_DATA_CREATE_FAILED;
		sAuth->auth_status = AUTH_STATE_READY;
		break;

	case HOST_IOCTL_NOTIFY_AUTH_CANCEL_START:
		GADGET_AUTH_LOG(LOG_DBG, "Cancel start.\n");
		sAuth->auth_status = AUTH_STATE_CANCEL_WAIT;
		break;

	case HOST_IOCTL_NOTIFY_AUTH_CANCEL_DONE:
		GADGET_AUTH_LOG(LOG_DBG, "Cancel done.\n");
		sAuth->status = AUTH_STATUS_WAIT_AUTH1_DATA_RECEIVE;
		sAuth->auth_status = AUTH_STATE_READY;
		break;

	case HOST_IOCTL_RESET_HAUTH:
		spin_lock(&sAuth->lock);
		mbridge_hauth_init_internal(sAuth);
		spin_unlock(&sAuth->lock);
		break;

	case HOST_IOCTL_SET_HAUTH_DEV_HELLO:
		if (arg) {
			spin_lock(&sAuth->lock);
			ret = copy_from_user((void *)&sHAuthDevHelloBuffer[0],
					     (const void *)arg, HAUTH_BUFSIZE_HELLO);
			if (ret) {
				GADGET_AUTH_LOG(LOG_ERR, "copy_from_user error. ret=[%#X(%d)]\n", ret, ret);
				sAuth->hauth_dev_hello_work_flag = 0;
			} else {
				GADGET_AUTH_LOG(LOG_DBG, "SetDevHello\n");
				sAuth->hauth_dev_hello_work_flag = 1;
			}
			spin_unlock(&sAuth->lock);
		} else {
			ret = -1;
		}
		break;

	case HOST_IOCTL_SET_HAUTH_MSG1:
		if (arg) {
			spin_lock(&sAuth->lock);
			ret = copy_from_user((void *)&sHAuthMsg1Buffer[0],
					     (const void *)arg, HAUTH_BUFSIZE_MSG1);
			if (ret) {
				GADGET_AUTH_LOG(LOG_ERR, "copy_from_user error. ret=[%#X(%d)]\n", ret, ret);
				sAuth->hauth_msg1_work_flag = 0;
			} else {
				GADGET_AUTH_LOG(LOG_DBG, "SetMsg1\n");
				sAuth->hauth_msg1_work_flag = 1;
			}
			spin_unlock(&sAuth->lock);
		} else {
			ret = -1;
		}
		break;

	case HOST_IOCTL_SET_HAUTH_STATUS:
		if (arg) {
			spin_lock(&sAuth->lock);
			ret = copy_from_user((void *)&sHAuthStatusBuffer[0],
					     (const void *)arg, HAUTH_BUFSIZE_STATUS);
			if (ret) {
				GADGET_AUTH_LOG(LOG_ERR, "copy_from_user error. ret=[%#X(%d)]\n", ret, ret);
				sAuth->hauth_status_work_flag = 0;
			} else {
				GADGET_AUTH_LOG(LOG_DBG, "SetStatus\n");
				sAuth->hauth_status_work_flag = 1;
			}
			spin_unlock(&sAuth->lock);
		} else {
			ret = -1;
		}
		break;

	case HOST_IOCTL_GET_HAUTH_HOST_HELLO:
		if (arg) {
			spin_lock(&sAuth->lock);
			if ((sAuth->hauth_host_hello_work_flag & HAUTH_FLAG_MASK_HELLO) == HAUTH_FLAG_MASK_HELLO) {
				// copy to user
				ret = copy_to_user((void *)arg,
						   (const void *)&sHAuthHostHelloBuffer[0], HAUTH_BUFSIZE_HELLO);
				if (ret) {
					GADGET_AUTH_LOG(LOG_ERR, "copy_to_user error. ret=[%#X(%d)]\n", ret, ret);
				} else {
					sAuth->hauth_host_hello_work_flag = 0;
					ret = HAUTH_BUFSIZE_HELLO;
				}
			}
			spin_unlock(&sAuth->lock);
		} else {
			ret = -1;
		}
		break;

	case HOST_IOCTL_GET_HAUTH_START:
		if (arg) {
			spin_lock(&sAuth->lock);
			if ((sAuth->hauth_start_work_flag & HAUTH_FLAG_MASK_START) == HAUTH_FLAG_MASK_START) {
				ret = copy_to_user((void *)arg,
						   (const void *)&sHAuthStartBuffer[0], HAUTH_BUFSIZE_START);
				if (ret) {
					GADGET_AUTH_LOG(LOG_ERR, "copy_to_user error. ret=[%#X(%d)]\n", ret, ret);
				} else {
					sAuth->hauth_start_work_flag = 0;
					ret = HAUTH_BUFSIZE_START;
				}
			}
			spin_unlock(&sAuth->lock);
		} else {
			ret = -1;
		}
		break;

	case HOST_IOCTL_GET_HAUTH_MSG2:
		if (arg) {
			spin_lock(&sAuth->lock);
			if ((sAuth->hauth_msg2_work_flag & HAUTH_FLAG_MASK_MSG2) == HAUTH_FLAG_MASK_MSG2) {
				ret = copy_to_user((void *)arg,
						   (const void *)&sHAuthMsg2Buffer[0], HAUTH_BUFSIZE_MSG2);
				if (ret) {
					GADGET_AUTH_LOG(LOG_ERR, "copy_to_user error. ret=[%#X(%d)]\n", ret, ret);
				} else {
					sAuth->hauth_msg2_work_flag = 0;
					ret = HAUTH_BUFSIZE_MSG2;
				}
			}
			spin_unlock(&sAuth->lock);
		} else {
			ret = -1;
		}
		break;

	case HOST_IOCTL_GET_HAUTH_DATA:
		if (arg) {
			spin_lock(&sAuth->lock);
			if ((sAuth->hauth_set_data_work_flag & HAUTH_FLAG_MASK_DATA) == HAUTH_FLAG_MASK_DATA) {
				ret = copy_to_user((void *)arg,
						   (const void *)&sHAuthSetDataBuffer[0], HAUTH_BUFSIZE_DATA);
				if (ret) {
					GADGET_AUTH_LOG(LOG_ERR, "copy_to_user error. ret=[%#X(%d)]\n", ret, ret);
				} else {
					sAuth->hauth_set_data_work_flag = 0;
					ret = HAUTH_BUFSIZE_DATA;
				}
			}
			spin_unlock(&sAuth->lock);
		} else {
			ret = -1;
		}
		break;

#ifdef ENABLE_HAUTH_DEBUG
	case HOST_IOCTL_DEBUG_GET_HAUTH_DEV_HELLO:
		if (!arg)
			break;

		if (!copy_to_user((void *)arg, sHAuthDevHelloBuffer, sizeof(sHAuthDevHelloBuffer)))
			ret = sizeof(sHAuthDevHelloBuffer);
		break;

	case HOST_IOCTL_DEBUG_GET_HAUTH_MSG1:
		if (!arg)
			break;

		if (!copy_to_user((void *)arg, sHAuthMsg1Buffer, sizeof(sHAuthMsg1Buffer)))
			ret = sizeof(sHAuthMsg1Buffer);
		break;

	case HOST_IOCTL_DEBUG_GET_HAUTH_STATUS:
		if (!arg)
			break;

		if (!copy_to_user((void *)arg, sHAuthStatusBuffer, sizeof(sHAuthStatusBuffer)))
			ret = sizeof(sHAuthStatusBuffer);
		break;

#endif
	default:
		break;
	}

	return ret;
} // device_auth_ioctl

/* Return - 0:	match, other: differrent */
static int mbridge_hauth_crc32_check(struct hauth_packet *hauth_packet)
{
#ifdef ENABLE_HAUTH_CRC_CHECK
	int32_t result = -1;
	uint8_t *p_packet = NULL;
	uint32_t calc_crc_result = 0;

	if (hauth_packet != NULL) {
		p_packet = (uint8_t *)hauth_packet;
		calc_crc_result = crc32_be(~0, (unsigned char const *)hauth_packet,
					   HAUTH_CRC_TARGET_LENGTH);

		GADGET_AUTH_LOG(LOG_DBG, "pay: %u, cal: %u\n",
				hauth_packet->crc32, calc_crc_result);
		if (calc_crc_result == hauth_packet->crc32)
			result = AUTH_SUCCESS;
	}

	return (int)result;
#else
	return AUTH_SUCCESS;
#endif
} // mbridge_hauth_crc32_check

static int mbridge_hauth_set_report(struct usb_request *req, struct mbridge_auth_interface *auth_interface)
{
	struct hauth_packet *hauth = NULL;
	int32_t result = AUTH_SUCCESS;

	GADGET_AUTH_LOG(LOG_DBG, "\n");

	hauth = (struct hauth_packet *)req->buf;

	spin_lock(&auth_interface->lock);
	// CRC check
	result = mbridge_hauth_crc32_check(hauth);
	if (result == AUTH_SUCCESS) {
		switch (hauth->header.r_id) {
		case REPORT_ID_HAUTH_SET_HOST_HELLO:
			if ((hauth->header.block_total == HAUTH_TOTAL_BLOCK_NUM_HELLO)
			    && (hauth->header.block_no < hauth->header.block_total)) {
				memcpy((void *)&sHAuthHostHelloBuffer[hauth->header.block_no * HAUTH_MSG_LENGTH],
				       (const void *)hauth->msg, HAUTH_MSG_LENGTH);
				/* bug #127060. */
				/* reset host_hello_work_flag when received block #0 and discard block #1's data */
				if (hauth->header.block_no == 0) {
					auth_interface->hauth_host_hello_work_flag = (uint8_t)(1 << hauth->header.block_no);
				} else {
					auth_interface->hauth_host_hello_work_flag |= (uint8_t)(1 << hauth->header.block_no);
				}
				GADGET_AUTH_LOG(LOG_DBG, "R-ID: %02X received.\n", hauth->header.r_id);
			} else {
				result = AUTH_ERROR_INVALID_BLOCK_NUMBER;
			}
			break;

		case REPORT_ID_HAUTH_SET_START:
			if ((hauth->header.block_total == HAUTH_TOTAL_BLOCK_NUM_START)
			    && (hauth->header.block_no < hauth->header.block_total)) {
				memcpy((void *)&sHAuthStartBuffer[hauth->header.block_no * HAUTH_MSG_LENGTH],
				       (const void *)hauth->msg, HAUTH_MSG_LENGTH);
				auth_interface->hauth_start_work_flag |= (uint8_t)(1 << hauth->header.block_no);
				GADGET_AUTH_LOG(LOG_DBG, "R-ID: %02X received.\n", hauth->header.r_id);
			} else {
				result = AUTH_ERROR_INVALID_BLOCK_NUMBER;
			}
			break;

		case REPORT_ID_HAUTH_SET_MSG2:
			if ((hauth->header.block_total == HAUTH_TOTAL_BLOCK_NUM_MSG2)
			    && (hauth->header.block_no < hauth->header.block_total)) {
				memcpy((void *)&sHAuthMsg2Buffer[hauth->header.block_no * HAUTH_MSG_LENGTH],
				       (const void *)hauth->msg, HAUTH_MSG_LENGTH);
				auth_interface->hauth_msg2_work_flag |= (uint8_t)(1 << hauth->header.block_no);
				GADGET_AUTH_LOG(LOG_DBG, "R-ID: %02X received.\n", hauth->header.r_id);
			} else {
				result = AUTH_ERROR_INVALID_BLOCK_NUMBER;
			}
			break;

		case REPORT_ID_HAUTH_SET_DATA:
			if ((hauth->header.block_total == HAUTH_TOTAL_BLOCK_NUM_DATA)
			    && (hauth->header.block_no < hauth->header.block_total)) {
				memcpy((void *)&sHAuthSetDataBuffer[hauth->header.block_no * HAUTH_MSG_LENGTH],
				       (const void *)hauth->msg, HAUTH_MSG_LENGTH);
				auth_interface->hauth_set_data_work_flag |= (uint8_t)(1 << hauth->header.block_no);
				GADGET_AUTH_LOG(LOG_DBG, "R-ID: %02X received.\n", hauth->header.r_id);
			} else {
				result = AUTH_ERROR_INVALID_BLOCK_NUMBER;
			}
			break;

		default:
			result = AUTH_ERROR_NOT_SUPPORTED;
			break;
		}
	} else	{
		// CRC error
		result = -1;
	}

	auth_interface->hauth_ack.intended_r_id = hauth->header.r_id;
	if (result == AUTH_SUCCESS)
		auth_interface->hauth_ack.ack_status = HAUTH_ACK;
	else
		auth_interface->hauth_ack.ack_status = HAUTH_NAK;

	spin_unlock(&auth_interface->lock);

	return (int)result;
} // mbridge_hauth_set_report

static int mbridge_hauth_get_report(struct usb_request *req,
				    struct mbridge_auth_interface *auth_interface,
				    uint8_t report_id, uint16_t length)
{
	struct hauth_packet stHAuthGetReportWorkPacket;
	uint8_t block_no = 0;
	int result = AUTH_SUCCESS;

	memset((void *)&stHAuthGetReportWorkPacket, 0, sizeof(struct hauth_packet));

	switch (report_id) {
	case REPORT_ID_HAUTH_GET_DEV_HELLO_0:
	case REPORT_ID_HAUTH_GET_DEV_HELLO_1:
		spin_lock(&auth_interface->lock);
		if (auth_interface->hauth_dev_hello_work_flag) {
			block_no = (uint8_t)(report_id - REPORT_ID_HAUTH_GET_DEV_HELLO_0);
			stHAuthGetReportWorkPacket.header.r_id		 = report_id;
			stHAuthGetReportWorkPacket.header.block_no	 = block_no;
			stHAuthGetReportWorkPacket.header.block_total	 = HAUTH_TOTAL_BLOCK_NUM_HELLO;
			stHAuthGetReportWorkPacket.header.reserved	 = 0;
			memcpy((void *)&stHAuthGetReportWorkPacket.msg[0],
			       (const void *)&sHAuthDevHelloBuffer[block_no * HAUTH_MSG_LENGTH],
			       HAUTH_MSG_LENGTH);
			stHAuthGetReportWorkPacket.crc32 =
				crc32_be(~0, (unsigned const char *)&stHAuthGetReportWorkPacket,
					 HAUTH_CRC_TARGET_LENGTH);
			GADGET_AUTH_LOG(LOG_DBG, "R-ID: %02X, block_no: %u, crc32: %u\n",
					report_id, block_no, stHAuthGetReportWorkPacket.crc32);
		} else {
			GADGET_AUTH_LOG(LOG_DBG, "R-ID: %02X, Not ready.\n", report_id);
		}
		spin_unlock(&auth_interface->lock);
		break;

	case REPORT_ID_HAUTH_GET_MSG1_0:
	case REPORT_ID_HAUTH_GET_MSG1_1:
	case REPORT_ID_HAUTH_GET_MSG1_2:
	case REPORT_ID_HAUTH_GET_MSG1_3:
	case REPORT_ID_HAUTH_GET_MSG1_4:
	case REPORT_ID_HAUTH_GET_MSG1_5:
		spin_lock(&auth_interface->lock);
		if (auth_interface->hauth_msg1_work_flag) {
			block_no = (uint8_t)(report_id - REPORT_ID_HAUTH_GET_MSG1_0);
			stHAuthGetReportWorkPacket.header.r_id		 = report_id;
			stHAuthGetReportWorkPacket.header.block_no	 = block_no;
			stHAuthGetReportWorkPacket.header.block_total	 = HAUTH_TOTAL_BLOCK_NUM_MSG1;
			memcpy((void *)&stHAuthGetReportWorkPacket.msg[0],
			       (const void *)&sHAuthMsg1Buffer[block_no * HAUTH_MSG_LENGTH],
			       HAUTH_MSG_LENGTH);
			stHAuthGetReportWorkPacket.crc32 =
				crc32_be(~0, (unsigned const char *)&stHAuthGetReportWorkPacket,
					 HAUTH_CRC_TARGET_LENGTH);
			GADGET_AUTH_LOG(LOG_DBG, "R-ID: %02X, block_no: %u, crc32: %u\n",
					report_id, block_no, stHAuthGetReportWorkPacket.crc32);
		} else {
			GADGET_AUTH_LOG(LOG_DBG, "R-ID: %02X, Not ready.\n", report_id);
		}
		spin_unlock(&auth_interface->lock);
		break;

	case REPORT_ID_HAUTH_GET_STATUS_0:
	case REPORT_ID_HAUTH_GET_STATUS_1:
		spin_lock(&auth_interface->lock);
		if (auth_interface->hauth_status_work_flag) {
			block_no = (uint8_t)(report_id - REPORT_ID_HAUTH_GET_STATUS_0);
			stHAuthGetReportWorkPacket.header.r_id		 = report_id;
			stHAuthGetReportWorkPacket.header.block_no	 = block_no;
			stHAuthGetReportWorkPacket.header.block_total	 = HAUTH_TOTAL_BLOCK_NUM_STATUS;
			memcpy((void *)&stHAuthGetReportWorkPacket.msg[0],
			       (const void *)&sHAuthStatusBuffer[block_no * HAUTH_MSG_LENGTH],
			       HAUTH_MSG_LENGTH);
			stHAuthGetReportWorkPacket.crc32 =
				crc32_be(~0, (unsigned const char *)&stHAuthGetReportWorkPacket,
					 HAUTH_CRC_TARGET_LENGTH);
			GADGET_AUTH_LOG(LOG_DBG, "R-ID: %02X, block_no: %u, crc32: %u\n",
					report_id, block_no, stHAuthGetReportWorkPacket.crc32);
		} else {
			GADGET_AUTH_LOG(LOG_DBG, "R-ID: %02X, Not ready.\n", report_id);
		}
		spin_unlock(&auth_interface->lock);
		break;

	default:
		GADGET_AUTH_LOG(LOG_DBG, "do nothing.\n");
		result = AUTH_ERROR_NOT_SUPPORTED;
		break;
	}

	if (result == AUTH_SUCCESS)
		memcpy((void *)req->buf, (const void *)&stHAuthGetReportWorkPacket,
		       sizeof(struct hauth_packet));

	return result;
} // mbridge_hauth_get_report

//----------------------------------------------------------------------------
static void dump_auth1(struct mbridge_auth1_data *auth1_data)
{

	GADGET_AUTH_LOG(LOG_DBG, "report_id=[%#x]\n", auth1_data->report_id);
	GADGET_AUTH_LOG(LOG_DBG, "sequence_number=[%#x]\n", auth1_data->sequence_number);
	GADGET_AUTH_LOG(LOG_DBG, "block_number=[%#x]\n", auth1_data->block_number);
#if defined(ENABLE_DUMP_LOG)
	{
	int i = 0;
	GADGET_AUTH_LOG(LOG_DBG, "auth1 data begin------------------------------\n");
	for (i = 0; i < sizeof(auth1_data->data); i++) {
		GADGET_AUTH_LOG(LOG_DBG, "0x%02x", auth1_data->data[i]);
		if ((i + 1) % 16 != 0)
			GADGET_AUTH_LOG(LOG_DBG, ",");
		if ((i + 1) % 16 == 0)
			GADGET_AUTH_LOG(LOG_DBG, "\n");
	}
	GADGET_AUTH_LOG(LOG_DBG, "auth1 data end------------------------------\n");
	}
#endif
	GADGET_AUTH_LOG(LOG_DBG, "crc=[%#x,%#x,%#x,%#x]\n",
			auth1_data->crc[0], auth1_data->crc[1],
			auth1_data->crc[2], auth1_data->crc[3]);
}

static void dump_auth2(struct mbridge_auth2_data *auth2_data)
{
	GADGET_AUTH_LOG(LOG_DBG, "report_id=[%#x]\n", auth2_data->report_id);
	GADGET_AUTH_LOG(LOG_DBG, "sequence_number=[%#x]\n", auth2_data->sequence_number);
	GADGET_AUTH_LOG(LOG_DBG, "block_number=[%#x]\n", auth2_data->block_number);
#if defined(ENABLE_DUMP_LOG)
	{
	int i = 0;
	GADGET_AUTH_LOG(LOG_DBG, "auth2 data begin------------------------------\n");
	for (i = 0; i < sizeof(auth2_data->data); i++) {
		GADGET_AUTH_LOG(LOG_DBG, "0x%02x", auth2_data->data[i]);
		if ((i + 1) % 16 != 0)
			GADGET_AUTH_LOG(LOG_DBG, ",");
		if ((i + 1) % 16 == 0)
			GADGET_AUTH_LOG(LOG_DBG, "\n");
	}
	GADGET_AUTH_LOG(LOG_DBG, "auth2 data end------------------------------\n");
	}
#endif
	GADGET_AUTH_LOG(LOG_DBG, "crc=[%#x,%#x,%#x,%#x]\n",
			auth2_data->crc[0], auth2_data->crc[1],
			auth2_data->crc[2], auth2_data->crc[3]);
}

static int mbridge_auth_set_auth1_data(struct usb_request *req,
				       struct mbridge_auth_interface *auth_interface)
{
	struct mbridge_auth1_data auth1_data;
	int ret = AUTH_SUCCESS;
	int length = 0;

	GADGET_AUTH_LOG(LOG_DBG, "start set auth1 data.\n");

	length = req->length;
	memset(&auth1_data, 0, sizeof(auth1_data));
	memcpy(&auth1_data, req->buf, length);
	dump_auth1(&auth1_data);

	spin_lock(&auth_interface->lock);

	if ((auth_interface->auth_status == AUTH_STATE_READY)
	    && (auth_interface->status == AUTH_STATUS_WAIT_AUTH1_DATA_RECEIVE)) {
		GADGET_AUTH_LOG(LOG_DBG, "State change. Ready -> Receiving.\n");
		auth_interface->auth_status = AUTH_STATE_AUTH1_RECEIVING;
	}

	if (auth_interface->auth_status != AUTH_STATE_AUTH1_RECEIVING) {
		GADGET_AUTH_LOG(LOG_ERR, "Invalid request. State error. current=[%d].\n",
				auth_interface->auth_status);
		ret = AUTH_ERROR_INVALID_REQUEST;
		goto cleanup;
	}

	if (auth1_data.block_number > MAX_AUTH1_BLOCK_NUMBER) {
		GADGET_AUTH_LOG(LOG_ERR, "Invalid block number. limit over. value=[%d]\n",
				auth1_data.block_number);
		ret = AUTH_ERROR_INVALID_BLOCK_NUMBER;
		goto cleanup;
	}

	if (auth_interface->sequence_number != auth1_data.sequence_number) {
		GADGET_AUTH_LOG(LOG_DBG, "Sequence number chenge. perv=[%d], current=[%d]\n",
				auth_interface->sequence_number, auth1_data.sequence_number);

		auth_interface->sequence_number = auth1_data.sequence_number;
		auth_interface->num_of_received = 0;
		auth_interface->auth1_block_number = 0;
	}

	if (auth_interface->auth1_block_number != auth1_data.block_number) {
		GADGET_AUTH_LOG(LOG_ERR, "Invalid block number. sequence error. prev=[%d], value=[%d]\n",
				auth_interface->auth1_block_number, auth1_data.block_number);
		ret = AUTH_ERROR_INVALID_BLOCK_NUMBER;
		goto cleanup;
	}

	memcpy(&auth_interface->auth1_data[auth1_data.block_number*SET_AUTH_DATA_BLOCK_SIZE],
	       &auth1_data.data[0], SET_AUTH_DATA_BLOCK_SIZE);

	auth_interface->auth1_block_number++;
	auth_interface->num_of_received += SET_AUTH_DATA_BLOCK_SIZE;
	GADGET_AUTH_LOG(LOG_DBG, "%d byte received. Total %d byte received.\n",
			length, auth_interface->num_of_received);

	if (auth_interface->num_of_received == AUTH1_DATA_LENGTH+24) {
		GADGET_AUTH_LOG(LOG_DBG, "Received all data. Start creation of auth2 data.\n");
		auth_interface->status = AUTH_STATUS_CREATING_AUTH2_DATA;
		auth_interface->auth_status = AUTH_STATE_AUTH1_ALL_RECEIVED;
	}

cleanup:
	spin_unlock(&auth_interface->lock);
	return ret;
} // mbridge_auth_set_auth1_data

static void mbridge_auth_ctrl_complete(struct usb_ep *ep, struct usb_request *req)
{
	struct mbridge_auth_interface *auth_interface = NULL;
	unsigned char report_id;

	GADGET_AUTH_LOG(LOG_DBG, "ctrl_complete authentication interface...\n");

	auth_interface = ep->driver_data;

	memcpy(&report_id, req->buf, 1);
	GADGET_AUTH_LOG(LOG_DBG, "report_id=[%#x]\n", report_id);

	switch (report_id) {
// Device Authentication
	case REPORT_ID_SET_AUTH1_DATA: // Set auth1 data
		mbridge_auth_set_auth1_data(req, auth_interface);
		break;
// Host Authentication
	case REPORT_ID_HAUTH_SET_HOST_HELLO:
	case REPORT_ID_HAUTH_SET_START:
	case REPORT_ID_HAUTH_SET_MSG2:
	case REPORT_ID_HAUTH_SET_DATA:
		mbridge_hauth_set_report(req, auth_interface);
		break;
	case REPORT_ID_GET_AUTH2_DATA: // Get auth2 data
	case REPORT_ID_GET_AUTH_STATUS: // Get auth status
	case REPORT_ID_GET_AUTH_CONFIG: // Get auth config
	case REPORT_ID_HAUTH_GET_DEV_HELLO_0:
	case REPORT_ID_HAUTH_GET_DEV_HELLO_1:
	case REPORT_ID_HAUTH_GET_MSG1_0:
	case REPORT_ID_HAUTH_GET_MSG1_1:
	case REPORT_ID_HAUTH_GET_MSG1_2:
	case REPORT_ID_HAUTH_GET_MSG1_3:
	case REPORT_ID_HAUTH_GET_MSG1_4:
	case REPORT_ID_HAUTH_GET_MSG1_5:
	case REPORT_ID_HAUTH_GET_STATUS_0:
	case REPORT_ID_HAUTH_GET_STATUS_1:
	default:
		GADGET_AUTH_LOG(LOG_DBG, "do nothing.\n");
		break;
	}
} // mbridge_auth_ctrl_complete

static int mbridge_auth_bind(struct usb_configuration *c, struct usb_function *f)
{
	struct mbridge_auth_interface *auth_interface = (struct mbridge_auth_interface *)f;

	GADGET_AUTH_LOG(LOG_DBG, "binding authentication interface...\n");

	/* Configure endpoint from the descriptor. */
	auth_interface->source_ep = usb_ep_autoconfig(c->cdev->gadget, &fs_auth_source_desc);
	if (auth_interface->source_ep == NULL) {
		GADGET_AUTH_LOG(LOG_ERR, "can't autoconfigure auth endpoint\n");
		return -ENODEV;
	}

	/* Need to set this during setup only. */
	auth_interface->source_ep->driver_data = c->cdev;
	auth_interface->sink_ep = usb_ep_autoconfig(c->cdev->gadget, &fs_auth_sink_desc);
	if (auth_interface->sink_ep == NULL) {
		GADGET_AUTH_LOG(LOG_ERR, "can't autoconfigure auth endpoint\n");
		return -ENODEV;
	}
	/* Need to set this during setup only. */
	auth_interface->sink_ep->driver_data = c->cdev;

	auth_hid_desc.desc[0].bDescriptorType = HID_DT_REPORT;
	auth_hid_desc.desc[0].wDescriptorLength = cpu_to_le16(AUTHENTICATION_REPORT_DESC_LENGTH);
	usb_assign_descriptors(f, fs_auth_descs, hs_auth_descs, NULL);

	return AUTH_SUCCESS;
} // mbridge_auth_bind

static int mbridge_auth_enable(struct usb_function *f)
{
	struct mbridge_auth_interface *auth_interface = (struct mbridge_auth_interface *)f;
	int ret = 0;

	GADGET_AUTH_LOG(LOG_DBG, "enabling authentication interface...\n");

	/* Enable the endpoint. */
	ret = config_ep_by_speed(f->config->cdev->gadget, f, auth_interface->source_ep);
	if (ret) {
		GADGET_AUTH_LOG(LOG_ERR, "error configuring auth endpoint. ret=[%d]\n", ret);
		return ret;
	}

	ret = usb_ep_enable(auth_interface->source_ep);
	if (ret) {
		GADGET_AUTH_LOG(LOG_ERR, "error starting auth endpoint. ret=[%d]\n", ret);
		return ret;
	}

	/* Data for the completion callback. */
	auth_interface->source_ep->driver_data = auth_interface;

	ret = config_ep_by_speed(f->config->cdev->gadget, f, auth_interface->sink_ep);
	if (ret) {
		GADGET_AUTH_LOG(LOG_ERR, "error configuring auth endpoint. ret=[%d]\n", ret);
		return ret;
	}

	ret = usb_ep_enable(auth_interface->sink_ep);
	if (ret) {
		GADGET_AUTH_LOG(LOG_ERR, "error starting auth endpoint. ret=[%d]\n", ret);
		return ret;
	}

	auth_interface->sink_ep->driver_data = auth_interface;
	return AUTH_SUCCESS;
} //  mbridge_auth_enable

static void mbridge_auth_disable(struct usb_function *f)
{
	struct mbridge_auth_interface *auth_interface = (struct mbridge_auth_interface *)f;

	GADGET_AUTH_LOG(LOG_DBG, "disabling authentication interface...\n");

	disable_ep(auth_interface->source_ep);
	disable_ep(auth_interface->sink_ep);

	spin_lock(&auth_interface->lock);
	mbridge_hauth_init_internal(auth_interface);
	spin_unlock(&auth_interface->lock);
} // mbridge_auth_disable

static int mbridge_auth_reset(struct usb_function *f, unsigned intf, unsigned alt)
{
	int ret = AUTH_SUCCESS;

	GADGET_AUTH_LOG(LOG_DBG, "intf(%d)\n", intf);

	if (alt)
		return -EINVAL;

	mbridge_auth_disable(f);
	ret = mbridge_auth_enable(f);

	return ret;
} // mbridge_auth_reset

static int mbridge_auth_get_auth_status(struct usb_request *req,
					struct mbridge_auth_interface *auth_interface,
					uint16_t length)
{
	int ret = AUTH_SUCCESS;
	struct mbridge_auth_status auth_status;

	GADGET_AUTH_LOG(LOG_DBG, "Get Auth status. status=[%d], sequence=[%d]\n",
			auth_interface->status, auth_interface->sequence_number);

	memset(&auth_status, 0, sizeof(auth_status));
	auth_status.report_id = REPORT_ID_GET_AUTH_STATUS;
	auth_status.sequence_number = auth_interface->sequence_number;
	auth_status.status = auth_interface->status;
	memcpy(req->buf, &auth_status, sizeof(auth_status));

	return ret;
} // mbrige_auth_get_auth_status

static int mbridge_auth_get_auth2_data(struct usb_request *req,
				       struct mbridge_auth_interface *auth_interface,
				       uint16_t length)
{
	int ret = AUTH_SUCCESS;
	int send_size;
	struct mbridge_auth2_data auth2_data;

	GADGET_AUTH_LOG(LOG_DBG, "Get Auth2 data. status=[%d], sequence=[%d], block=[%d]\n",
			auth_interface->status, auth_interface->sequence_number,
			auth_interface->auth2_block_number);

	memset(&auth2_data, 0, sizeof(auth2_data));
	auth2_data.report_id = REPORT_ID_GET_AUTH2_DATA;

	if (auth_interface->status != AUTH_STATUS_COMPLETE_AUTH2_DATA_CREATE) {
		GADGET_AUTH_LOG(LOG_WARN, "auth2 not created.\n");
		memcpy(req->buf, &auth2_data, sizeof(auth2_data));
		return AUTH_ERROR_NOT_CREATED;
	}

	if (auth_interface->auth2_block_number > MAX_AUTH2_BLOCK_NUMBER) {
		GADGET_AUTH_LOG(LOG_WARN, "auth2 no data.\n");
		memcpy(req->buf, &auth2_data, sizeof(auth2_data));
		return AUTH_ERROR_NO_DATA;
	}

	auth2_data.sequence_number = auth_interface->sequence_number;
	auth2_data.block_number = auth_interface->auth2_block_number;

	// data copy.
	if (auth_interface->num_of_sent >= (AUTH2_DATA_LENGTH - GET_AUTH_DATA_BLOCK_SIZE)) {
		send_size = (AUTH2_DATA_LENGTH - auth_interface->num_of_sent);
	} else {
		send_size = GET_AUTH_DATA_BLOCK_SIZE;
	}

	GADGET_AUTH_LOG(LOG_DBG, "Get Auth2 data. send size=[%d]\n", send_size);
	memcpy(&auth2_data.data[0],
	       &auth_interface->auth2_data[auth_interface->auth2_block_number*GET_AUTH_DATA_BLOCK_SIZE],
	       send_size);
	auth_interface->auth2_block_number++;
	dump_auth2(&auth2_data);
	memcpy(req->buf, &auth2_data, sizeof(auth2_data));
	auth_interface->num_of_sent += send_size;

	if (auth_interface->num_of_sent == AUTH2_DATA_LENGTH) {
		GADGET_AUTH_LOG(LOG_DBG, "Send all data.\n");
		mbridge_auth_init_internal(auth_interface);
	}

	return ret;
} // mbridge_auth_get_auth2_data

static int mbridge_auth_get_auth_config(struct usb_request *req,
					struct mbridge_auth_interface *auth_interface,
					uint16_t length)
{
	int ret = AUTH_SUCCESS;

	GADGET_AUTH_LOG(LOG_DBG, "Get Auth config. status=[%d], sequence=[%d]\n",
			auth_interface->status, auth_interface->sequence_number);

	length = min_t(unsigned, length, sizeof(s_auth_config));

	memcpy(req->buf, &s_auth_config, length);

	spin_lock(&auth_interface->lock);

	auth_interface->auth1_block_number = 0;
	auth_interface->auth2_block_number = 0;
	auth_interface->num_of_received = 0;
	auth_interface->num_of_sent = 0;
	memset(auth_interface->auth1_data, 0, sizeof(auth_interface->auth1_data));
	memset(auth_interface->auth2_data, 0, sizeof(auth_interface->auth2_data));

	if (auth_interface->auth_status == AUTH_STATE_AUTH2_CREATING) {
		auth_interface->auth_status = AUTH_STATE_CANCEL;
	} else {
		auth_interface->status = AUTH_STATUS_WAIT_AUTH1_DATA_RECEIVE;
		auth_interface->auth_status = AUTH_STATE_READY;
	}

	spin_unlock(&auth_interface->lock);
	return ret;
} // mbridge_auth_get_auth_config

static int mbridge_auth_get_report(struct usb_function *f, const struct usb_ctrlrequest *ctrl)
{
	struct mbridge_auth_interface *auth_interface = (struct mbridge_auth_interface *)f;
	struct usb_composite_dev *cdev = f->config->cdev;
	struct usb_request *req = cdev->req;
	uint8_t report_id;
	__u16 value, length;
	int ret = AUTH_SUCCESS;

	value  = __le16_to_cpu(ctrl->wValue);
	length = __le16_to_cpu(ctrl->wLength);

	report_id = (value & 0x00FF);
	GADGET_AUTH_LOG(LOG_DBG, "report_id=[%#x]\n", report_id);

	/* report id */
	switch (report_id) {
// Device Authentication
	case REPORT_ID_GET_AUTH2_DATA: // Get auth2 data
		ret = mbridge_auth_get_auth2_data(req, auth_interface, length);
		break;
	case REPORT_ID_GET_AUTH_STATUS: // Get auth status
		ret = mbridge_auth_get_auth_status(req, auth_interface, length);
	break;
	case REPORT_ID_GET_AUTH_CONFIG: // Get auth config
		ret = mbridge_auth_get_auth_config(req, auth_interface, length);
	break;
// Host Authentication
	case REPORT_ID_HAUTH_GET_DEV_HELLO_0:
	case REPORT_ID_HAUTH_GET_DEV_HELLO_1:
	case REPORT_ID_HAUTH_GET_MSG1_0:
	case REPORT_ID_HAUTH_GET_MSG1_1:
	case REPORT_ID_HAUTH_GET_MSG1_2:
	case REPORT_ID_HAUTH_GET_MSG1_3:
	case REPORT_ID_HAUTH_GET_MSG1_4:
	case REPORT_ID_HAUTH_GET_MSG1_5:
	case REPORT_ID_HAUTH_GET_STATUS_0:
	case REPORT_ID_HAUTH_GET_STATUS_1:
		ret = mbridge_hauth_get_report(req, auth_interface, report_id, length);
		break;
	case REPORT_ID_HAUTH_GET_ACK:
		if (req->buf != NULL && auth_interface != NULL)
			memcpy((void *)req->buf,
			       (void *)&auth_interface->hauth_ack, sizeof(struct hauth_ack_packet));
		break;
	default:
		GADGET_AUTH_LOG(LOG_DBG, "do nothing.\n");
		ret = AUTH_ERROR_NOT_SUPPORTED;
		break;
	}

	return ret;
} // mbridge_auth_get_report

static int mbridge_auth_setup(struct usb_function *f, const struct usb_ctrlrequest *ctrl)
{
	struct mbridge_auth_interface *auth_interface = (struct mbridge_auth_interface *)f;
	int ret = AUTH_SUCCESS;

	struct usb_composite_dev *cdev = f->config->cdev;
	struct usb_request *req = cdev->req;
	uint16_t value, length;

	value  = __le16_to_cpu(ctrl->wValue);
	length = __le16_to_cpu(ctrl->wLength);

	GADGET_AUTH_LOG(LOG_DBG, "mbridge_auth_setup\n");
	GADGET_AUTH_LOG(LOG_DBG, "control req%02x.%02x v%04x i%04x l%d\n",
			ctrl->bRequestType, ctrl->bRequest, value, le16_to_cpu(ctrl->wIndex), length);

	switch ((ctrl->bRequestType << 8) | ctrl->bRequest) {

	case ((USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8 | HID_REQ_GET_REPORT):
		GADGET_AUTH_LOG(LOG_DBG, "get_report\n");
		ret = mbridge_auth_get_report(f, ctrl);
		break;

	case ((USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8 | HID_REQ_GET_PROTOCOL):
		GADGET_AUTH_LOG(LOG_DBG, "get_protocol\n");
		ret = AUTH_ERROR_NOT_SUPPORTED;
		break;

	case ((USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8 | HID_REQ_SET_REPORT):
		GADGET_AUTH_LOG(LOG_DBG, "set_report | wLength=%d\n", ctrl->wLength);
		req->complete = mbridge_auth_ctrl_complete;
		break;

	case ((USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8 | HID_REQ_SET_PROTOCOL):
		GADGET_AUTH_LOG(LOG_DBG, "set_protocol\n");
		ret = AUTH_ERROR_NOT_SUPPORTED;
		break;

	case ((USB_DIR_IN | USB_TYPE_STANDARD | USB_RECIP_INTERFACE) << 8 | USB_REQ_GET_DESCRIPTOR):
		GADGET_AUTH_LOG(LOG_DBG, "USB_REQ_GET_DESCRIPTOR\n");
		switch (value >> 8) {
		case HID_DT_HID:
			GADGET_AUTH_LOG(LOG_DBG, "USB_REQ_GET_DESCRIPTOR: HID\n");
			length = min_t(unsigned short, length, auth_hid_desc.bLength);
			memcpy(req->buf, &auth_hid_desc, length);
			break;

		case HID_DT_REPORT:
			GADGET_AUTH_LOG(LOG_DBG, "USB_REQ_GET_DESCRIPTOR: REPORT\n");
			length = min_t(unsigned short, length, AUTHENTICATION_REPORT_DESC_LENGTH);
			memcpy(req->buf, AuthenticationReportDescriptor, length);
			break;
		default:
			GADGET_AUTH_LOG(LOG_DBG, "Unknown descriptor request 0x%x\n", value >> 8);
			ret = AUTH_ERROR_NOT_SUPPORTED;
			break;
		}
		break;
	default:
		GADGET_AUTH_LOG(LOG_DBG, "Unknown request 0x%x\n", ctrl->bRequest);
		ret = AUTH_ERROR_NOT_SUPPORTED;
		break;
	}

	if (ret == AUTH_ERROR_NOT_SUPPORTED)
		return -EOPNOTSUPP;

	req->zero = 0;
	req->length = length;
	cdev->gadget->ep0->driver_data = auth_interface;
	ret = usb_ep_queue(cdev->gadget->ep0, req, GFP_ATOMIC);
	if (ret < 0)
		GADGET_AUTH_LOG(LOG_DBG, "usb_ep_queue error on ep0 %d, ret=[%d]\n", value, ret);
	return ret;
} // mbridge_auth_setup


#if 0 // unused for now
static int mbridge_auth_set_report(struct usb_function *f,
				   const struct usb_ctrlrequest *ctrl)
{
	uint16_t value;
	uint8_t report_id;
	int ret = AUTH_SUCCESS;

	value  = __le16_to_cpu(ctrl->wValue);
	report_id = (value & 0x00FF);

	GADGET_AUTH_LOG(LOG_DBG, "report_id=[%#x]\n", report_id);

	/* report id */
	switch (report_id) {
	case REPORT_ID_SET_AUTH1_DATA: // Set auth1 data
		GADGET_AUTH_LOG(LOG_WARN, "start set auth1 data.\n");
		break;
	default:
		ret = AUTH_ERROR_NOT_SUPPORTED;
		break;
	}

	return ret;
} // mbridge_auth_set_report
#endif


//----------------------------------------------------------------------------

static struct file_operations auth_fops = {
	.unlocked_ioctl = device_auth_ioctl,
};

int mbridge_auth_init(struct usb_composite_dev *cdev, struct usb_function **interface)
{
	struct mbridge_auth_interface *auth_interface;
	int ret;

	GADGET_AUTH_LOG(LOG_DBG, "\n");

	auth_intf.bInterfaceNumber = MORPHEUS_AUTHENTICATION_INTERFACE;

	ret = usb_string_id(cdev);
	if (ret < 0)
		return ret;

	strings_dev[STRING_AUTHENTICATION_IDX].id = ret;

	auth_intf.iInterface = ret;

	sAuth = auth_interface = kzalloc(sizeof (*auth_interface), GFP_KERNEL);
	if (auth_interface == NULL)
		return -ENOMEM;

	/* register devnode */
	auth_interface->class = class_create(THIS_MODULE, "pu_auth");
	if (IS_ERR(auth_interface->class)) {
		dev_err(&cdev->gadget->dev, "failed to create class\n");
		ret = PTR_ERR(auth_interface->class);
		goto rewind0;
	}
	ret = register_chrdev(0, auth_interface->class->name, &auth_fops);
	if (ret < 0) {
		dev_info(&cdev->gadget->dev, "failed to register chrdev\n");
		goto rewind1;
	}
	auth_interface->major = ret;
	auth_interface->nodedev = device_create(auth_interface->class, NULL,
						MKDEV(ret, 0), auth_interface,
						auth_interface->class->name);
	if (IS_ERR(auth_interface->nodedev)) {
		dev_err(&cdev->gadget->dev, "failed to create nodedev\n");
		ret = PTR_ERR(auth_interface->nodedev);
		goto rewind2;
	}

	spin_lock_init(&auth_interface->lock);

	auth_interface->interface.name = "auth";
	auth_interface->interface.fs_descriptors = fs_auth_descs;
	auth_interface->interface.hs_descriptors = hs_auth_descs;
	auth_interface->interface.bind = mbridge_auth_bind;
	auth_interface->interface.set_alt = mbridge_auth_reset;
	auth_interface->interface.disable = mbridge_auth_disable;
	auth_interface->interface.setup = mbridge_auth_setup;
	mbridge_auth_init_internal(auth_interface);
	// Host Authentication
	mbridge_hauth_init_internal(auth_interface);

	*interface = &auth_interface->interface;
	return 0;

rewind2:
	unregister_chrdev(auth_interface->major, auth_interface->class->name);
rewind1:
	class_destroy(auth_interface->class);
rewind0:
	kfree(auth_interface);
	return ret;
} // mbridge_auth_init


void mbridge_auth_destroy(struct usb_function *interface)
{
	struct mbridge_auth_interface *auth_interface =
		(struct mbridge_auth_interface *)interface;

	if (auth_interface->nodedev)
		device_destroy(auth_interface->class,
			       MKDEV(auth_interface->major,0));
	if (auth_interface->major)
		unregister_chrdev(auth_interface->major,
				  auth_interface->class->name);
	if (auth_interface->class)
		class_destroy(auth_interface->class);

	kfree(auth_interface);
	sAuth = NULL;
} // mbridge_auth_destroy
