/*
 * Copyright (C) 2016 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 **/

#include <linux/usb/composite.h>

#include "bridge.h"
#include "hostdef.h"
#include "mgbuf.h"

//#define DFU_DEBUG_LOG_ENABLE

#define DFU_ENDPOINT_SINK_NUM_REQUESTS (16)
#define DFU_ENDPOINT_SINK_REQUEST_SIZE (512)

#if defined(DFU_DEBUG_LOG_ENABLE)
#define GADGET_LOG_DFU(LEVEL, FORMAT, ...)                                                         \
	if ((LEVEL) <= LOG_LEVEL) {                                                                \
		const char *file = __FILE__;                                                       \
		if (strlen(file) > 20) {                                                           \
			file = file + strlen(file) - 20;                                           \
		}                                                                                  \
		printk("%s:%d (%s) -" FORMAT, file, __LINE__, __FUNCTION__, ##__VA_ARGS__);        \
	}
#else
#define GADGET_LOG_DFU(LEVEL, FORMAT, ...)
#endif

static atomic64_t s_received_size = ATOMIC64_INIT(0);

static struct usb_interface_descriptor dfu_intf = {
	.bLength = sizeof(dfu_intf),
	.bDescriptorType = USB_DT_INTERFACE,
	.bInterfaceNumber = MORPHEUS_DFU_INTERFACE,
	.bAlternateSetting = 0,
	.bNumEndpoints = 1,
	.bInterfaceClass = USB_CLASS_VENDOR_SPEC,
	.bInterfaceSubClass = 1,
	.bInterfaceProtocol = 0,
	.iInterface = 0, // set at initialize
};

static const struct usb_endpoint_descriptor fs_dfu_sink_desc = {
	// using same descriptor in FS/HS
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = (MORPHEUS_DFU_ENDPOINT_SINK | USB_DIR_OUT),
	.bmAttributes = USB_ENDPOINT_XFER_BULK,
	.wMaxPacketSize = cpu_to_le16(64),
};

static const struct usb_endpoint_descriptor hs_dfu_sink_desc = {
	// using same descriptor in FS/HS
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = (MORPHEUS_DFU_ENDPOINT_SINK | USB_DIR_OUT),
	.bmAttributes = USB_ENDPOINT_XFER_BULK,
	.wMaxPacketSize = cpu_to_le16(DFU_ENDPOINT_SINK_REQUEST_SIZE),
};

static const struct usb_descriptor_header *fs_dfu_descs[] = {
	(struct usb_descriptor_header *)&dfu_intf,
	(struct usb_descriptor_header *)&fs_dfu_sink_desc,
	NULL,
};

static const struct usb_descriptor_header  *hs_dfu_descs[] = {
	(struct usb_descriptor_header *)&dfu_intf,
	(struct usb_descriptor_header *)&hs_dfu_sink_desc,
	NULL,
};

struct mbridge_dfu_interface
{
	struct usb_function interface;
	struct usb_ep *sink_ep;
	struct mgbuf mgbuf;
	unsigned long flags;
#define EP_ENABLED (0)
	int major; /* userspace device node */
	struct class *class;
	struct device *nodedev;
};
#define to_mbridge_dfu_interface(f) \
	container_of(f, struct mbridge_dfu_interface, interface)

static void mbridge_dfu_complete(struct mgbuf *mgbuf, struct usb_ep *ep, struct usb_request *req)
{
	switch (req->status) {
	case 0:
		atomic64_add(req->actual, &s_received_size);
		break;
	default:
		break;
	}
}

static int mbridge_dfu_bind(struct usb_configuration *c, struct usb_function *f)
{
	struct mbridge_dfu_interface *dfu_interface;
	struct mgbuf *mgbuf;

	dfu_interface = to_mbridge_dfu_interface(f);
	mgbuf = &dfu_interface->mgbuf;

	GADGET_LOG_DFU(LOG_DBG, "\n");

	/* Configure endpoint from the descriptor. */
	dfu_interface->sink_ep = usb_ep_autoconfig(c->cdev->gadget,
						   (struct usb_endpoint_descriptor *)
						   &fs_dfu_sink_desc);
	if (dfu_interface->sink_ep == NULL) {
		GADGET_LOG_DFU(LOG_ERR, "can't autoconfigure dfu endpoint\n");
		return -ENODEV;
	}

	/* Need to set this during setup only. */
	dfu_interface->sink_ep->driver_data = c->cdev;

	GADGET_LOG_DFU(LOG_DBG, "epname=%s address=0x%x\n", dfu_interface->sink_ep->name,
		       dfu_interface->sink_ep->address);

	if (mgbuf_init(mgbuf, "dfu",
		       dfu_interface->sink_ep,
		       DFU_ENDPOINT_SINK_REQUEST_SIZE,
		       DFU_ENDPOINT_SINK_NUM_REQUESTS,
		       usb_endpoint_xfer_isoc(&fs_dfu_sink_desc),
		       NULL, 0, 0, 0)) {
		GADGET_LOG_DFU(LOG_ERR, "can't initialize mgbuf\n");
		return -ENODEV;
	}
	mgbuf_set_usbconnected(&dfu_interface->mgbuf, 0);

	/* handle completion */
	mgbuf->complete[MGBUF_OUT] = mbridge_dfu_complete;

	return 0;
}

static void mbridge_dfu_unbind(struct usb_configuration *c, struct usb_function *f)
{
	struct mbridge_dfu_interface *dfu_interface;
	struct mgbuf *mgbuf;

	dfu_interface = to_mbridge_dfu_interface(f);
	mgbuf = &dfu_interface->mgbuf;

	mgbuf_deinit(mgbuf);
}

static int mbridge_dfu_enable(struct usb_function *f)
{
	struct mbridge_dfu_interface *dfu_interface;
	struct mgbuf *mgbuf;
	int ret;

	dfu_interface = to_mbridge_dfu_interface(f);
	mgbuf = &dfu_interface->mgbuf;

	GADGET_LOG_DFU(LOG_DBG, "enabling dfu interface...\n");

	/* Enable the endpoint. */
	ret = config_ep_by_speed(f->config->cdev->gadget, f, dfu_interface->sink_ep);
	if (ret) {
		GADGET_LOG_DFU(LOG_ERR, "error configuring dfu endpoint\n");
		return ret;
	}

	if (!test_and_set_bit(EP_ENABLED, &dfu_interface->flags)) {
		ret = usb_ep_enable(dfu_interface->sink_ep);
		if (ret) {
			clear_bit(EP_ENABLED, &dfu_interface->flags);
			GADGET_LOG_DFU(LOG_ERR, "error starting dfu endpoint\n");
			return ret;
		}
		ret = mgbuf_pre_queue(mgbuf, 1, 0);
		if (ret) {
			clear_bit(EP_ENABLED, &dfu_interface->flags);
			usb_ep_disable(dfu_interface->sink_ep);
			return ret;
		}
		mgbuf_set_usbconnected(&dfu_interface->mgbuf, 1);
	}

	return 0;
}

static void mbridge_dfu_disable(struct usb_function *f)
{
	struct mbridge_dfu_interface *dfu_interface;

	dfu_interface = to_mbridge_dfu_interface(f);

	GADGET_LOG_DFU(LOG_DBG, "disabling chat interface...\n");

	if (test_and_clear_bit(EP_ENABLED, &dfu_interface->flags))
		usb_ep_disable(dfu_interface->sink_ep);
}

static int mbridge_dfu_reset(struct usb_function *f, unsigned intf, unsigned alt)
{
	int ret;

	// we have only one interface
	if (alt)
		return -EINVAL;

	mbridge_dfu_disable(f);
	ret = mbridge_dfu_enable(f);
	GADGET_LOG(LOG_DBG_DFU, "intf(%d), ret=%d\n", intf, ret);
	return ret;
}

struct mbridge_dfu_interface s_dfu_interface = {
	.interface = {
		.name = "dfu",
		.fs_descriptors = (struct usb_descriptor_header **)fs_dfu_descs,
		.hs_descriptors = (struct usb_descriptor_header **)hs_dfu_descs,
		.bind = mbridge_dfu_bind,
		.unbind = mbridge_dfu_unbind,
		.set_alt = mbridge_dfu_reset,
		.disable = mbridge_dfu_disable,
	},
};

//----------------------------------------------------------------------------
static int device_dfu_open(struct inode *inode, struct file *filp)
{
	filp->private_data = &s_dfu_interface.mgbuf;
	return mgbuf_open(&s_dfu_interface.mgbuf, inode, filp);
}

static int device_dfu_release(struct inode *inode, struct file *filp)
{
	struct mgbuf *mgbuf = filp->private_data;

	return mgbuf_release(mgbuf, inode, filp);
}

static ssize_t device_dfu_read(struct file *filp, char *dest, size_t length, loff_t *offset)
{
	ssize_t ret;
	struct mgbuf *mgbuf = filp->private_data;

	ret = mgbuf_read(mgbuf, filp, dest, length, offset);

	return ret;
}

static unsigned int device_dfu_poll(struct file *filp, poll_table *wait)
{
	struct mgbuf *mgbuf = filp->private_data;

	return mgbuf_poll(mgbuf, filp, wait);
}

static long device_dfu_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int Result = 0;

	switch (cmd) {
	case HOST_IOCTL_GET_DFU_RECEIVED_SIZE_DEBUG:
		Result = put_user(atomic64_read(&s_received_size), (uint64_t *)arg);
		break;

	case HOST_IOCTL_GET_DFU_PUSHED_SIZE_DEBUG:
	case HOST_IOCTL_GET_DFU_POPPED_SIZE_DEBUG:
		Result = put_user(0, (uint64_t *)arg );
		break;

	case HOST_IOCTL_RESET_DFU_SIZE_DEBUG:
		atomic64_set(&s_received_size, 0);
		break;
	case HOST_IOCTL_QUIESCENT_EP:
		Result = -EINVAL;
		break;
	default:
		break;
	}
	return Result;
}

//----------------------------------------------------------------------------

static struct file_operations dfu_fops = {
	.read = device_dfu_read,
	.poll = device_dfu_poll,
	.unlocked_ioctl = device_dfu_ioctl,
	.open = device_dfu_open,
	.release = device_dfu_release,
};

static void dfu_usb_disconnect(struct usb_function *f)
{
	struct mbridge_dfu_interface *intf =
		container_of(f, struct mbridge_dfu_interface, interface);

	mgbuf_set_usbconnected(&intf->mgbuf, 0);
}

int mbridge_dfu_init(struct usb_composite_dev *cdev, struct usb_function **interface,
	mbridge_usb_disconnect *disconn_cb)
{
	int ret;

	pr_info("%s: start\n", __func__);
	ret = usb_string_id(cdev);
	if (ret < 0)
		return ret;

	strings_dev_dfu[STRING_DFU_IDX].id = ret;
	dfu_intf.iInterface = ret;

	/* register devnode */
	s_dfu_interface.class = class_create(THIS_MODULE, "pu_dfu");
	if (IS_ERR(s_dfu_interface.class)) {
		dev_err(&cdev->gadget->dev, "failed to create class\n");
		return PTR_ERR(s_dfu_interface.class);
	}
	ret = register_chrdev(0, s_dfu_interface.class->name, &dfu_fops);
	if (ret < 0) {
		dev_err(&cdev->gadget->dev, "failed to register chrdev\n");
		goto rewind1;
	}
	s_dfu_interface.major = ret;
	s_dfu_interface.nodedev = device_create(s_dfu_interface.class, NULL,
					       MKDEV(ret, 0), &s_dfu_interface,
					       s_dfu_interface.class->name);
	if (IS_ERR(s_dfu_interface.nodedev)) {
		dev_err(&cdev->gadget->dev, "failed to create device\n");
		ret = PTR_ERR(s_dfu_interface.nodedev);
		goto rewind2;
	}

	clear_bit(EP_ENABLED, &s_dfu_interface.flags);

	*interface = &s_dfu_interface.interface;
	*disconn_cb = dfu_usb_disconnect;

	GADGET_LOG_DFU(LOG_INFO, "Initialized.\n");
	return 0;

rewind2:
	unregister_chrdev(s_dfu_interface.major, s_dfu_interface.class->name);
rewind1:
	class_destroy(s_dfu_interface.class);
	return ret;
}

void mbridge_dfu_destroy(struct usb_function *interface)
{
	if (s_dfu_interface.nodedev) {
		device_destroy(s_dfu_interface.class,
			       MKDEV(s_dfu_interface.major,0));
		s_dfu_interface.nodedev = NULL;
	}
	if (s_dfu_interface.major) {
		unregister_chrdev(s_dfu_interface.major,
				  s_dfu_interface.class->name);
		s_dfu_interface.major = 0;
	}
	if (s_dfu_interface.class) {
		class_destroy(s_dfu_interface.class);
		s_dfu_interface.class = NULL;
	}
}
