/*
 * Copyright (C) 2016 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef _MGBUF_H
#define _MGBUF_H

#include <linux/ioctl.h>
#include <linux/types.h>
struct mgbuf;

enum mgbuf_dir {
	MGBUF_OUT,       // PS4 -> PU
	MGBUF_IN,        // PU  -> PS4
};

enum mgbuf_qkind {
	MGBUF_Q_BUSY,    // submitted to UDC and not given back
	MGBUF_Q_FREE,    // ready for submitting to UDC
	MGBUF_Q_FILLED,  // valid data filled (or with error status)
	MGBUF_Q_UOWN,    // owned by user with ioctl
};

/*
 * for ioctl
 */
struct mgbuf_block_param {
	int version;         // in: set sizeof(mgbuf_block_param)
	enum mgbuf_dir dir;  // in: specify EP direction
	void *bufaddr;       // out: data buffer mapped on user space
	int status;          // out: status of usb_request
	size_t validlen;     // out: length of data
	                     //      only if status == 0 when OUT
	                     //      When IN, specify size of tx length
	void *priv;          // in/out: for library use. dont modify!
};

/* driver returns if IOCTL_GET_INFO issued */
struct mgbuf_info {
	int version;          // in: set sizeof(mbuf_info)
	short packetlen[2];   // out: packet size. Zero if unused
	short items[2];       // out: buffer item count
	size_t areasize[2];   // out: mmap area size
};

#define MGBUF_IOCTL_GET_MEM_BLOCK _IOWR('m', 1, struct mgbuf_block_param)
#define MGBUF_IOCTL_PUT_MEM_BLOCK _IOWR('m', 2, struct mgbuf_block_param)
#define MGBUF_IOCTL_GET_INFO      _IOWR('m', 3, struct mgbuf_info)

#ifdef __KERNEL__
#include <asm/uaccess.h>
#include <linux/fs.h>
#include <linux/list.h>
#include <linux/poll.h>
#include <linux/usb/gadget.h>
#include <linux/sched.h>
#include <linux/wait.h>

#define MGBUF_NAME_LEN (16)

struct mgbuf_dmabuf {
	struct list_head list;
	struct usb_request *req; // usb_request mapped with this
	struct usb_ep *ep;       // associated EP
	void *vaddr;             // kernel virtual address of this segment
	unsigned long vm_off;    // offset from top of the buffer
	size_t validlen;         // valid data size returned by UDC
	int status;              // copy of usb_request.status
	enum mgbuf_dir dir;      // direction
	wait_queue_head_t waitq; // for sync write (IN)
	struct mgbuf *mgbuf;     // parent
};

#define QUEUE_MAX (16)  // maximum allowable queue count in dwc3
struct mgbuf {
	// io lock
	struct mutex io_mutex[2];
	// lists for an mbuf_dmabuf
	spinlock_t lock[2];                // lock for following lists
	struct list_head freelist[2];      // unused. ready for usb_ep_request()
	struct list_head filledlist[2];    // data buffer holding (OUT only)
	struct list_head busylist[2];      // enqueued to udc
	struct list_head uownlist[2];      // user owned (by ioctl GET)

	atomic_t busycount[2];             // how many requests in UDC queue

	wait_queue_head_t waitq[2];
	size_t packetlen[2];               // buffer size of each item
	unsigned int items[2];             // number of mgbuf_dmabuf
	// DMA buffer
	void *vaddr[2];                    // virtual kernel address of DMA buffer
	unsigned int order[2];             // area order
	size_t areasize[2];                // remapped size
	struct mgbuf_dmabuf *dmabufs[2];
	// mmap info
	unsigned long vm_start[2];         // user addr of 'vaddr'
	unsigned long vm_end[2];

	atomic_t ref;                      // for kmalloc memory and usb_request
	// callbacks (usb_request)
	void (*complete[2])(struct mgbuf *mgbuf,
			    struct usb_ep *ep,
			    struct usb_request *req);
	// EPs associated
	struct usb_ep *ep[2];

	unsigned long flags;
#define MGBUF_FLAG_INIT      (0)           // mgbuf_init() already called
#define MGBUF_FLAG_OPEN      (1)           // open(2) called
#define MGBUF_FLAG_DISCONN   (2)           // plugged off from usb host
	                                   // set/reset by mgbuf user
	                                   // default state is unset(=connected)
	// stat
	unsigned int stat_error[2];        // error reported in usb_request.stat
	unsigned int error[2];             // misc error on underlaying layer

	// misc
	char name[MGBUF_NAME_LEN];         // identifier (just for debug)
	void *user;                        // user scratch pad

	// stat/debug
	struct dentry *dbgroot;
	int completed[2];
	int zlp[2];
	int wakeup[2];
	int irw[2];
	int woken[2];
	int busyfill[2];
	int isoc[2];
	ktime_t lastcb[2];
};
// filesystem_ops
extern ssize_t mgbuf_read(struct mgbuf *mgbuf, struct file *file, char *buffer,
			  size_t count, loff_t *ppos);
extern ssize_t mgbuf_write(struct mgbuf *mgbuf, struct file *file, const char *buffer,
			   size_t count, loff_t *ppos);
extern unsigned int mgbuf_poll(struct mgbuf *mgbuf, struct file *file, poll_table *wait);
extern int mgbuf_mmap(struct mgbuf *mgbuf, struct file *file, struct vm_area_struct *vma);
extern long mgbuf_ioctl(struct mgbuf *mgbuf, struct file *file, unsigned int cmd,
			unsigned long arg);
extern int mgbuf_open(struct mgbuf *mgbuf, struct inode *inode, struct file *file);
extern int mgbuf_release(struct mgbuf *mgbuf, struct inode *inode, struct file *file);
extern int mgbuf_fsync(struct mgbuf *mgbuf, struct file *file, loff_t start, loff_t end,
		       int datasync);
// mgbuf if
extern int mgbuf_init(struct mgbuf *mgbuf, const char *name,
		      struct usb_ep *oep, size_t opacketlen, unsigned int orequests, int oisoc,
		      struct usb_ep *iep, size_t ipacketlen, unsigned int irequests, int iisoc);
extern void mgbuf_deinit(struct mgbuf *mgbuf);

// to tell mgbuf about USB connection
// call it from bind/unbind or disconnect
extern void mgbuf_set_usbconnected(struct mgbuf *mgbuf, int connected);
extern int mgbuf_get_usbconnected(struct mgbuf *mgbuf);
extern void mgbuf_make_quiescent(struct mgbuf *mgbuf);

// Pre-queue some requests
extern int mgbuf_pre_queue(struct mgbuf *mgbuf, int start_out, int start_in);
// get item count in the specified queue
extern int mgbuf_get_queue_count(struct mgbuf *mgbuf, enum mgbuf_dir dir,
				 enum mgbuf_qkind qkind);
#endif // _KERNEL_
#endif // _MGBUF_H
