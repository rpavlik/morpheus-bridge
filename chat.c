/*
 * Copyright (C) 2016 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <linux/usb/audio.h>

//#define ENABLE_MBRIDGE_DEBUG
#include "bridge.h"
#include "hostdef.h"
#include "chat.h"
#include "mgbuf.h"

//----------------------------------------------------------------------------
#define OUT_EP_MAX_PACKET_SIZE_FS 64
#define OUT_EP_MAX_PACKET_SIZE 192
#define CHAT_INPUT_TERMINAL_ID 2
#define CHAT_ENDPOINT_SINK_NUM_REQUESTS 16
#define CHAT_ENDPOINT_SINK_REQUEST_SIZE 192

/* Standard AS Interface Descriptor */
static struct usb_interface_descriptor chat_as_interface_alt_0_desc = {
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bAlternateSetting = 0,
	.bNumEndpoints = 0,
	.bInterfaceClass = USB_CLASS_AUDIO,
	.bInterfaceSubClass = USB_SUBCLASS_AUDIOSTREAMING,
	.bInterfaceNumber = MORPHEUS_CHAT_INTERFACE,
};

static struct usb_interface_descriptor chat_as_interface_alt_1_desc = {
	.bLength = USB_DT_INTERFACE_SIZE,
	.bDescriptorType = USB_DT_INTERFACE,
	.bAlternateSetting = 1,
	.bNumEndpoints = 1,
	.bInterfaceClass = USB_CLASS_AUDIO,
	.bInterfaceSubClass = USB_SUBCLASS_AUDIOSTREAMING,
	.bInterfaceNumber = MORPHEUS_CHAT_INTERFACE,
};

/* B.4.2  Class-Specific AS Interface Descriptor */
static struct uac1_as_header_descriptor chat_as_header_desc = {
	.bLength = UAC_DT_AS_HEADER_SIZE,
	.bDescriptorType = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_AS_GENERAL,
	.bTerminalLink = CHAT_INPUT_TERMINAL_ID, // INPUT_TERMINAL_ID,
	.bDelay = 0,
	.wFormatTag = UAC_FORMAT_TYPE_I_PCM,
};

DECLARE_UAC_FORMAT_TYPE_I_DISCRETE_DESC(1);

static struct uac_format_type_i_discrete_descriptor_1 chat_as_type_i_desc = {
	.bLength = UAC_FORMAT_TYPE_I_DISCRETE_DESC_SIZE(1),
	.bDescriptorType = USB_DT_CS_INTERFACE,
	.bDescriptorSubtype = UAC_FORMAT_TYPE,
	.bFormatType = UAC_FORMAT_TYPE_I,
	.bNrChannels = 2,
	.bSubframeSize = 2,
	.bBitResolution = 16,
	.bSamFreqType = 1,
	.tSamFreq[0][0] = 0x80,
	.tSamFreq[0][1] = 0xBB,
	.tSamFreq[0][2] = 0x00
};

/* Standard ISO OUT Endpoint Descriptor for highspeed */
static struct usb_endpoint_descriptor chat_hs_as_out_ep_desc = {
	.bLength = USB_DT_ENDPOINT_AUDIO_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_CHAT_ENDPOINT_SINK | USB_DIR_OUT,
	.bmAttributes = USB_ENDPOINT_SYNC_ASYNC | USB_ENDPOINT_XFER_ISOC,
	.wMaxPacketSize = __constant_cpu_to_le16(OUT_EP_MAX_PACKET_SIZE),
	.bInterval = 4, /* poll 1 per millisecond */
};

/* Standard ISO OUT Endpoint Descriptor for fullhspeed */
static struct usb_endpoint_descriptor chat_fs_as_out_ep_desc = {
	.bLength = USB_DT_ENDPOINT_AUDIO_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_CHAT_ENDPOINT_SINK | USB_DIR_OUT,
	.bmAttributes = USB_ENDPOINT_SYNC_ASYNC | USB_ENDPOINT_XFER_ISOC,
	.wMaxPacketSize = __constant_cpu_to_le16(OUT_EP_MAX_PACKET_SIZE_FS),
	.bInterval = 1, /* poll 1 per millisecond */
};

/* Class-specific AS ISO OUT Endpoint Descriptor */
static struct uac_iso_endpoint_descriptor chat_as_iso_out_desc = {
	.bLength = UAC_ISO_ENDPOINT_DESC_SIZE,
	.bDescriptorType = USB_DT_CS_ENDPOINT,
	.bDescriptorSubtype = UAC_EP_GENERAL,
	.bmAttributes = 0x80, // 1,
	.bLockDelayUnits = 0,
	.wLockDelay = 0, //__constant_cpu_to_le16(1),
};

static struct usb_interface_assoc_descriptor iad_desc = {
	.bLength = sizeof iad_desc,
	.bDescriptorType = USB_DT_INTERFACE_ASSOCIATION,
	.bFirstInterface = 1,
	.bInterfaceCount = 3,
	.bFunctionClass = USB_CLASS_AUDIO,
	.bFunctionSubClass = 0, // UAC2_FUNCTION_SUBCLASS_UNDEFINED,
	.bFunctionProtocol = UAC_VERSION_1,
};

static struct usb_descriptor_header *fs_chat_descs[] = {
	(struct usb_descriptor_header *)&chat_as_interface_alt_0_desc,
	(struct usb_descriptor_header *)&chat_as_interface_alt_1_desc,
	(struct usb_descriptor_header *)&chat_as_header_desc,
	(struct usb_descriptor_header *)&chat_as_type_i_desc,
	(struct usb_descriptor_header *)&chat_fs_as_out_ep_desc,
	(struct usb_descriptor_header *)&chat_as_iso_out_desc,
	NULL,
};

static struct usb_descriptor_header *hs_chat_descs[] = {
	(struct usb_descriptor_header *)&chat_as_interface_alt_0_desc,
	(struct usb_descriptor_header *)&chat_as_interface_alt_1_desc,
	(struct usb_descriptor_header *)&chat_as_header_desc,
	(struct usb_descriptor_header *)&chat_as_type_i_desc,
	(struct usb_descriptor_header *)&chat_hs_as_out_ep_desc,
	(struct usb_descriptor_header *)&chat_as_iso_out_desc,
	NULL,
};

struct mbridge_chat_interface
{
	struct usb_function interface;
	struct usb_ep *sink_ep;
	struct mgbuf mgbuf;
	unsigned long flags;
#define EP_ENABLED (0)
	int alt;
	int major; /* userspace device node */
	struct class *class;
	struct device *nodedev;
};
#define to_mbridge_chat_interface(f) \
	container_of(f, struct mbridge_chat_interface, interface)

//----------------------------------------------------------------------------

static int mbridge_chat_bind(struct usb_configuration *c, struct usb_function *f)
{
	struct mbridge_chat_interface *chat_interface;
	struct mgbuf *mgbuf;

	chat_interface = to_mbridge_chat_interface(f);
	mgbuf = &chat_interface->mgbuf;

	/* Configure endpoint from the descriptor. */
	chat_interface->sink_ep = usb_ep_autoconfig(c->cdev->gadget, &chat_hs_as_out_ep_desc);
	if (chat_interface->sink_ep == NULL) {
		GADGET_LOG(LOG_ERR, "can't autoconfigure chat endpoint\n");
		return -ENODEV;
	}

	/* Need to set this during setup only. */
	chat_interface->sink_ep->driver_data = c->cdev;

	GADGET_LOG(LOG_DBG, "name=%s address=0x%x\n", chat_interface->sink_ep->name,
		   chat_interface->sink_ep->address);

	if (mgbuf_init(mgbuf, "chat",
		       chat_interface->sink_ep,
		       __roundup_pow_of_two(CHAT_ENDPOINT_SINK_REQUEST_SIZE),
		       CHAT_ENDPOINT_SINK_NUM_REQUESTS,
		       usb_endpoint_xfer_isoc(&chat_hs_as_out_ep_desc),
		       NULL, 0, 0, 0)) {
		GADGET_LOG(LOG_ERR, "can't initialize mgbuf\n");
		return -ENODEV;
	}
	mgbuf_set_usbconnected(&chat_interface->mgbuf, 0);

	return 0;
}

static void mbridge_chat_unbind(struct usb_configuration *c, struct usb_function *f)
{
	struct mbridge_chat_interface *chat_interface;
	struct mgbuf *mgbuf;

	chat_interface = to_mbridge_chat_interface(f);
	mgbuf = &chat_interface->mgbuf;

	mgbuf_deinit(mgbuf);
}

static int mbridge_chat_enable(struct usb_function *f)
{
	struct mbridge_chat_interface *chat_interface;
	struct mgbuf *mgbuf;
	int ret;

	chat_interface = to_mbridge_chat_interface(f);
	mgbuf = &chat_interface->mgbuf;

	GADGET_LOG(LOG_DBG, "enabling chat interface...\n");

	/* Enable the endpoint. */
	ret = config_ep_by_speed(f->config->cdev->gadget, f, chat_interface->sink_ep);
	if (ret) {
		GADGET_LOG(LOG_ERR, "error configuring chat endpoint\n");
		return ret;
	}

	if (!test_and_set_bit(EP_ENABLED, &chat_interface->flags)) {
		ret = usb_ep_enable(chat_interface->sink_ep);
		if (ret) {
			clear_bit(EP_ENABLED, &chat_interface->flags);
			GADGET_LOG(LOG_ERR, "error starting chat endpoint\n");
			return ret;
		}
		ret = mgbuf_pre_queue(mgbuf, 1, 0);
		if (ret) {
			clear_bit(EP_ENABLED, &chat_interface->flags);
			usb_ep_disable(chat_interface->sink_ep);
			return ret;
		}
		// say mgbuf host starts to communicate
		mgbuf_set_usbconnected(&chat_interface->mgbuf, 1);
	}

	return 0;
}

static void mbridge_chat_disable(struct usb_function *f)
{
	struct mbridge_chat_interface *chat_interface;

	chat_interface = to_mbridge_chat_interface(f);

	GADGET_LOG(LOG_DBG, "disabling chat interface...\n");

	if (test_and_clear_bit(EP_ENABLED, &chat_interface->flags))
		usb_ep_disable(chat_interface->sink_ep);
}

static int mbridge_chat_reset(struct usb_function *f, unsigned intf, unsigned alt)
{
	struct mbridge_chat_interface *chat_interface;
	GADGET_LOG(LOG_DBG, "intf(%d) alt(%d)\n", intf, alt);

	chat_interface = to_mbridge_chat_interface(f);

	chat_interface->alt = alt;

	mbridge_chat_disable(f);
	return mbridge_chat_enable(f);
}

static int mbridge_chat_get_alt(struct usb_function *f, unsigned intf)
{
	struct mbridge_chat_interface *chat_interface;

	chat_interface = to_mbridge_chat_interface(f);

	GADGET_LOG(LOG_DBG, "intf(%d) alt =(%d)\n", intf, chat_interface->alt);
	return chat_interface->alt;
}

//----------------------------------------------------------------------------
static struct mbridge_chat_interface s_chat_interface = {
	.interface = {
		.name = "chat",
		.fs_descriptors = fs_chat_descs,
		.hs_descriptors = hs_chat_descs,
		.bind = mbridge_chat_bind,
		.unbind = mbridge_chat_unbind,
		.set_alt = mbridge_chat_reset,
		.disable = mbridge_chat_disable,
		.get_alt = mbridge_chat_get_alt,

	},
};

//----------------------------------------------------------------------------
static int device_chat_open(struct inode *inode, struct file *filp)
{
	struct mgbuf *mgbuf = &s_chat_interface.mgbuf;
	// mgbuf: set mgbuf* to file->private_data
	filp->private_data = mgbuf;

	return mgbuf_open(mgbuf, inode, filp);
}

static int device_chat_release(struct inode *inode, struct file *filp)
{
	struct mgbuf *mgbuf = filp->private_data;

	return mgbuf_release(mgbuf, inode, filp);
}

// For device_chat_read:  for chat endpoint, read/write system call should return error
static ssize_t device_chat_read(struct file *filp, char *dest, size_t length, loff_t *offset)
{
	struct mgbuf *mgbuf = filp->private_data;

	return mgbuf_read(mgbuf, filp, dest, length, offset);
}

//----------------------------------------------------------------------------

static struct file_operations chat_fops = {
	.read = device_chat_read,
	.open = device_chat_open,
	.release = device_chat_release,
};

static void mbridge_chat_disconnect(struct usb_function *f)
{
	struct mbridge_chat_interface *intf =
		container_of(f, struct mbridge_chat_interface, interface);

	mgbuf_set_usbconnected(&intf->mgbuf, 0);
}

static void mbridge_chat_quiescent(struct usb_function *f)
{
	struct mbridge_chat_interface *intf =
		container_of(f, struct mbridge_chat_interface, interface);

	mgbuf_make_quiescent(&intf->mgbuf);
}

int mbridge_chat_init(struct usb_composite_dev *cdev, struct usb_function **interface,
		      mbridge_usb_disconnect *disconn_cb, mbridge_usb_quiescent *quiescent_cb)
{
	int ret;

	ret = usb_string_id(cdev);
	if (ret < 0)
		return ret;

	strings_dev[STRING_CHAT_IDX].id = ret;
	chat_as_interface_alt_0_desc.iInterface = ret;
	chat_as_interface_alt_1_desc.iInterface = ret;

	/* register devnode */
	s_chat_interface.class = class_create(THIS_MODULE, "pu_chat");
	if (IS_ERR(s_chat_interface.class)) {
		dev_err(&cdev->gadget->dev, "failed to create class\n");
		ret = PTR_ERR(s_chat_interface.class);
		return ret;
	}
	ret = register_chrdev(0, s_chat_interface.class->name, &chat_fops);
	if (ret < 0) {
		dev_info(&cdev->gadget->dev, "failed to register chrdev\n");
		goto rewind1;
	}
	s_chat_interface.major = ret;
	s_chat_interface.nodedev = device_create(s_chat_interface.class, NULL,
						MKDEV(ret, 0),
						&s_chat_interface,
						s_chat_interface.class->name);
	if (IS_ERR(s_chat_interface.nodedev)) {
		dev_err(&cdev->gadget->dev, "failed to create noddev\n");
		ret = PTR_ERR(s_chat_interface.nodedev);
		goto rewind2;
	}

	clear_bit(EP_ENABLED, &s_chat_interface.flags);

	*interface = &s_chat_interface.interface;
	*disconn_cb = mbridge_chat_disconnect;
	*quiescent_cb = mbridge_chat_quiescent;
	GADGET_LOG(LOG_DBG, "\n");

	return 0;

rewind2:
	unregister_chrdev(s_chat_interface.major, s_chat_interface.class->name);
rewind1:
	class_destroy(s_chat_interface.class);
	return ret;
}

void mbridge_chat_destroy(struct usb_function *interface)
{
	if (s_chat_interface.nodedev) {
		device_destroy(s_chat_interface.class,
			       MKDEV(s_chat_interface.major,0));
		s_chat_interface.nodedev = NULL;
	}
	if (s_chat_interface.major) {
		unregister_chrdev(s_chat_interface.major,
				  s_chat_interface.class->name);
		s_chat_interface.major = 0;
	}
	if (s_chat_interface.class) {
		class_destroy(s_chat_interface.class);
		s_chat_interface.class = NULL;
	}
}
