/*
 * Copyright (C) 2016 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <linux/hid.h>

#include "bridge.h"
#include "command.h"
#include "hostdef.h"
#include "mgbuf.h"

//#define CMD_DEBUG_LOG_ENABLE

#if defined(CMD_DEBUG_LOG_ENABLE)
#define GADGET_CMD_LOG(LEVEL, FORMAT, ...)                                                         \
	if ((LEVEL) <= LOG_LEVEL) {                                                                \
		const char *file = __FILE__;                                                       \
		if (strlen(file) > 20) {                                                           \
			file = file + strlen(file) - 20;                                           \
		}                                                                                  \
		printk("%s:%d (%s) -" FORMAT, file, __LINE__, __FUNCTION__, ##__VA_ARGS__);        \
	}
#else
#define GADGET_CMD_LOG(LEVEL, FORMAT, ...)
#endif

#define CMD_REPORT_DESC_LENGTH (23)

#define COMMAND_ENDPOINT_SOURCE_NUM_REQUESTS 16
#define COMMAND_ENDPOINT_SOURCE_REQUEST_SIZE 64
#define COMMAND_ENDPOINT_SINK_NUM_REQUESTS 16
#define COMMAND_ENDPOINT_SINK_REQUEST_SIZE 64

/* USB Descriptors */
static char CMDReportDesc[CMD_REPORT_DESC_LENGTH] = {
	0x06, 0x00, 0xff,
	0x15, 0x00,
	0x26, 0xff, 0x00,
	0x09, 0x08,
	0xa1, 0x01,
	0x09, 0x80,
	0x85, 0x40,
	0x75, 0x08,
	0x95, 0x04,
	0xb1, 0x02,
	0xc0,
};

static struct usb_interface_descriptor cmd_intf = {
	.bLength = sizeof(cmd_intf),
	.bDescriptorType = USB_DT_INTERFACE,
	.bAlternateSetting = 0,
	.bNumEndpoints = 2,
	.bInterfaceClass = USB_CLASS_HID,
	.bInterfaceNumber = MORPHEUS_COMMAND_INTERFACE, // Normal:5, DFU:0
};

static struct hid_descriptor cmd_hid_desc = {
	.bLength = sizeof(cmd_hid_desc),
	.bDescriptorType = HID_DT_HID,
	.bcdHID = 0x0111,
	.bCountryCode = 0x00,
	.bNumDescriptors = 0x1,
};

static struct usb_endpoint_descriptor fs_cmd_source_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_COMMAND_ENDPOINT_SOURCE | USB_DIR_IN,
	.bmAttributes = USB_ENDPOINT_XFER_INT,
	.wMaxPacketSize = cpu_to_le16(COMMAND_ENDPOINT_SOURCE_REQUEST_SIZE),
	.bInterval = 1, /* 1ms */
};

static struct usb_endpoint_descriptor hs_cmd_source_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_COMMAND_ENDPOINT_SOURCE | USB_DIR_IN,
	.bmAttributes = USB_ENDPOINT_XFER_INT,
	.wMaxPacketSize = cpu_to_le16(COMMAND_ENDPOINT_SOURCE_REQUEST_SIZE),
	.bInterval = 4, /* 1ms */
};

static struct usb_endpoint_descriptor fs_cmd_sink_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_COMMAND_ENDPOINT_SINK | USB_DIR_OUT,
	.bmAttributes = USB_ENDPOINT_XFER_INT,
	.wMaxPacketSize = cpu_to_le16(COMMAND_ENDPOINT_SINK_REQUEST_SIZE),
	.bInterval = 1, /* 1ms */
};

static struct usb_endpoint_descriptor hs_cmd_sink_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_COMMAND_ENDPOINT_SINK | USB_DIR_OUT,
	.bmAttributes = USB_ENDPOINT_XFER_INT,
	.wMaxPacketSize = cpu_to_le16(COMMAND_ENDPOINT_SINK_REQUEST_SIZE),
	.bInterval = 4, /* 1ms */
};

static struct usb_descriptor_header *fs_cmd_descs[] = {
	(struct usb_descriptor_header *)&cmd_intf,
	(struct usb_descriptor_header *)&cmd_hid_desc,
	(struct usb_descriptor_header *)&fs_cmd_source_desc,
	(struct usb_descriptor_header *)&fs_cmd_sink_desc,
	NULL
};

static struct usb_descriptor_header *hs_cmd_descs[] = {
	(struct usb_descriptor_header *)&cmd_intf,
	(struct usb_descriptor_header *)&cmd_hid_desc,
	(struct usb_descriptor_header *)&hs_cmd_source_desc,
	(struct usb_descriptor_header *)&hs_cmd_sink_desc,
	NULL
};

struct mbridge_cmd_interface
{
	struct usb_function interface;

	struct usb_ep *source_ep;
	struct usb_ep *sink_ep;

	struct mgbuf mgbuf;

	unsigned long flags;
#define CMD_EP_ENABLED (0)
#define CMD_EP_QUEUED (1)

	int major; /* userspace device node */
	struct class *class;
	struct device *nodedev;
};
#define to_mbridge_cmd_interface(f) \
	container_of(f, struct mbridge_cmd_interface, interface)
/* USB Descriptors */

/* mbridge PS4 Command */

#define MBRIDGE_CTRLCMD_OUT_MAX_LEN (64)
#define MBRIDGE_CTRLCMD_MAX_PAYLOAD_LEN (60)

#pragma pack(1)
struct mbridge_cmd_status
{
	uint8_t r_id;
	uint8_t command_status;
	uint8_t reserved[2];
};

struct mbridge_ctrlcmd_out_packet
{
	struct mbridge_cmd_status header;
	uint8_t payload[MBRIDGE_CTRLCMD_MAX_PAYLOAD_LEN];
};

struct mbridge_cmd_command
{
	uint8_t r_id;
	uint8_t command;
	uint8_t reserved[2];
};

struct mbridge_ctrlcmd_in_packet
{
	struct mbridge_cmd_command header;
	uint8_t payload[MBRIDGE_CTRLCMD_MAX_PAYLOAD_LEN];
};

struct mbridge_ctrlcmd_report
{
	struct mbridge_ctrlcmd_out_packet packet;
	uint8_t packet_length;
};

#pragma pack()

#define MBRIDGE_CTRLCMD_REPORT_NUM (256)
static struct mbridge_ctrlcmd_report s_ctrlcmd_report_list[MBRIDGE_CTRLCMD_REPORT_NUM];
static spinlock_t s_ctrlcmd_report_list_lock;
static struct mbridge_ctrlcmd_in_packet s_ctrlcmd_set_report_request;
static int s_ctrlcmd_set_report_request_is_receipt;

/* mbridge PS4 Command */

// For sending signals when USB cable connected/disconnected
static int usbCableConnectedState = false;

//----------------------------------------------------------------------------

static int mbridge_cmd_bind(struct usb_configuration *c, struct usb_function *f)
{
	struct mbridge_cmd_interface *cmd_interface =	to_mbridge_cmd_interface(f);
	struct mgbuf *mgbuf = &cmd_interface->mgbuf;

	/* Configure endpoint from the descriptor. */
	cmd_interface->source_ep = usb_ep_autoconfig(c->cdev->gadget, &fs_cmd_source_desc);
	if (cmd_interface->source_ep == NULL) {
		GADGET_CMD_LOG(LOG_ERR, "can't autoconfigure cmd endpoint\n");
		return -ENODEV;
	}

	GADGET_CMD_LOG(LOG_DBG, "epname=%s address=0x%x\n", cmd_interface->source_ep->name,
		       cmd_interface->source_ep->address);
	/* Need to set this during setup only. */
	cmd_interface->source_ep->driver_data = c->cdev;

	cmd_interface->sink_ep = usb_ep_autoconfig(c->cdev->gadget, &fs_cmd_sink_desc);
	if (cmd_interface->sink_ep == NULL) {
		GADGET_CMD_LOG(LOG_ERR, "can't autoconfigure cmd endpoint\n");
		return -ENODEV;
	}

	GADGET_CMD_LOG(LOG_DBG, "epname=%s address=0x%x\n", cmd_interface->sink_ep->name,
		       cmd_interface->sink_ep->address);
	/* Need to set this during setup only. */
	cmd_interface->sink_ep->driver_data = c->cdev;

	cmd_hid_desc.desc[0].bDescriptorType = HID_DT_REPORT;
	cmd_hid_desc.desc[0].wDescriptorLength =
		cpu_to_le16(CMD_REPORT_DESC_LENGTH);
	usb_assign_descriptors(f, fs_cmd_descs, hs_cmd_descs, NULL);

	if (mgbuf_init(mgbuf, "command",
		       cmd_interface->sink_ep,
		       __roundup_pow_of_two(COMMAND_ENDPOINT_SINK_REQUEST_SIZE),
		       COMMAND_ENDPOINT_SINK_NUM_REQUESTS,
		       usb_endpoint_xfer_isoc(&fs_cmd_sink_desc),
		       cmd_interface->source_ep,
		       __roundup_pow_of_two(COMMAND_ENDPOINT_SOURCE_REQUEST_SIZE),
		       COMMAND_ENDPOINT_SOURCE_NUM_REQUESTS,
		       usb_endpoint_xfer_isoc(&fs_cmd_source_desc))) {
		GADGET_CMD_LOG(LOG_ERR, "can't initialize mgbuf\n");
		return -ENODEV;
	}
	mgbuf_set_usbconnected(mgbuf, 0);

	return 0;
}
static void mbridge_cmd_unbind(struct usb_configuration *c, struct usb_function *f)
{
	struct mbridge_cmd_interface *cmd_interface = to_mbridge_cmd_interface(f);

	mgbuf_deinit(&cmd_interface->mgbuf);

	usbCableConnectedState = false;
}

static int mbridge_cmd_enable(struct usb_function *f)
{
	struct mbridge_cmd_interface *cmd_interface = to_mbridge_cmd_interface(f);
	struct mgbuf *mgbuf = &cmd_interface->mgbuf;
	int ret;

	GADGET_CMD_LOG(LOG_DBG, "enabling cmd interface...\n");

	/* Enable the endpoint. */
	ret = config_ep_by_speed(f->config->cdev->gadget, f, cmd_interface->source_ep);
	if (ret) {
		GADGET_CMD_LOG(LOG_ERR, "error configuring cmd endpoint\n");
		return ret;
	}

	ret = config_ep_by_speed(f->config->cdev->gadget, f, cmd_interface->sink_ep);
	if (ret) {
		GADGET_CMD_LOG(LOG_ERR, "error configuring cmd endpoint\n");
		return ret;
	}

	if (!test_and_set_bit(CMD_EP_ENABLED, &cmd_interface->flags)) {
		ret = usb_ep_enable(cmd_interface->source_ep);
		if (ret) {
			GADGET_CMD_LOG(LOG_ERR, "error starting cmd endpoint\n");
			goto rewind0;
		}

		ret = usb_ep_enable(cmd_interface->sink_ep);
		if (ret) {
			GADGET_CMD_LOG(LOG_ERR, "error starting cmd endpoint\n");
			goto rewind1;
		}

		if(!test_and_set_bit(CMD_EP_QUEUED, &cmd_interface->flags)) {
			ret = mgbuf_pre_queue(mgbuf, 1, 0);
			if(ret) {
				GADGET_CMD_LOG(LOG_ERR, "failed queuing usb request\n");
				goto rewind2;
			}
		}

		// say mgbuf host starts to communicate
		mgbuf_set_usbconnected(mgbuf, 1);
	}

	return 0;
rewind2:
	clear_bit(CMD_EP_QUEUED, &cmd_interface->flags);
	usb_ep_disable(cmd_interface->sink_ep);
rewind1:
	usb_ep_disable(cmd_interface->source_ep);
rewind0:
	clear_bit(CMD_EP_ENABLED, &cmd_interface->flags);

	return ret;
}

static void mbridge_cmd_disable(struct usb_function *f)
{
	struct mbridge_cmd_interface *cmd_interface = to_mbridge_cmd_interface(f);

	GADGET_CMD_LOG(LOG_DBG, "disabling cmd interface...\n");

	if (test_and_clear_bit(CMD_EP_ENABLED, &cmd_interface->flags)) {
		clear_bit(CMD_EP_QUEUED, &cmd_interface->flags);
		usb_ep_disable(cmd_interface->source_ep);
		usb_ep_disable(cmd_interface->sink_ep);
	}
}

static int mbridge_cmd_reset(struct usb_function *f, unsigned intf, unsigned alt)
{
	int result = -1;
	GADGET_CMD_LOG(LOG_DBG, "intf(%d)\n", intf);

	mbridge_cmd_disable(f);
	result = mbridge_cmd_enable(f);

	if (result == 0) {
		// When command I/F is opened, detect USB connection.
		usbCableConnectedState = true;
	}
	return result;
}

static void mbridge_cmd_ctrl_complete(struct usb_ep *ep, struct usb_request *req)
{
	struct mbridge_ctrlcmd_in_packet *received_pkt = NULL;
	struct mbridge_ctrlcmd_report *report_data_status;
	unsigned len;
	unsigned long flags;

	GADGET_CMD_LOG(LOG_INFO, "CTRL comp called\n");

	received_pkt = req->buf;

	len = req->actual;

	GADGET_CMD_LOG(LOG_INFO, "Set Report R-ID: %02X\n", received_pkt->header.r_id);

	spin_lock_irqsave(&s_ctrlcmd_report_list_lock, flags);

	report_data_status = &(s_ctrlcmd_report_list[MBRIDGE_R_ID_GET_COMMAND_STATUS]);
	if (report_data_status->packet.header.command_status != MBRIDGE_CTRL_STATUS_PROCESS) {
		report_data_status->packet.header.r_id = received_pkt->header.r_id;
		report_data_status->packet.header.command_status = MBRIDGE_CTRL_STATUS_PROCESS;
		report_data_status->packet_length = sizeof(report_data_status->packet.header);

		spin_unlock_irqrestore(&s_ctrlcmd_report_list_lock, flags);

		memset(&s_ctrlcmd_set_report_request, 0, sizeof(s_ctrlcmd_set_report_request));
		memcpy(&s_ctrlcmd_set_report_request, received_pkt, min(len, sizeof(s_ctrlcmd_set_report_request)) );
		s_ctrlcmd_set_report_request_is_receipt = 1;
	}
	else {
		spin_unlock_irqrestore(&s_ctrlcmd_report_list_lock, flags);

		usb_ep_set_halt(ep);
	}
}

static int mbridge_cmd_get_report(struct usb_function *f, const struct usb_ctrlrequest *ctrl)
{
	struct usb_composite_dev *cdev = f->config->cdev;
	struct usb_request *req = cdev->req;
	uint16_t value = __le16_to_cpu(ctrl->wValue);
	uint16_t length = __le16_to_cpu(ctrl->wLength);
	uint8_t r_id = (uint8_t)(value & 0x00FF);
	unsigned long flags;

	GADGET_CMD_LOG(LOG_INFO, "R-ID: %02X\n", r_id);

	spin_lock_irqsave(&s_ctrlcmd_report_list_lock, flags);

	req->length = min3((unsigned)s_ctrlcmd_report_list[r_id].packet_length,
			   (unsigned)length,
			   (unsigned)MBRIDGE_CTRLCMD_OUT_MAX_LEN);
	memcpy(req->buf, &(s_ctrlcmd_report_list[r_id].packet), req->length);

	spin_unlock_irqrestore(&s_ctrlcmd_report_list_lock, flags);

	return 0;
}

static int mbridge_cmd_setup(struct usb_function *f, const struct usb_ctrlrequest *ctrl)
{
	struct usb_composite_dev *cdev = f->config->cdev;
	struct usb_request *req = cdev->req;
	int status = 0;
	int is_stall = 1;
	__u16 value, length;

	value = __le16_to_cpu(ctrl->wValue);
	length = __le16_to_cpu(ctrl->wLength);

	GADGET_CMD_LOG(LOG_DBG, "mbridge_hid_setup\n");
	switch ((ctrl->bRequestType << 8) | ctrl->bRequest) {
	// Class Request
	case((USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8 | HID_REQ_GET_REPORT) :
		GADGET_CMD_LOG(LOG_DBG, "get_report\n");
		/* handle get report */

		if (mbridge_cmd_get_report(f, ctrl) >= 0) {
			GADGET_CMD_LOG(LOG_INFO, "get_report succeeded\n");
			is_stall = 0;
		} else {
			GADGET_CMD_LOG(LOG_INFO, "Push Decriptor\n");
			/* Error case: send report descriptor */
			length =
				min_t(unsigned, length, CMD_REPORT_DESC_LENGTH);
			memcpy(req->buf, CMDReportDesc, length);
			req->length = length;
		}
		break;

	case((USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8 | HID_REQ_SET_REPORT) :
		GADGET_CMD_LOG(LOG_DBG, "set_report | wLength=%d\n", ctrl->wLength);
		req->length = length;
		// req->buf is read in mbridge_cmd_ctrl_complete.
		// So, if it is called after get_report, returned buffer will be processed.
		req->complete = mbridge_cmd_ctrl_complete;
		is_stall = 0;
		break;

	// We won't support get/set protocol.
	case((USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8 | HID_REQ_GET_PROTOCOL) :
		GADGET_CMD_LOG(LOG_DBG, "get_protocol\n");
		break;
	case((USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8 | HID_REQ_SET_PROTOCOL) :
		GADGET_CMD_LOG(LOG_DBG, "set_protocol\n");
		break;

	// Standard Request
	case((USB_DIR_IN | USB_TYPE_STANDARD | USB_RECIP_INTERFACE) << 8 | USB_REQ_GET_DESCRIPTOR) :
		GADGET_CMD_LOG(LOG_DBG, "USB_REQ_GET_DESCRIPTOR\n");
		switch (value >> 8) {
		case HID_DT_HID:
			GADGET_CMD_LOG(LOG_DBG, "USB_REQ_GET_DESCRIPTOR: HID\n");
			length = min_t(unsigned short, length, cmd_hid_desc.bLength);
			memcpy(req->buf, &cmd_hid_desc, length);
			req->length = length;
			is_stall = 0;
			break;
		case HID_DT_REPORT:
			GADGET_CMD_LOG(LOG_DBG, "USB_REQ_GET_DESCRIPTOR: REPORT\n");
			length = min_t(unsigned short, length,
				       CMD_REPORT_DESC_LENGTH);
			memcpy(req->buf, CMDReportDesc, length);
			req->length = length;
			is_stall = 0;
			break;
		default:
			GADGET_CMD_LOG(LOG_DBG, "Unknown descriptor request 0x%x\n", value >> 8);
			break;
		}
		break;
	default:
		GADGET_CMD_LOG(LOG_DBG, "Unknown request 0x%x\n", ctrl->bRequest);
		break;
	}

	if (is_stall) {
		status = -EOPNOTSUPP;
	} else {
		req->zero = 0;
		if (status >= 0) {
			status = usb_ep_queue(cdev->gadget->ep0, req, GFP_ATOMIC);
			if (status < 0) {
				GADGET_CMD_LOG(LOG_DBG, "usb_ep_queue error %d\n", value);
			}
		}
	}

	return status;
}

static void mbridge_cmd_disconnect(struct usb_function *f)
{
	struct mbridge_cmd_interface *cmd_interface = to_mbridge_cmd_interface(f);

	mgbuf_set_usbconnected(&cmd_interface->mgbuf, 0);

	usbCableConnectedState = false;
}

static void mbridge_cmd_quiescent(struct usb_function *f)
{
	struct mbridge_cmd_interface *cmd_interface = to_mbridge_cmd_interface(f);

	mgbuf_make_quiescent(&cmd_interface->mgbuf);
}

//----------------------------------------------------------------------------

static struct mbridge_cmd_interface s_cmd_interface = {
	.interface = {
		.name = "cmd",
		.fs_descriptors = fs_cmd_descs,
		.hs_descriptors = hs_cmd_descs,
		.bind = mbridge_cmd_bind,
		.unbind = mbridge_cmd_unbind,
		.set_alt = mbridge_cmd_reset,
		.disable = mbridge_cmd_disable,
		.setup = mbridge_cmd_setup,
	},
};

static int device_cmd_open(struct inode *inode, struct file *filp)
{
	struct mgbuf *mgbuf = &s_cmd_interface.mgbuf;

	// queuing usb_request to ep if connected and not queued.
	// otherwise, do it when connect
	if(mgbuf_get_usbconnected(mgbuf) &&
	   !test_and_set_bit(CMD_EP_QUEUED, &s_cmd_interface.flags) ){
		if(mgbuf_pre_queue(mgbuf, 1, 0)) {
			GADGET_CMD_LOG(LOG_ERR, "failed queuing usb request\n");
			clear_bit(CMD_EP_QUEUED, &s_cmd_interface.flags);
		}
	}

	// mgbuf: set mgbuf* to file->private_data
	filp->private_data = mgbuf;

	return mgbuf_open(mgbuf, inode, filp);
}

static int device_cmd_release(struct inode *inode, struct file *filp)
{
	int ret;
	struct mgbuf *mgbuf = filp->private_data;
	struct mbridge_cmd_interface *cmd_interface =
		container_of(mgbuf, struct mbridge_cmd_interface, mgbuf);

	ret = mgbuf_release(mgbuf, inode, filp);
	clear_bit(CMD_EP_QUEUED, &cmd_interface->flags);
	return ret;
}

static ssize_t device_cmd_read(struct file *filp, char *buff, size_t count, loff_t *offp)
{
	ssize_t ret = 0;
	struct mgbuf *mgbuf = filp->private_data;

	if (mgbuf_get_usbconnected(mgbuf)) {
		ret = mgbuf_read(mgbuf, filp, buff, count, offp);
	}

	return ret;
}

static ssize_t device_cmd_write(struct file *filp, const char *pPayload, size_t szPayload, loff_t *off)
{
	ssize_t ret = -ENODEV;
	struct mgbuf *mgbuf = filp->private_data;

	if (mgbuf_get_usbconnected(mgbuf)) {
		ret = mgbuf_write(mgbuf, filp, pPayload, szPayload, off);
	}

	return ret;
}

static long device_cmd_ioctl_set_ctrl_report_data(unsigned long arg)
{
	long Result;
	uint8_t user_report_data[2 + sizeof(struct mbridge_ctrlcmd_out_packet)];
	struct mbridge_ctrlcmd_report *report_data;
	unsigned long flags;

	if ((void*)arg == NULL) {
		return -EINVAL;
	}

	// arg: [0] R-ID, [1] PAYLOAD SIZE, [2-] PAYLOAD
	Result = copy_from_user(user_report_data, (void *)arg, sizeof(user_report_data));
	if(Result == 0) {
		spin_lock_irqsave(&s_ctrlcmd_report_list_lock, flags);

		report_data = &(s_ctrlcmd_report_list[user_report_data[0]]);

		report_data->packet_length = min((unsigned)user_report_data[1], sizeof(report_data->packet));
		memcpy(&(report_data->packet), &(user_report_data[2]), report_data->packet_length);

		spin_unlock_irqrestore(&s_ctrlcmd_report_list_lock, flags);
	}

	return Result;
}
static long device_cmd_ioctl_get_ctrl_report_data(unsigned long arg)
{
	long Result;
	uint8_t user_report_data[2 + sizeof(struct mbridge_ctrlcmd_out_packet)];
	struct mbridge_ctrlcmd_report *report_data;
	unsigned long flags;

	if ((void*)arg == NULL) {
		return -EINVAL;
	}

	// arg: [0] R-ID, [1] PAYLOAD CAPACITY, [2-] PAYLOAD
	Result = copy_from_user(user_report_data, (void *)arg, sizeof(user_report_data));
	if(Result == 0) {
		spin_lock_irqsave(&s_ctrlcmd_report_list_lock, flags);

		report_data = &(s_ctrlcmd_report_list[user_report_data[0]]);

		user_report_data[1] = min3((unsigned)user_report_data[1], (unsigned)report_data->packet_length, sizeof(report_data->packet));
		memcpy(&(user_report_data[2]), &(report_data->packet), user_report_data[1]);

		spin_unlock_irqrestore(&s_ctrlcmd_report_list_lock, flags);

		Result = copy_to_user((void *)arg, user_report_data, 2 + user_report_data[1]);
	}

	return Result;
}

static long device_cmd_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int Result = 0;
	struct mbridge_ctrlcmd_report *report_data;
	struct mbridge_cmd_status user_ctrlcmd_result;
	unsigned long flags;

	switch (cmd) {
	case HOST_IOCTL_SET_CTRL_REPORT_DATA:
		Result = device_cmd_ioctl_set_ctrl_report_data(arg);
		break;

	case HOST_IOCTL_GET_CTRL_REPORT_DATA:
		Result = device_cmd_ioctl_get_ctrl_report_data(arg);
		break;

	case HOST_IOCTL_GET_USB_CONNECTED_STATE:
		Result = copy_to_user((uint64_t *)arg, &usbCableConnectedState,
				      sizeof(usbCableConnectedState));
		break;

	case HOST_IOCTL_GET_CTRL_COMMAND_REQ:
		if((void*)arg == NULL){
			Result = -EINVAL;
			break;
		}

		// arg: [0-63] COMMAND_REQ. include R-ID and COMMAND, PAYLOAD.
		if (s_ctrlcmd_set_report_request_is_receipt) {
			Result = copy_to_user((void *)arg,
					      &s_ctrlcmd_set_report_request,
					      sizeof(s_ctrlcmd_set_report_request));
			if (Result == 0) {
				s_ctrlcmd_set_report_request_is_receipt = 0;
			} else {
				Result = -ENOMEM;
			}
		} else {
			Result = -EAGAIN;
		}
		break;

	case HOST_IOCTL_SET_CTRL_COMMAND_RESULT:
		if((void*)arg == NULL){
			Result = -EINVAL;
			break;
		}

		Result = copy_from_user(&user_ctrlcmd_result, (void *)arg, sizeof(user_ctrlcmd_result));
		if (Result == 0) {
			spin_lock_irqsave(&s_ctrlcmd_report_list_lock, flags);

			report_data = &(s_ctrlcmd_report_list[MBRIDGE_R_ID_GET_COMMAND_STATUS]);
			if((report_data->packet.header.command_status == MBRIDGE_CTRL_STATUS_PROCESS) &&
			   (report_data->packet.header.r_id == user_ctrlcmd_result.r_id) ){
				report_data->packet.header.command_status = user_ctrlcmd_result.command_status;
			} else {
				Result = -EINVAL;
			}

			spin_unlock_irqrestore(&s_ctrlcmd_report_list_lock, flags);
		}
		break;

	default:
		Result = -EINVAL;
		break;
	}

	return Result;
}

static struct file_operations cmd_fops = {
	.read = device_cmd_read,
	.write = device_cmd_write,
	.unlocked_ioctl = device_cmd_ioctl,
	.open = device_cmd_open,
	.release = device_cmd_release,
};

int mbridge_cmd_init(struct usb_composite_dev *cdev, struct usb_function **interface,
		     mbridge_usb_disconnect *disconn_cb, mbridge_usb_quiescent *quiescent_cb)
{
	int ret;

	GADGET_CMD_LOG(LOG_DBG, "\n");

	cmd_intf.bInterfaceNumber =
	    (gbIsDfuMode) ? (MORPHEUS_DFU_COMMAND_INTERFACE) : (MORPHEUS_COMMAND_INTERFACE);

	memset(&s_ctrlcmd_set_report_request, 0, sizeof(s_ctrlcmd_set_report_request));
	s_ctrlcmd_set_report_request_is_receipt = 0;

	ret = usb_string_id(cdev);
	if (ret < 0)
		return ret;

	if (gbIsDfuMode)
		strings_dev_dfu[STRING_DFU_COMMAND_IDX].id = ret;
	else
		strings_dev[STRING_COMMAND_IDX].id = ret;

	cmd_intf.iInterface = ret;

	/* register devnode */
	s_cmd_interface.class = class_create(THIS_MODULE, "pu_cmd");
	if (IS_ERR(s_cmd_interface.class)) {
		dev_err(&cdev->gadget->dev, "failed to create class\n");
		ret = PTR_ERR(s_cmd_interface.class);
		goto rewind0;
	}
	ret = register_chrdev(0, s_cmd_interface.class->name, &cmd_fops);
	if (ret < 0) {
		dev_info(&cdev->gadget->dev, "failed to register chrdev\n");
		goto rewind1;
	}
	s_cmd_interface.major = ret;
	s_cmd_interface.nodedev = device_create(s_cmd_interface.class, NULL,
						MKDEV(ret, 0),
						&s_cmd_interface,
						s_cmd_interface.class->name);
	if (IS_ERR(s_cmd_interface.nodedev)) {
		dev_err(&cdev->gadget->dev, "failed to create class\n");
		ret = PTR_ERR(s_cmd_interface.nodedev);
		goto rewind2;
	}

	clear_bit(CMD_EP_ENABLED, &s_cmd_interface.flags);
	clear_bit(CMD_EP_QUEUED, &s_cmd_interface.flags);

	*interface = &s_cmd_interface.interface;
	*disconn_cb = mbridge_cmd_disconnect;
	if (quiescent_cb)
		*quiescent_cb = mbridge_cmd_quiescent;
	return 0;
rewind2:
	unregister_chrdev(s_cmd_interface.major, s_cmd_interface.class->name);
	s_cmd_interface.major = 0;
rewind1:
	class_destroy(s_cmd_interface.class);
	s_cmd_interface.class = NULL;
rewind0:
	return ret;
}

void mbridge_cmd_destroy(struct usb_function *interface)
{
	if (s_cmd_interface.nodedev) {
		device_destroy(s_cmd_interface.class,
			       MKDEV(s_cmd_interface.major,0));
		s_cmd_interface.nodedev = NULL;
	}
	if (s_cmd_interface.major) {
		unregister_chrdev(s_cmd_interface.major,
				  s_cmd_interface.class->name);
		s_cmd_interface.major = 0;
	}
	if (s_cmd_interface.class) {
		class_destroy(s_cmd_interface.class);
		s_cmd_interface.class = NULL;
	}
}

void __init mbridge_cmd_module_init(void)
{
	int i;

	spin_lock_init(&s_ctrlcmd_report_list_lock);
	memset(s_ctrlcmd_report_list, 0, sizeof(s_ctrlcmd_report_list));
	for(i = 0; i < MBRIDGE_CTRLCMD_REPORT_NUM; i++) {
		s_ctrlcmd_report_list[i].packet.header.r_id = i;
		s_ctrlcmd_report_list[i].packet.header.command_status = MBRIDGE_CTRL_STATUS_ERROR;
		s_ctrlcmd_report_list[i].packet_length = sizeof(s_ctrlcmd_report_list[i].packet.header);
	}
}
