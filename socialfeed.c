/*
 * Copyright (C) 2016 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <asm/uaccess.h>
#include <linux/sched.h>
#include <linux/fs.h>
#include <linux/cdev.h>

#include "bridge.h"
#include "socialfeed.h"
#include "socialfeed_ioctl.h"

#define SOCIALFEED_ENDPOINT_SINK_NUM_REQUESTS 16
#define SOCIALFEED_ENDPOINT_SINK_REQUEST_SIZE 1024
#define SOCIALFEED_MBUF_NUM	1024
#define SOCIALFEED_MBUF_SIZE	SOCIALFEED_ENDPOINT_SINK_REQUEST_SIZE

#define IS_ZLP(x)	((x) == 0 || (x) == -1)
#define IS_FLP(x)	((x) == SOCIALFEED_ENDPOINT_SINK_REQUEST_SIZE || \
			 (x) == -SOCIALFEED_ENDPOINT_SINK_REQUEST_SIZE)
#define IS_SLP(x)	((x) > 0 && (x) != SOCIALFEED_ENDPOINT_SINK_REQUEST_SIZE)

#define SOCIALFEED_MAX_PACKETS		256

static struct usb_interface_descriptor socialfeed_alt0_intf = {
	.bLength = sizeof(socialfeed_alt0_intf),
	.bDescriptorType = USB_DT_INTERFACE,
	.bNumEndpoints = 0,
	.bAlternateSetting = 0,
	.bInterfaceClass = USB_CLASS_VENDOR_SPEC,
	.bInterfaceNumber = MORPHEUS_SOCIALFEED_INTERFACE,
};

static struct usb_interface_descriptor socialfeed_alt1_intf = { // alt1
	.bLength = sizeof(socialfeed_alt1_intf),
	.bDescriptorType = USB_DT_INTERFACE,
	.bNumEndpoints = 1,
	.bAlternateSetting = 1,
	.bInterfaceClass = USB_CLASS_VENDOR_SPEC,
	.bInterfaceNumber = MORPHEUS_SOCIALFEED_INTERFACE, };

static struct usb_interface_descriptor socialfeed_alt2_intf = { // alt2
	.bLength = sizeof(socialfeed_alt2_intf),
	.bDescriptorType = USB_DT_INTERFACE,
	.bNumEndpoints = 1,
	.bAlternateSetting = 2,
	.bInterfaceClass = USB_CLASS_VENDOR_SPEC,
	.bInterfaceNumber = MORPHEUS_SOCIALFEED_INTERFACE, };

static struct usb_interface_descriptor socialfeed_alt3_intf = { // alt3
	.bLength = sizeof(socialfeed_alt3_intf),
	.bDescriptorType = USB_DT_INTERFACE,
	.bNumEndpoints = 1,
	.bAlternateSetting = 3,
	.bInterfaceClass = USB_CLASS_VENDOR_SPEC,
	.bInterfaceNumber = MORPHEUS_SOCIALFEED_INTERFACE, };

static struct usb_endpoint_descriptor fs_socialfeed_sink_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_SOCIALFEED_ENDPOINT_SINK | USB_DIR_OUT,
	/* We don't support isoc at full-speed, so just make valid descriptors. */
	.bmAttributes = USB_ENDPOINT_XFER_ISOC | USB_ENDPOINT_SYNC_ASYNC,
	.wMaxPacketSize = cpu_to_le16(1023),
	.bInterval = 1,
};

static struct usb_endpoint_descriptor hs_socialfeed_sink_desc = { // interval3
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_SOCIALFEED_ENDPOINT_SINK | USB_DIR_OUT,
	.bmAttributes = USB_ENDPOINT_XFER_ISOC | USB_ENDPOINT_SYNC_ASYNC,
	.wMaxPacketSize = cpu_to_le16(SOCIALFEED_ENDPOINT_SINK_REQUEST_SIZE),
	.bInterval = 3, /* 500us */
			/* 1 transaction * 2 times per millisecond * 1024 bytes = 2MB/s */
};

static struct usb_endpoint_descriptor hs_socialfeed_sink_desc2 = { // intervla2
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_SOCIALFEED_ENDPOINT_SINK | USB_DIR_OUT,
	.bmAttributes = USB_ENDPOINT_XFER_ISOC | USB_ENDPOINT_SYNC_ASYNC,
	.wMaxPacketSize = cpu_to_le16(SOCIALFEED_ENDPOINT_SINK_REQUEST_SIZE),
	.bInterval = 2, /* 250us */
};

static struct usb_endpoint_descriptor hs_socialfeed_sink_desc1 = { // intervla1
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_SOCIALFEED_ENDPOINT_SINK | USB_DIR_OUT,
	.bmAttributes = USB_ENDPOINT_XFER_ISOC | USB_ENDPOINT_SYNC_ASYNC,
	.wMaxPacketSize = cpu_to_le16(SOCIALFEED_ENDPOINT_SINK_REQUEST_SIZE),
	.bInterval = 1, /* 125us */
};

static struct usb_descriptor_header *fs_socialfeed_descs[] = {
	(struct usb_descriptor_header *)&socialfeed_alt0_intf,
	(struct usb_descriptor_header *)&socialfeed_alt1_intf,
	(struct usb_descriptor_header *)&fs_socialfeed_sink_desc,
	NULL,
};

static struct usb_descriptor_header *hs_socialfeed_descs[] = {
	(struct usb_descriptor_header *)&socialfeed_alt0_intf,
	(struct usb_descriptor_header *)&socialfeed_alt1_intf, // alt1 interval3
	(struct usb_descriptor_header *)&hs_socialfeed_sink_desc,
	(struct usb_descriptor_header *)&socialfeed_alt2_intf, // alt2 interval2
	(struct usb_descriptor_header *)&hs_socialfeed_sink_desc2,
	(struct usb_descriptor_header *)&socialfeed_alt3_intf, // alt3 interval1
	(struct usb_descriptor_header *)&hs_socialfeed_sink_desc1,
	NULL,
};

struct mbridge_socialfeed_mbuf {
	atomic_t refcnt;

	spinlock_t lock;
	wait_queue_head_t rx_wait;
	int rx_cond;
	int cancel;
	int overrun;
	int unplug;

	int *actual;
	unsigned char *buff;
	int dp;		/* Device OUT requested */
	int cp;		/* Device OUT completed */
	int wp;		/* Write pointer        */
	int rp;		/* Read pointer         */
	int up;		/* User pointer         */
	int quiescent;
};

struct mbridge_socialfeed_dev
{
	struct usb_function funcs;

	struct usb_ep *sink_ep;

	int intf;
	int alt;

	dev_t devt;
	struct class *class;
	struct device *nodedev;
	struct cdev cdev;
	struct mbridge_socialfeed_mbuf mbuf;
};

static struct mbridge_socialfeed_dev g_socialfeed_dev;
static struct usb_request *g_usb_req[SOCIALFEED_ENDPOINT_SINK_NUM_REQUESTS];

//----------------------------------------------------------------------------
static int _mbuf_alloc(struct mbridge_socialfeed_mbuf *mbuf)
{
	pr_info("socialfeed: %s\n", __func__);
	mbuf->buff = kzalloc(SOCIALFEED_MBUF_SIZE * SOCIALFEED_MBUF_NUM,
			     (GFP_KERNEL | __GFP_DMA));
	if (mbuf->buff == NULL) {
		return -ENOMEM;
	}
	mbuf->actual = kzalloc(sizeof(int) * SOCIALFEED_MBUF_NUM, GFP_KERNEL);
	if (mbuf->actual == NULL) {
		kfree(mbuf->buff);
		mbuf->buff = NULL;
		return -ENOMEM;
	}
	spin_lock_init(&mbuf->lock);
	init_waitqueue_head(&mbuf->rx_wait);
	return 0;
}

static void _mbuf_free(struct mbridge_socialfeed_mbuf *mbuf)
{
	pr_info("socialfeed: %s\n", __func__);
	if (mbuf->actual)
		kfree(mbuf->actual);
	if (mbuf->buff)
		kfree(mbuf->buff);
}

static long _mbuf_wait_event(struct mbridge_socialfeed_mbuf *mbuf, long t)
{
	spin_lock(&mbuf->lock);
	if (mbuf->rx_cond == 0) {
		spin_unlock(&mbuf->lock);
		t = wait_event_interruptible_timeout(mbuf->rx_wait, mbuf->rx_cond, t);
		if (t == 0) {
			return -ETIMEDOUT;
		} else if (t < 0) {
			return t;
		}
		spin_lock(&mbuf->lock);
	}
	mbuf->rx_cond = 0;

	if (mbuf->cancel) {
		mbuf->cancel = 0;
		t = -ECANCELED;
	}
	if (mbuf->overrun) {
		mbuf->overrun = 0;
		t = -EOVERFLOW;
	}
	if (mbuf->unplug) {
		mbuf->unplug = 0;
		t = -ECONNRESET;
	}
	spin_unlock(&mbuf->lock);
	return t;
}

static void _mbuf_wake_up(struct mbridge_socialfeed_mbuf *mbuf,
			  int overrun, int cancel, int unplug)
{
	unsigned long spinirq;

	spin_lock_irqsave(&mbuf->lock, spinirq);
	if (overrun)
		mbuf->overrun = 1;
	if (cancel)
		mbuf->cancel = 1;
	if (unplug)
		mbuf->unplug = 1;
	mbuf->rx_cond = 1;
	wake_up_interruptible(&mbuf->rx_wait);
	spin_unlock_irqrestore(&mbuf->lock, spinirq);
}

static inline unsigned char *_mbuf_ptr(unsigned char *buff, int idx)
{
	return &buff[SOCIALFEED_MBUF_SIZE * idx];
}

static inline int _mbuf_add(int idx)
{
	return ((idx + 1) % SOCIALFEED_MBUF_NUM);
}

static inline int _mbuf_del(int idx)
{
	return ((idx == 0) ? SOCIALFEED_MBUF_NUM - 1 : idx - 1);
}

static inline int _mbuf_cmp(int idx_a, int idx_b)
{
	if (idx_a > idx_b)
		return (idx_b + SOCIALFEED_MBUF_NUM) - idx_a;
	return (idx_b - idx_a);
}

//----------------------------------------------------------------------------
static int device_socialfeed_open(struct inode *inode, struct file *filp)
{
	struct mbridge_socialfeed_dev *dev =
		container_of(inode->i_cdev, struct mbridge_socialfeed_dev, cdev);
	filp->private_data = &dev->mbuf;
	atomic_inc(&dev->mbuf.refcnt);
	return 0;
}

static int device_socialfeed_release(struct inode *inode, struct file *filp)
{
	struct mbridge_socialfeed_mbuf *mbuf;

	mbuf = (struct mbridge_socialfeed_mbuf *)filp->private_data;
	if (!atomic_dec_return(&mbuf->refcnt))
		_mbuf_free(mbuf);
	filp->private_data = NULL;
	return 0;
}

static int device_socialfeed_mmap(struct file *filp, struct vm_area_struct *vma)
{
	struct page *page;
	struct mbridge_socialfeed_mbuf *mbuf;

	mbuf = (struct mbridge_socialfeed_mbuf *)filp->private_data;
	page = virt_to_page(mbuf->buff + vma->vm_pgoff);
	if (remap_pfn_range(vma, vma->vm_start, page_to_pfn(page),
			    vma->vm_end - vma->vm_start,
			    PAGE_READONLY)) {
		pr_err("%s: map failed\n", __func__);
		return -EAGAIN;
	}
	return 0;
}

static long device_socialfeed_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int retval = -EINVAL;
	struct mbridge_socialfeed_mbuf *mbuf =
		(struct mbridge_socialfeed_mbuf *)filp->private_data;
	if (mbuf == NULL) {
		pr_err("%s : closed file descriptor.\n", __func__);
		return -EBADFD;
	}

	switch (cmd) {
	case SOCIALFEED_IOCTL_GET_MBUF_INFO:
	{
		struct mbridge_socialfeed_mbuf_info inf;

		inf.buff_size = SOCIALFEED_MBUF_SIZE * SOCIALFEED_MBUF_NUM;
		inf.block_size = SOCIALFEED_MBUF_SIZE;
		retval = copy_to_user((void *)arg, &inf, sizeof(inf));
	}
	break;

	case SOCIALFEED_IOCTL_GET_MBUF_ACTUAL:
	{
		struct mbridge_socialfeed_mbuf_actual req;
		long t;
		int missed;

		retval = copy_from_user(&req, (void *)arg, sizeof(req));
		if (retval) {
			return retval;
		}
		t = msecs_to_jiffies(req.wait_msec);

sleep_again:
		mbuf->up = mbuf->rp;
		t = _mbuf_wait_event(mbuf, t);
		if (t == -EOVERFLOW || t == -ECONNRESET) {
			req.rp = mbuf->up = mbuf->rp = mbuf->wp;
			req.actual = 0;
			retval = copy_to_user((void *)arg, &req, sizeof(req));
			if (retval) {
//				return retval;
			}
			return t;
		} else if (t < 0) {
			return t;
		}

		missed = 0;
		if (_mbuf_cmp(mbuf->rp, mbuf->wp) == 0) {
			goto sleep_again;
		}
		for ( ;IS_ZLP(mbuf->actual[mbuf->rp]); ) {
			if (mbuf->actual[mbuf->rp] < 0) {
				missed ++;
			}
			mbuf->rp = _mbuf_add(mbuf->rp);
			if (_mbuf_cmp(mbuf->rp, mbuf->wp) == 0) {
				if (missed)
					pr_info("socialfeed: missed in zlp (%d)\n", missed);
				req.rp = mbuf->up = mbuf->rp;
				req.actual = 0;
				retval = copy_to_user((void *)arg, &req, sizeof(req));
				return retval;
			}
		}
		if (missed)
			pr_info("socialfeed: missed in zlp (%d)\n", missed);
		missed = 0;
		req.rp = mbuf->up = mbuf->rp;
		req.actual = 0;
		for ( ;!IS_ZLP(mbuf->actual[mbuf->rp]); ) {
			if (mbuf->actual[mbuf->rp] < 0) {
				missed ++;
			} else {
				req.actual += mbuf->actual[mbuf->rp];
			}
			mbuf->rp = _mbuf_add(mbuf->rp);
			if (_mbuf_cmp(mbuf->rp, mbuf->wp) == 0) {
				break;
			}
			if (req.actual % SOCIALFEED_ENDPOINT_SINK_REQUEST_SIZE) {
				mbuf->rx_cond = 1;
				break;
			}
			if (missed == 0 && mbuf->rp == 0) {
				mbuf->rx_cond = 1;
				break;
			}
		}
		if (missed) {
			pr_info("socialfeed: missed (%d)\n", missed);
			req.rp = mbuf->up = mbuf->rp;
			req.actual = 0;
			retval = copy_to_user((void *)arg, &req, sizeof(req));
			if (retval) {
//				return retval;
			}
			return -EILSEQ;
		}
		retval = copy_to_user((void *)arg, &req, sizeof(req));
	}
	break;

	case SOCIALFEED_IOCTL_GET_MBUF_ACTUAL_CANCEL:
		_mbuf_wake_up(mbuf, 0, 1, 0);
		retval = 0;
		break;

	default:
		break;
	}
	return retval;
}

//----------------------------------------------------------------------------

static void mbridge_socialfeed_complete(struct usb_ep *ep, struct usb_request *req)
{
	struct mbridge_socialfeed_dev *dev = ep->driver_data;
	struct mbridge_socialfeed_mbuf *mbuf = &dev->mbuf;
	int ret, curr_actual, prev_actual;

	ASSERT(ep == dev->sink_ep);
	/* treat as dequeued if quiescent */
	if (mbuf->quiescent)
		req->status = -ECONNRESET;

	switch (req->status) {
	default:
	case 0:
		/* missed isoc */
		prev_actual = mbuf->actual[_mbuf_del(mbuf->cp)];
		if (req->status == -ETIMEDOUT) {
			if (IS_FLP(prev_actual)) {
				curr_actual = -SOCIALFEED_ENDPOINT_SINK_REQUEST_SIZE;
			} else {
				curr_actual = -1;
			}
		} else {
			curr_actual = (int)req->actual;
		}
		mbuf->actual[mbuf->cp] = curr_actual;
		mbuf->cp = _mbuf_add(mbuf->cp);

		/* short packets */
		if (IS_SLP(curr_actual)) {
			mbuf->wp = mbuf->cp;
			_mbuf_wake_up(mbuf, 0, 0, 0);

		/* zlp or vsync */
		} else if ((curr_actual > 0 && IS_ZLP(prev_actual)) ||
			   (curr_actual == 0 && IS_FLP(prev_actual))) {
			mbuf->wp = _mbuf_del(mbuf->cp);
			_mbuf_wake_up(mbuf, 0, 0, 0);

		/* max packets */
		} else if ((_mbuf_cmp(mbuf->wp, mbuf->cp) % SOCIALFEED_MAX_PACKETS) == 0) {
			mbuf->wp = mbuf->cp;
			_mbuf_wake_up(mbuf, 0, 0, 0);
		}

		/* overrun */
		if (mbuf->rp == mbuf->dp) {
			_mbuf_wake_up(mbuf, 1, 0, 0);
		}

		req->buf = _mbuf_ptr(mbuf->buff, mbuf->dp);
		req->context = (void *)mbuf->dp;
		mbuf->dp = _mbuf_add(mbuf->dp);
		ret = usb_ep_queue(ep, req, GFP_ATOMIC);
		if (ret == 0) {
			return;
		}
	/* Intentional fallthrough. */
	/* NOTE:  since this driver doesn't maintain an explicit record
	* of requests it submitted (just maintains qlen count), we
	* rely on the hardware driver to clean up on disconnect or
	* endpoint disable.
	*/
	case -ECONNRESET:   /* request dequeued */
	case -ESHUTDOWN: /* disconnect from host */
		prev_actual = mbuf->actual[_mbuf_del(mbuf->cp)];
		if (IS_FLP(prev_actual)) {
			mbuf->wp = mbuf->cp;
		}
		for(req->context = (void *)_mbuf_add((int)req->context);
			(int)req->context != mbuf->cp; mbuf->cp = _mbuf_add(mbuf->cp)) {
			mbuf->actual[mbuf->cp] = 0;
		}
 		if (prev_actual == SOCIALFEED_ENDPOINT_SINK_REQUEST_SIZE) {
			_mbuf_wake_up(mbuf, 0, 0, 0);
		}
		break;
	}
}

//----------------------------------------------------------------------------

static int _socialfeed_config_ep_by_speed(struct usb_gadget *g, struct usb_function *f,
					  struct usb_ep *_ep, unsigned alt, unsigned intf)
{
	struct usb_composite_dev *cdev = get_gadget_data(g);
	struct usb_endpoint_descriptor *chosen_desc = NULL;
	struct usb_descriptor_header **speed_desc = NULL;
	struct usb_interface_descriptor *if_desc = NULL;

	struct usb_ss_ep_comp_descriptor *comp_desc = NULL;
	int i, want_comp_desc = 0;

	if (!g || !f || !_ep)
		return -EIO;

	/* select desired speed */
	switch (g->speed) {
	case USB_SPEED_SUPER:
		if (gadget_is_superspeed(g)) {
			speed_desc = f->ss_descriptors;
			want_comp_desc = 1;
			break;
		}
	/* else: Fall trough */
	case USB_SPEED_HIGH:
		if (gadget_is_dualspeed(g)) {
			speed_desc = f->hs_descriptors;
			break;
		}
	/* else: fall through */
	default:
		speed_desc = f->fs_descriptors;
	}

	for (; *speed_desc; speed_desc++) {
		if (((struct usb_interface_descriptor *)*speed_desc)->bDescriptorType !=
		    USB_DT_INTERFACE)
			continue;

		if_desc = (struct usb_interface_descriptor *)*speed_desc;
		if ((if_desc->bInterfaceNumber == intf) && (if_desc->bAlternateSetting == alt)) {
			if (if_desc->bNumEndpoints == 0)
				return -EOPNOTSUPP;

			speed_desc++;
			for (i = 0; i < if_desc->bNumEndpoints; i++, speed_desc++) {
				if (((struct usb_interface_descriptor *)*speed_desc)
					->bDescriptorType != USB_DT_ENDPOINT)
					break;

				chosen_desc = (struct usb_endpoint_descriptor *)*speed_desc;
				if (chosen_desc->bEndpointAddress == _ep->address)
					goto ep_found;
			}
		}
	}
	return -EIO;

ep_found:
	/* commit results */
	_ep->maxpacket = usb_endpoint_maxp(chosen_desc);
	_ep->desc = chosen_desc;
	_ep->comp_desc = NULL;
	_ep->maxburst = 0;
	_ep->mult = 0;
	if (!want_comp_desc)
		return 0;

	/*
	 * Companion descriptor should follow EP descriptor
	 * USB 3.0 spec, #9.6.7
	 */
	comp_desc = (struct usb_ss_ep_comp_descriptor *)*(++speed_desc);
	if (!comp_desc || (comp_desc->bDescriptorType != USB_DT_SS_ENDPOINT_COMP))
		return -EIO;
	_ep->comp_desc = comp_desc;
	if (g->speed == USB_SPEED_SUPER) {
		switch (usb_endpoint_type(_ep->desc)) {
		case USB_ENDPOINT_XFER_ISOC:
			/* mult: bits 1:0 of bmAttributes */
			_ep->mult = comp_desc->bmAttributes & 0x3;
		case USB_ENDPOINT_XFER_BULK:
		case USB_ENDPOINT_XFER_INT:
			_ep->maxburst = comp_desc->bMaxBurst + 1;
			break;
		default:
			if (comp_desc->bMaxBurst != 0)
				ERROR(cdev, "ep0 bMaxBurst must be 0\n");
			_ep->maxburst = 1;
			break;
		}
	}
	return 0;
}

static void _socialfeed_disable(struct mbridge_socialfeed_dev *dev)
{
	int i;

	if (dev->sink_ep->driver_data) {
		usb_ep_disable(dev->sink_ep);
		dev->sink_ep->driver_data = NULL;
		for (i = 0; i < SOCIALFEED_ENDPOINT_SINK_NUM_REQUESTS; i++) {
			if (g_usb_req[i] != NULL) {
				usb_ep_free_request(dev->sink_ep, g_usb_req[i]);
				g_usb_req[i] = NULL;
			}
		}
	}
}

static int _socialfeed_enable(struct mbridge_socialfeed_dev *dev)
{
	struct mbridge_socialfeed_mbuf *mbuf = &dev->mbuf;
	int ret, i;

	ret = usb_ep_enable(dev->sink_ep);
	if (ret) {
		dev_err(dev->nodedev, "error starting social endpoint\n");
		return ret;
	}

	mbuf->quiescent = 0;

	/* Data for the completion callback. */
	dev->sink_ep->driver_data = dev;

	/* Enqueue descriptors for RX. */
	for (i = 0; i < SOCIALFEED_ENDPOINT_SINK_NUM_REQUESTS; i++) {
		g_usb_req[i] = usb_ep_alloc_request(dev->sink_ep, GFP_ATOMIC);
		if (g_usb_req[i] == NULL) {
			dev_err(dev->nodedev, "failed allocation usb request\n");
			_socialfeed_disable(dev);
			return -ENOMEM;
		}

		g_usb_req[i]->buf = _mbuf_ptr(mbuf->buff, mbuf->dp);
		g_usb_req[i]->context = (void *)mbuf->dp;
		mbuf->dp = _mbuf_add(mbuf->dp);
		g_usb_req[i]->length = SOCIALFEED_ENDPOINT_SINK_REQUEST_SIZE;
		g_usb_req[i]->complete = mbridge_socialfeed_complete;
		ret = usb_ep_queue(dev->sink_ep, g_usb_req[i], GFP_ATOMIC);
		if (ret) {
			dev_err(dev->nodedev, "failed queuing usb request\n");
			_socialfeed_disable(dev);
			return ret;
		}
	}

	return 0;
}

//----------------------------------------------------------------------------

static int mbridge_socialfeed_bind(struct usb_configuration *c, struct usb_function *f)
{
	struct mbridge_socialfeed_dev *dev =
	    container_of(f, struct mbridge_socialfeed_dev, funcs);

	ASSERT(c != NULL);
	ASSERT(dev != NULL);

	/* Configure endpoint from the descriptor. */
	dev->sink_ep =
	    usb_ep_autoconfig(c->cdev->gadget, &fs_socialfeed_sink_desc);
	if (dev->sink_ep == NULL) {
		dev_err(dev->nodedev, "can't autoconfigure social endpoint\n");
		return -ENODEV;
	}

	ASSERT((dev->sink_ep->address & USB_ENDPOINT_NUMBER_MASK) ==
	       MORPHEUS_SOCIALFEED_ENDPOINT_SINK);

	/* Need to set this during setup only. */
	dev->sink_ep->driver_data = c->cdev;

	dev_dbg(dev->nodedev, "name=%s address=0x%x\n", dev->sink_ep->name,
		dev->sink_ep->address);
	return 0;
}

static void mbridge_socialfeed_disable(struct usb_function *f)
{
	struct mbridge_socialfeed_dev *dev =
	    container_of(f, struct mbridge_socialfeed_dev, funcs);

	ASSERT(dev != NULL);
	dev_info(dev->nodedev, "disabling interface...\n");

	_socialfeed_disable(dev);
	_mbuf_wake_up(&dev->mbuf, 0, 0, 1);
}

static void mbridge_socialfeed_quiescent(struct usb_function *f)
{
	struct mbridge_socialfeed_dev *dev =
	    container_of(f, struct mbridge_socialfeed_dev, funcs);

	dev->mbuf.quiescent = 1;
}


static int mbridge_socialfeed_set_alt(struct usb_function *f, unsigned intf, unsigned alt)
{
	struct mbridge_socialfeed_dev *dev =
	    container_of(f, struct mbridge_socialfeed_dev, funcs);
	int ret;

	ASSERT(dev != NULL);
	dev_dbg(dev->nodedev, "set_alt intf(%d) alt(%d)\n", intf, alt);

	_socialfeed_disable(dev);

	dev->alt = alt;
	dev->intf = intf;

	/* Enable the endpoint. */
	ret = _socialfeed_config_ep_by_speed(f->config->cdev->gadget, f,
					     dev->sink_ep,
					     dev->alt,
					     dev->intf);
	if (ret == -EOPNOTSUPP)
		return 0;

	if (ret) {
		dev_err(dev->nodedev, "error configuring social endpoint\n");
		return ret;
	}
	dev_dbg(dev->nodedev, "isoc interval(%d)\n", dev->sink_ep->desc->bInterval);

	return _socialfeed_enable(dev);
}

static int mbridge_socialfeed_get_alt(struct usb_function *f, unsigned intf)
{
	struct mbridge_socialfeed_dev *dev =
	    container_of(f, struct mbridge_socialfeed_dev, funcs);

	ASSERT(dev != NULL);
	dev_dbg(dev->nodedev, "get_alt intf(%d) alt(%d)\n", intf, dev->alt);

	return dev->alt;
}

//----------------------------------------------------------------------------

static struct file_operations social_fops = {
	.owner = THIS_MODULE,
	.open = device_socialfeed_open,
	.release = device_socialfeed_release,
	.mmap = device_socialfeed_mmap,
	.unlocked_ioctl = device_socialfeed_ioctl,
};

void mbridge_socialfeed_destroy(struct usb_function *f);

int mbridge_socialfeed_init(struct usb_composite_dev *cdev, struct usb_function **ff,
			    mbridge_usb_quiescent *quiescent_cb)
{
	struct mbridge_socialfeed_dev *dev;
	int ret;

	pr_info("%s: Start(ref:25)\n", __func__);
	ASSERT(cdev != NULL);
	ASSERT(ff != NULL);

	ret = usb_string_id(cdev);
	if (ret < 0)
		return ret;

	strings_dev[STRING_SOCIALFEED_IDX].id = ret;
	socialfeed_alt0_intf.iInterface = ret;
	socialfeed_alt1_intf.iInterface = ret;

	dev = &g_socialfeed_dev;

	if (!atomic_read(&dev->mbuf.refcnt)) {
		ret = _mbuf_alloc(&dev->mbuf);
		if (ret < 0) {
			dev_err(&cdev->gadget->dev, "socialfeed: failed to mbuf alloc\n");
			return ret;
		}
		dev->mbuf.cp = dev->mbuf.dp = _mbuf_add(dev->mbuf.dp);
	}
	atomic_inc(&dev->mbuf.refcnt);

	/* register devnode */
	dev->class = class_create(THIS_MODULE, "socialfeed");
	if (IS_ERR(dev->class)) {
		dev_err(&cdev->gadget->dev, "socialfeed: failed to create class\n");
		ret = PTR_ERR(dev->class);
		goto fail;
	}
	ret = alloc_chrdev_region(&dev->devt, 0, 1, dev->class->name);
	if (ret < 0) {
		dev_err(&cdev->gadget->dev, "socialfeed: failed to register chrdev\n");
		goto fail;
	}
	dev->nodedev = device_create(dev->class, NULL, dev->devt, dev, "pu_social");
	if (IS_ERR(dev->nodedev)) {
		dev_err(&cdev->gadget->dev, "socialfeed: failed to create device\n");
		ret = PTR_ERR(dev->nodedev);
		goto fail;
	}
	cdev_init(&dev->cdev, &social_fops);
	dev->cdev.owner = THIS_MODULE;
	ret = cdev_add(&dev->cdev, dev->devt, 1);
	if (ret < 0) {
		dev_err(&cdev->gadget->dev, "socialfeed: failed to add cdev\n");
		goto fail;
	}


	dev->funcs.name = "socialfeed";
	dev->funcs.fs_descriptors = fs_socialfeed_descs;
	dev->funcs.hs_descriptors = hs_socialfeed_descs;
	dev->funcs.bind = mbridge_socialfeed_bind;
	dev->funcs.set_alt = mbridge_socialfeed_set_alt;
	dev->funcs.disable = mbridge_socialfeed_disable;
	dev->funcs.get_alt = mbridge_socialfeed_get_alt;

	*ff = &dev->funcs;
	*quiescent_cb = mbridge_socialfeed_quiescent;

	return 0;

fail:
	mbridge_socialfeed_destroy(&dev->funcs);
	return ret;
}

void mbridge_socialfeed_destroy(struct usb_function *f)
{
	struct mbridge_socialfeed_dev *dev =
	    container_of(f, struct mbridge_socialfeed_dev, funcs);

	pr_info("%s\n", __func__);
	cdev_del(&dev->cdev);
	if (dev->nodedev)
		device_destroy(dev->class, dev->devt);
	dev->nodedev = NULL;

	if (dev->devt)
		unregister_chrdev_region(dev->devt, 1);
	dev->devt = 0;

	if (dev->class)
		class_destroy(dev->class);
	dev->class = NULL;

	if (!atomic_dec_return(&dev->mbuf.refcnt))
		_mbuf_free(&dev->mbuf);
}
