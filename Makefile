mbridge-y += sensor.o audio3d.o command.o bridge.o socialfeed.o util.o
mbridge-y += chat.o mic.o chatmicctrl.o bulkin.o
mbridge-y += dfu.o bridge_dfu.o
mbridge-y += authentication.o
mbridge-y += mgbuf.o
hmu-y := hmusys.o
obj-m  := mbridge.o
obj-m  += hmu.o

ifeq ($(TARGET_BUILD_VARIANT),user)
ccflags-y += -DCONFIG_MBRIDGE_RELEASE
else
ifeq ($(TARGET_BUILD_VARIANT),factory)
ccflags-y += -DCONFIG_MBRIDGE_FACTORY
else
ccflags-y += -DCONFIG_MBRIDGE_DEBUG
endif
endif

all: morpheus

morpheus:
	$(MAKE) ARCH=arm CROSS_COMPILE="$(SDK_ROOT)/morpheus/prebuilt/toolchain/armv5-marvell-eabi-softfp/bin/arm-marvell-eabi-" -C "$(SDK_ROOT)/linux" M="$$PWD"

clean:
	@echo "Cleaning up..."
	$(RM) -R .*.cmd .tmp_versions Module.symvers modules.order *.mod.c *.o *.ko
