/*
 * Copyright (C) 2017 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <linux/usb.h>
#include <linux/poll.h>
#include <linux/proc_fs.h>
#include <linux/module.h>

#define ENABLE_MBRIDGE_DEBUG

#include "bridge.h"
#include "hostdef.h"
#include "hmusys.h"
#include "sensor.h"
#include "mic.h"
#include "stat.h"
char hmu_name[] = "hmusys";

#define IF_ISO_IN 0x01
#define EP_ISO_IN 0x01
#define IF_ISO_OUT 0x02
#define EP_ISO_OUT 0x02

#define HMUSYS_ISO_URBS				128
#define ISO_PACKETS_PER_DESC	1

#define CREATE_PROC_ENTRY

#if defined(CREATE_PROC_ENTRY)
struct proc_dir_entry* s_ProcEntry = NULL;
#endif

uint64_t s_LastReadTimestamp;
unsigned int s_LastDeviceReadTimestamp;
static bool s_bRegistered = false;
static DEFINE_MUTEX(hmu_mutex);
static long hmu_ioctl( struct file *filp, unsigned int cmd, unsigned long arg);

struct hmu_device;
struct hmu_urb_context
{
	int					urb_index;
	struct hmu_device*	pDevice;
	struct urb*			iso_urb;
};

struct hmu_device {
	bool					is_present;
	atomic_t				is_opened;
	struct usb_device*		udev;
	struct hmu_urb_context	urb_context[HMUSYS_ISO_URBS];
	wait_queue_head_t		read_wait;
	struct usb_interface*	interface;
	bool					iso_packet_received;
	bool					is_running;
	uint32_t				iso_in_maxpacket;
	spinlock_t				lock;
};

static struct usb_device_id hmu_ids[] =
{
	{ USB_DEVICE(0x054c, 0x09ac) },
	{ }
};

struct sensor_data
{
	uint32_t timestamp;
	uint8_t data[12];
};

struct hmu_iso_in_packet
{
	uint8_t mmi_event[16];
	struct sensor_data sensor[2];
	uint8_t misc[48];
	uint8_t mic_data[];
};

static struct usb_driver hmu_driver;

static unsigned long s_timehigh, s_timelow, s_timeaverage, s_timetick;
static uint32_t s_intervals[11];
static uint32_t s_uiSendOK=0;
static uint32_t s_uiSendFailed=0;
static uint32_t s_uiSendFailedNoURB=0;
static uint32_t s_uiTotalReceived=0;
static bool		s_bForwardingEnabled = true;

//------------------------------------------------------------------------------

struct Data
{
	int status;
	char pBuffer[MMI_SENSOR_DATACH_TIME_LENGTH];
};

struct Ring
{
	struct Data* pData;
	size_t szSize;
	unsigned int uHead;
	unsigned int uTail;
} Ring;

struct Ring s_SensorBuffer;
struct Data s_RingBuffer[128];

static inline void ringReset(struct Ring* pRing, struct Data* pData, size_t szBufferSize)
{
	pRing->pData = pData;
	pRing->szSize = szBufferSize;
	pRing->uHead = pRing->uTail = 0;
}

static inline void ringPush(struct Ring *pRing, int status, struct hmu_iso_in_packet* pPacket, uint64_t system_timestamp)
{
	size_t szAvailable = pRing->szSize - (pRing->uHead - pRing->uTail);
	struct Data* pData;

	if (szAvailable <= 0)
		return;

	pData = &pRing->pData[pRing->uHead % pRing->szSize];
	if (status == 0) {
		/* add system_timestamp */
		memcpy(pData->pBuffer, pPacket, sizeof(pData->pBuffer) - sizeof(system_timestamp));
		memcpy(pData->pBuffer + (sizeof(pData->pBuffer) - sizeof(system_timestamp)), &system_timestamp, sizeof(system_timestamp));
	}

	pData->status = status;
	smp_mb();
	pRing->uHead++;
}

static inline int ringPopToUser(struct Ring *pRing, void __user *pBuffer, size_t szBufferSize)
{
	size_t szData = pRing->uHead - pRing->uTail;
	struct Data* pData;
	int status;

	if (szData <= 0)
		return 0;

	if (szBufferSize > MMI_SENSOR_DATACH_TIME_LENGTH)
		szBufferSize = MMI_SENSOR_DATACH_TIME_LENGTH;

	pData = &pRing->pData[pRing->uTail % pRing->szSize];
	status = pData->status;
	if (status != 0)
	{
		pRing->uTail++;
		return status;
	}
	else if (copy_to_user(pBuffer, pData->pBuffer, szBufferSize) == 0)
	{
		pRing->uTail++;
		return (int)szBufferSize;
	}
	return -EFAULT;
}

static uint64_t mbridge_get_time(void)
{
	uint64_t time;
	struct timespec tp;
	do_posix_clock_monotonic_gettime(&tp);
	time = ((uint64_t)tp.tv_sec * 1000000ull) + (tp.tv_nsec / 1000);

	return time;
}

//------------------------------------------------------------------------------

void timing(void)
{
	unsigned long time, delta;
	unsigned int index;

	static unsigned int lasttime=0;

	time = mbridge_get_time();
	delta = time-lasttime;
	lasttime = time;
	if( s_timetick>0 )
	{
		if( delta<s_timelow )
		{
			s_timelow = delta;
		}
		if( delta>s_timehigh )
		{
			s_timehigh = delta;
		}
		s_timeaverage+=delta;
	}
	s_timetick++;
	index = delta/1000;
	if( index>10 )
	{
		index = 10;
	}
	s_intervals[index]++;

}

static int hmu_iso_in_submit(struct urb *urb, gfp_t mem_flags)
{
	struct hmu_urb_context* urb_context;
	int error;

	if (!urb || !urb->context) {
		GADGET_LOG( LOG_DBG, "urb is NULL\n");
		return -ENODEV;
	}

	urb_context = urb->context;

	error = usb_submit_urb( urb, mem_flags );
	if ( error != 0 )
	{
		if( urb_context->pDevice->is_running )
		{
			GADGET_LOG( LOG_DBG, "ERROR: usb_submit_urb %p, pDevice:%p, error:%d\n", urb, urb_context->pDevice, error );
			urb_context->pDevice->is_running = false;
		}
	}
	else
		urb_context->pDevice->is_running = true;

	return error;
}

static int hmu_forward_hid(const void *buf)
{
	int error;

	if( !mbridge_hid_is_ready() )
		return 0;

	error = mbridge_hid_send( buf, SENSOR_ENDPOINT_SOURCE_REQUEST_SIZE );
	if( error == -ENOENT )
	{
		s_uiSendFailedNoURB++;
	}
	else if( error < 0 )
	{
		s_uiSendFailed++;
	}
	else
	{
		s_uiSendOK++;
	}
	s_uiTotalReceived++;
	return error;
}

static int hmu_forward_mic(const void *buf, size_t buflen)
{
	int error = 0;

	if( !mbridge_mic_is_ready() )
		return 0;

	if ( buflen > 0 )
		error = mbridge_mic_send( buf, buflen );

	return error;
}

static void hmu_iso_urb_complete(struct urb *urb)
{
	struct hmu_urb_context* urb_context;
	struct hmu_iso_in_packet* pPacket;
	unsigned int actual_length;
	bool recoverable_error = false;

	if (!urb || !urb->context || !urb->transfer_buffer) {
		return;
	}
	urb_context = urb->context;
	pPacket = (struct hmu_iso_in_packet*)urb->transfer_buffer;
	actual_length = urb->iso_frame_desc[0].actual_length;

	timing();

	if ( urb->iso_frame_desc[0].status == -EILSEQ || urb->iso_frame_desc[0].status == -EPROTO )
		recoverable_error = true;

	if ( recoverable_error == false && ( urb->status != 0 || urb->iso_frame_desc[0].status !=0 ) )
	{
		if ( urb_context->pDevice->is_running )
		{
			urb_context->pDevice->is_running = false;
			GADGET_LOG( LOG_DBG, "[%s] URB ERROR: %d %d\n", hmu_name, urb->status, urb->iso_frame_desc[0].status);
		}
	}

	if ( urb_context->pDevice->is_running == false )
		return;

	spin_lock( &urb_context->pDevice->lock );
	if ( !urb_context->pDevice->is_present )
	{
		spin_unlock( &urb_context->pDevice->lock );
		return;
	}

	/* Setup For URB Defer */
	urb->dev = urb_context->pDevice->udev;

	if ( recoverable_error || actual_length < MMI_SENSOR_DATACH_LENGTH )
	{
		if ( recoverable_error )
			ringPush( &s_SensorBuffer, urb->iso_frame_desc[0].status, NULL, 0 );
		hmu_iso_in_submit( urb, GFP_ATOMIC );
		spin_unlock( &urb_context->pDevice->lock );
		return;
	}

	// Copy system timestamp when the sample is actually received
	s_LastReadTimestamp = mbridge_get_time();
	s_LastDeviceReadTimestamp = pPacket->sensor[1].timestamp;

	// The second sample can sometimes be dropped, so if it's timestamp is less than the first, then use the first
	if( (int32_t)(pPacket->sensor[1].timestamp - pPacket->sensor[0].timestamp) < 0 )
	{
		s_LastDeviceReadTimestamp = pPacket->sensor[0].timestamp;
	}

	if( s_bForwardingEnabled )
		hmu_forward_hid( &pPacket->mmi_event );

	if( s_bForwardingEnabled && actual_length - MMI_SENSOR_DATACH_LENGTH >= 0 )
		hmu_forward_mic( pPacket->mic_data, actual_length - MMI_SENSOR_DATACH_LENGTH );

	ringPush( &s_SensorBuffer, 0, pPacket, s_LastReadTimestamp );

	hmu_iso_in_submit( urb, GFP_ATOMIC );
	spin_unlock( &urb_context->pDevice->lock );
	return;
}

//------------------------------------------------------------------------------
static void hmu_stop_transfer(struct hmu_device *pDevice)
{
	int i;

	GADGET_LOG( LOG_DBG,  "releasing URBs, pDevice:%p\n", pDevice );
	for (i = 0; i < HMUSYS_ISO_URBS; i++)
	{
		struct urb* urb = pDevice->urb_context[i].iso_urb;
		if( urb )
		{
			usb_kill_urb( urb );
			kfree( urb->transfer_buffer );
			usb_free_urb( urb );
			pDevice->urb_context[i].iso_urb = NULL;
		}
	}
}

static int hmu_start_transfer(struct hmu_device *pDevice)
{
	int i, j, error;
	struct usb_device *udev = pDevice->udev;
	struct urb* urb;

	if (pDevice->iso_in_maxpacket == 0 || pDevice->iso_in_maxpacket > ISO_MAX_PACKET_SIZE)
	{
		error = -EINVAL;
		GADGET_LOG( LOG_DBG, "[%s] invalid maximum packet size (%x)\n", hmu_name, pDevice->iso_in_maxpacket);
		goto hmu_start_transfer_free_urbs;
	}

	error = 0;
	for (i = 0; i < HMUSYS_ISO_URBS; i++)
	{
		struct hmu_urb_context* urb_context = &pDevice->urb_context[i];

		urb = usb_alloc_urb(ISO_PACKETS_PER_DESC, GFP_KERNEL);
		if( urb==NULL )
		{
			GADGET_LOG( LOG_DBG, "ERROR Could not allocate URB\n" );
			goto hmu_start_transfer_free_urbs;
		}

		urb_context->iso_urb = urb;
		urb_context->pDevice = pDevice;

		urb->transfer_buffer = kzalloc(ISO_PACKETS_PER_DESC * pDevice->iso_in_maxpacket, GFP_KERNEL);
		if (urb->transfer_buffer == NULL)
		{
			error = -ENOMEM;
			GADGET_LOG( LOG_DBG, "[%s] %s ERROR: URB: %d kzalloc()\n", hmu_name, __FUNCTION__, i);
			goto hmu_start_transfer_free_urbs;
		}

		urb->dev = udev;
		urb->context = urb_context;
		urb->pipe = usb_rcvisocpipe(udev, EP_ISO_IN);
		urb->transfer_flags = URB_ISO_ASAP;
		urb->number_of_packets = ISO_PACKETS_PER_DESC;
		urb->transfer_buffer_length = ISO_PACKETS_PER_DESC * pDevice->iso_in_maxpacket;
		urb->complete = hmu_iso_urb_complete;
		urb->interval = 1;
		for (j = 0; j < ISO_PACKETS_PER_DESC; j++) {
			urb->iso_frame_desc[j].offset = j * pDevice->iso_in_maxpacket;
			urb->iso_frame_desc[j].length = pDevice->iso_in_maxpacket;
		}
	}

	/* ISO Interface Setup - inbound */
	error = usb_set_interface(udev, IF_ISO_IN, 1);
	if (error)
	{
		GADGET_LOG( LOG_DBG, "\n[%s] %s ERROR usb_set_interface(IN), error:%d\n", hmu_name, __FUNCTION__, error );
		goto hmu_start_transfer_free_urbs;
	}

	// Set up for outbound interface
	error = usb_set_interface(udev, IF_ISO_OUT, 1);
	if( error )
	{
		GADGET_LOG( LOG_DBG,  "\n[%s] %s ERROR usb_set_interface(OUT), error:%d\n", hmu_name, __FUNCTION__, error);
	}

	pDevice->is_running = true;

	/* Submit URBs */
	for (i = 0; i < HMUSYS_ISO_URBS; i++)
	{
		urb = pDevice->urb_context[i].iso_urb;

		error = hmu_iso_in_submit( urb, GFP_KERNEL );
		if (error)
		{
			usb_kill_urb( urb );
			goto hmu_start_transfer_free_urbs;
		}
	}

	return 0;


hmu_start_transfer_free_urbs:
	for (i = 0; i < HMUSYS_ISO_URBS; i++)
	{
		urb = pDevice->urb_context[i].iso_urb;
		if( urb )
		{
			usb_kill_urb( urb );
			kfree( urb->transfer_buffer );
			usb_free_urb( urb );
			pDevice->urb_context[i].iso_urb = NULL;
		}
	}
	return error;
}

//------------------------------------------------------------------------------
static long hmu_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	uint64_t* param = (uint64_t*)arg;
	int ret = 0;
	const uint32_t Version = HMUSYS_REVISION;

	switch (cmd) {
	case HMUSYS_CMD_GET_LAST_PACKET_TIMESTAMP:
		if (!param)
			break;
		ret = copy_to_user(param, &s_LastReadTimestamp,
				   sizeof(s_LastReadTimestamp));
		break;
	case HMUSYS_CMD_GET_LAST_DEVICE_TIMESTAMP:
		if (!param)
			break;
		ret = copy_to_user(param, &s_LastDeviceReadTimestamp,
				   sizeof(s_LastDeviceReadTimestamp));
		break;
	case HMUSYS_CMD_GET_VERSION:
		if (!param)
			break;
		ret = copy_to_user(param, &Version, sizeof(Version));
		break;
	case HMUSYS_CMD_PAUSE_SENSOR_FORWARDING:
		GADGET_LOG(LOG_DBG, " Sensor forwarding disabled\n");
		s_bForwardingEnabled = false;
		break;
	case HMUSYS_CMD_RESUME_SENSOR_FORWARDING:
		if (!gbIsDfuMode) {
			GADGET_LOG(LOG_DBG, " Sensor forwarding enabled\n");
			s_bForwardingEnabled = true;
		}
		break;
	case HMUSYS_CMD_GET_MIC_STAT:
		if (param) {
			struct mbridge_mic_stat stat;

			mbridge_mic_get_stat(&stat);
			ret = copy_to_user(param, &stat, sizeof(stat));
		}
		break;
	case HMUSYS_CMD_GET_SENSOR_STAT:
		if (param) {
			struct mbridge_sensor_stat stat;

			mbridge_sensor_get_stat(&stat);
			ret = copy_to_user(param, &stat, sizeof(stat));
		}
		break;
	default:
		break;
	}

	return ret;
}


//------------------------------------------------------------------------------
static void *hmu_usb_get_intfdata(struct inode *inode)
{
	struct usb_interface *interface;
	int subminor = iminor(inode);

	interface = usb_find_interface(&hmu_driver, subminor);
	if (!interface)
	{
		return NULL;
	}

	return usb_get_intfdata(interface);
}

static int hmu_open(struct inode *inode, struct file *file)
{
	struct hmu_device *pDevice;
	struct usb_interface *interface;
	int subminor;
	int result = 0;
	subminor = iminor(inode);

	if (!s_bRegistered)
	{
		GADGET_LOG( LOG_DBG, "Hmu is not registered\n");
		return -ENODEV;
	}
	interface = usb_find_interface(&hmu_driver, subminor);

	if (!interface)
	{
		GADGET_LOG( LOG_DBG, "[%s] Error! Can't Find Interface - Device: %d\n", hmu_name, subminor);
		result = -ENODEV;
		goto hmu_open_exit;
	}

	pDevice = usb_get_intfdata(interface);
	GADGET_LOG( LOG_DBG, "hmu_open, pDevice:%p\n", pDevice );

	if (!pDevice)
	{
		GADGET_LOG( LOG_DBG, "[%s] %s: ERROR: Can't Get Device From Interface\n", hmu_name, __FUNCTION__);
		result = -ENODEV;
		goto hmu_open_exit;
	}

	if (atomic_read(&pDevice->is_opened) != 0)
	{
		GADGET_LOG( LOG_DBG, "[%s] %s: ERROR: Already opened\n", hmu_name, __FUNCTION__);
		result = -EBUSY;
		goto hmu_open_exit;
	}

	ringReset( &s_SensorBuffer, s_RingBuffer, sizeof(s_RingBuffer) / sizeof(s_RingBuffer[0]));

	s_bForwardingEnabled = gbIsDfuMode ? false : true;
	result = hmu_start_transfer(pDevice);

hmu_open_exit:
	if( result==0 )
	{
		atomic_inc(&pDevice->is_opened);
		GADGET_LOG( LOG_DBG, "[%s] %s: OK.\n", hmu_name, __FUNCTION__);
	}
	else
	{
		GADGET_LOG( LOG_DBG, "[%s] %s: ERROR during open, error %d.\n", hmu_name, __FUNCTION__, result );
	}

	return result;
}


//------------------------------------------------------------------------------
static unsigned int hmu_poll(struct file *file, struct poll_table_struct * wait)
{
	struct hmu_device *pDevice = (struct hmu_device *)hmu_usb_get_intfdata(file_inode(file));
	unsigned int mask = 0;

	if(!pDevice)
	{
		return POLLERR;
	}

	poll_wait(file, &pDevice->read_wait, wait);

	if(pDevice->iso_packet_received)
	{
		pDevice->iso_packet_received = false;
		mask |= POLLIN | POLLRDNORM;
	}

	return mask;
}

//------------------------------------------------------------------------------
static void  hmu_send_iso_urb_complete(struct urb *urb)
{
	struct hmu_urb_context* urb_context;

	if (!urb || !urb->context) {
		return;
	}

	urb_context = (struct hmu_urb_context*)urb->context;

	switch(urb->status)
	{
	case 0:
		break;
	case -ECONNRESET:
	case -ENOENT:
	case -ESHUTDOWN:
		GADGET_LOG( LOG_DBG, "[%s] URB ERROR: %d\n", hmu_name, urb->status);
		break;
	default:
		GADGET_LOG( LOG_DBG, "[%s] URB ERROR Unknown %d\n", hmu_name, urb->status);
		break;
	}

	// The hmu writes are single shot, and not used often, so when done, release buffers and urb.
	kfree( urb->transfer_buffer );
	usb_free_urb( urb );
	kfree( urb_context );
}

#define MAGIC 0x40
#define HMU_ISO_OUT_PACKET_SIZE 0x20

static int hmu_write( struct file* file, const char *buffer, size_t count, loff_t* loff )
{
	// Writes should not happen very often, just for control. So we do not need to bother
	// about pooling the urb requests. Just build a new one every time.

	struct hmu_device *pDevice = (struct hmu_device *)hmu_usb_get_intfdata(file_inode(file));
	int error;
	struct hmu_urb_context* urb_context = NULL;

	if( count != HMU_ISO_OUT_PACKET_SIZE )
	{
		GADGET_LOG( LOG_DBG,  "Error: Bad packet length written to host, ignored. Got:%d, expected:%d\n", count, HMU_ISO_OUT_PACKET_SIZE );
		return -EINVAL;
	}
	if( buffer[1] != MAGIC )
		return -EINVAL;

	if( !pDevice || pDevice->is_running == false )
		return -ENODEV;

	urb_context = kzalloc( sizeof(struct hmu_urb_context), GFP_KERNEL );
	if( urb_context == NULL )
		return -ENOMEM;

	urb_context->iso_urb = usb_alloc_urb( 1, GFP_KERNEL );
	if( urb_context->iso_urb == NULL )
	{
		error = -ENOMEM;
		goto hmu_write_exit;
	}

	urb_context->iso_urb->transfer_buffer = kzalloc( count, GFP_KERNEL );
	if( urb_context->iso_urb->transfer_buffer == NULL )
	{
		error = -ENOMEM;
		goto hmu_write_exit;
	}

	urb_context->pDevice = pDevice;
	urb_context->iso_urb->dev = pDevice->udev;
	urb_context->iso_urb->context = urb_context;
	urb_context->iso_urb->pipe = usb_sndisocpipe(pDevice->udev, EP_ISO_OUT );
	urb_context->iso_urb->transfer_flags = URB_ISO_ASAP;
	urb_context->iso_urb->number_of_packets = 1;
	urb_context->iso_urb->transfer_buffer_length = count;
	urb_context->iso_urb->complete = hmu_send_iso_urb_complete;
	urb_context->iso_urb->interval = 1;
	urb_context->iso_urb->iso_frame_desc[0].offset = 0;
	urb_context->iso_urb->iso_frame_desc[0].length = count;

	error = copy_from_user( urb_context->iso_urb->transfer_buffer, buffer, count );
	if (error)
		goto hmu_write_exit;

	error = usb_submit_urb(urb_context->iso_urb, GFP_KERNEL);
	if (error)
	{
		GADGET_LOG( LOG_DBG, "[%s] %s ERROR: usb_submit_urb(), error:%d\n", hmu_name, __FUNCTION__, error );
		goto hmu_write_exit;
	}

	return count;

hmu_write_exit:
	if ( urb_context )
	{
		if( urb_context->iso_urb )
			usb_free_urb( urb_context->iso_urb );
		if( urb_context->iso_urb->transfer_buffer )
			kfree( urb_context->iso_urb->transfer_buffer );
		kfree( urb_context );
	}
	return error;
}

static int hmu_read( struct file* file, char *buffer, size_t count, loff_t* loff )
{
	int length;
	struct hmu_device *pDevice = (struct hmu_device *)hmu_usb_get_intfdata(file_inode(file));

	length = ringPopToUser( &s_SensorBuffer, buffer, count );
	// If buffer fully flushed, and no more read activity running,
	// then the device must have been detached, so return error.
	if( (length <= 0) && (!pDevice || (pDevice->is_running == false)) )
	{
		return -ENODEV;
	}

	return length;
}

static int hmu_release(struct inode *inode, struct file *file)
{
	struct hmu_device *pDevice = (struct hmu_device *)hmu_usb_get_intfdata(inode);
	if (pDevice == NULL)
	{
		GADGET_LOG( LOG_DBG,  "hmu_release, already disconnected\n" );
		return 0;
	}

	GADGET_LOG( LOG_DBG,  "hmu_release, pDevice:%p present:%d opened:%d\n", pDevice, pDevice->is_present, atomic_read(&pDevice->is_opened) );

	mutex_lock(&hmu_mutex);
	if (atomic_read(&pDevice->is_opened) == 1)
		hmu_stop_transfer(pDevice);
	atomic_dec(&pDevice->is_opened);
	mutex_unlock(&hmu_mutex);
	return 0;
}

//------------------------------------------------------------------------------
const static struct file_operations hmu_fops =
{
	.owner = THIS_MODULE,
	.open = hmu_open,
	.release = hmu_release,
	.poll = hmu_poll,
	.unlocked_ioctl = hmu_ioctl,
	.read =	hmu_read,
	.write = hmu_write,
};

static struct usb_class_driver hmu_class =
{
	.name = "hmusys0",
	.fops = &hmu_fops,
	.minor_base = USB_PSEYE_MINOR_BASE,
};

static bool is_iso_in(struct usb_interface *interface, uint32_t* maxpacket)
{
	int i, j;

	/* Find isoc_in endpoint */
	for (i = 0; i < interface->num_altsetting; i++)
	{
		struct usb_host_interface* desc = &interface->altsetting[i];
		for (j = 0; j < desc->desc.bNumEndpoints; j++)
		{
			struct usb_endpoint_descriptor* endpoint = &desc->endpoint[j].desc;
			if (usb_endpoint_is_isoc_in(endpoint))
			{
				if (maxpacket)
					*maxpacket = le16_to_cpu(endpoint->wMaxPacketSize);
				return true;
			}
		}
	}
	return false;
}

static bool is_iso_out(struct usb_interface *interface)
{
	int i, j;

	/* Find isoc_out endpoint */
	for (i = 0; i < interface->num_altsetting; i++)
	{
		struct usb_host_interface* desc = &interface->altsetting[i];
		for (j = 0; j < desc->desc.bNumEndpoints; j++)
		{
			struct usb_endpoint_descriptor* endpoint = &desc->endpoint[j].desc;
			if (usb_endpoint_is_isoc_out(endpoint))
			{
				return true;
			}
		}
	}
	return false;
}

//------------------------------------------------------------------------------
static int hmu_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
	int error = -ENOMEM;
	uint32_t iso_in_maxpacket;
	struct hmu_device *pDevice;

	if (is_iso_in(interface, &iso_in_maxpacket))
	{
		if (!(pDevice = kzalloc(sizeof(struct hmu_device), GFP_KERNEL)))
		{
			GADGET_LOG( LOG_DBG, "[%s] Error! kmalloc()\n", hmu_name);
			return -ENOMEM;
		}
		GADGET_LOG( LOG_DBG, "Device Probe.. pDevice=%p iso_in_maxpacket is %d\n", pDevice, iso_in_maxpacket);

		pDevice->is_present = true;
		pDevice->iso_in_maxpacket = iso_in_maxpacket;
		pDevice->udev = interface_to_usbdev(interface);
		init_waitqueue_head(&pDevice->read_wait);
		spin_lock_init(&pDevice->lock);
		usb_set_intfdata(interface, pDevice);

		/* Register Device */
		error = usb_register_dev(interface, &hmu_class);
		if (error)
		{
			GADGET_LOG( LOG_DBG, "[%s] %s ERROR: Device Register.\n", hmu_name, __FUNCTION__);
			usb_set_intfdata(interface, NULL);
			kfree(pDevice);
			return error;
		}
		return 0;
	}
	else if (is_iso_out(interface))
	{
		return 0;
	}
	return -ENODEV;
}

static void hmu_disconnect(struct usb_interface *interface)
{
	if (is_iso_in(interface, NULL))
	{
		struct hmu_device* pDevice;
		unsigned long flags;

		pDevice = usb_get_intfdata(interface);
		usb_set_intfdata(interface, NULL);

		ASSERT(pDevice != NULL);
		GADGET_LOG( LOG_DBG,  "hmu_disconnect, hmusys%d pDevice:%p present:%d opened:%d\n", interface->minor, pDevice, pDevice->is_present, atomic_read(&pDevice->is_opened) );

		/* Release Device Minor */
		usb_deregister_dev(interface, &hmu_class);

		mutex_lock(&hmu_mutex);
		spin_lock_irqsave(&pDevice->lock, flags);
		pDevice->is_present = false;
		spin_unlock_irqrestore(&pDevice->lock, flags);
		memset( pDevice, 0xff, sizeof(struct hmu_device) );
		kfree( pDevice );
		mutex_unlock(&hmu_mutex);

		GADGET_LOG( LOG_DBG, "Disconnected\n");
	}
}


static struct usb_driver hmu_driver =
{
	.name = hmu_name,
	.id_table = hmu_ids,
	.probe = hmu_probe,
	.disconnect = hmu_disconnect,
};

//------------------------------------------------------------------------------
static int hmu_read_proc( struct file* file, char *buffer, size_t count, loff_t* loff )
{
	int len;
	if( s_timetick )
	{
		len = snprintf( buffer, count,
			"low:%6d, high:%6d, avg:%6d - <1:%4d 2:%4d 3:%4d 4:%4d 5:%4d 6:%4d 7:%4d 8:%4d 9:%4d 10:%4d >10:%4d\n"
			"sent:%d, dropped:%d, no_urb:%d, total:%d\n",
			(int)s_timelow, (int)s_timehigh, (int)(s_timeaverage/s_timetick),
			s_intervals[0], s_intervals[1], s_intervals[2], s_intervals[3], s_intervals[4], s_intervals[5], s_intervals[6], s_intervals[7], s_intervals[8], s_intervals[9], s_intervals[10],
			s_uiSendOK, s_uiSendFailed, s_uiSendFailedNoURB, s_uiTotalReceived );
	}
	else
	{
		len = 0;
	}

	s_timelow = 100000000;
	s_timehigh = 0;
	s_timeaverage = 0;
	s_timetick = 0;
	memset( s_intervals,0,sizeof(s_intervals) );
	s_uiSendFailed = 0;
	s_uiSendFailedNoURB = 0;
	s_uiSendOK = 0;
	s_uiTotalReceived = 0;
	return len;
}

struct file_operations hmu_proc_fops = { read: hmu_read_proc };

//------------------------------------------------------------------------------
int hmu_init(void)
{
	int result;

#if defined(CREATE_PROC_ENTRY)
	ASSERT(s_ProcEntry == NULL);
	s_ProcEntry = proc_create( "hmusys", 0, NULL, &hmu_proc_fops );
	if (!s_ProcEntry)
	{
		GADGET_LOG( LOG_DBG, "[%s] %s: ERROR: Unable to create /proc entry\n", hmu_name, __FUNCTION__);
		return -ENOMEM;
	}
#endif

	ASSERT(s_bRegistered == false);
	result = usb_register(&hmu_driver);
	if (result == 0)
	{
		s_bRegistered = true;
		GADGET_LOG( LOG_DBG, "[%s] Module Init OK\n", hmu_name );
	}
	else
	{
		GADGET_LOG( LOG_DBG,  "Unable to register hmusys driver\n" );
	}

	return result;
}
module_init(hmu_init);

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void hmu_exit(void)
{
	if( s_bRegistered )
	{
		GADGET_LOG( LOG_DBG,  "hmu_cleanup\n" );
#if defined(CREATE_PROC_ENTRY)
		if( s_ProcEntry )
		{
			remove_proc_entry("hmusys", NULL);
			s_ProcEntry = NULL;
		}
#endif
		s_bRegistered = false;
		usb_deregister(&hmu_driver);
	}
}
module_exit(hmu_exit);

MODULE_AUTHOR("Sony Interactive Entertainment Inc.");
MODULE_LICENSE("GPL");
