/*
 * Copyright (C) 2016 Sony Interactive Entertainment Inc.
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  version 2 of the  License.
 *
 * THIS  SOFTWARE  IS PROVIDED   ``AS  IS AND   ANY  EXPRESS OR IMPLIED
 * WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 * NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 * USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * You should have received a copy of the  GNU General Public License along
 * with this program; if not, write  to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * Copyright (C) 2010 Fabien Chouteau <fabien.chouteau@barco.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/hid.h>

#include "bridge.h"
#include "sensor.h"
#include "hostdef.h"
#include "stat.h"

#define SENSOR_ENDPOINT_SOURCE_NUM_REQUESTS 2

static const char sensorReportDesc[] = {
	0x06, 0x01, 0xff, // Usage Page (Vendor-defined 0xFF01)
	0x09, 0x01, // Usage (Vendor-defined 0x0001)
	0xa1, 0x01, // Collection (Application)
	0x09, 0x10, // Usage (Vendor-defined 0x0010)
	0x15, 0x00,       //    Logical minimum (0)
	0x26, 0xff, 0x00, //    Logical maximum (255)
	0x75, 0x08,       //    Report Size (8)
	0x95, 0x40,       //    Report Count (64)
	0x81, 0x02,       //    Input
	0xc0 // End Collection
};

static struct usb_interface_descriptor hid_intf = {
	.bLength = sizeof(hid_intf),
	.bDescriptorType = USB_DT_INTERFACE,
	.bAlternateSetting = 0,
	.bNumEndpoints = 1,
	.bInterfaceClass = USB_CLASS_HID,
	.bInterfaceNumber = MORPHEUS_SENSOR_INTERFACE,
};

static const struct hid_descriptor hid_desc = {
	.bLength = sizeof(hid_desc),
	.bDescriptorType = HID_DT_HID,
	.bcdHID = 0x0111,
	.bCountryCode = 0x00,
	.bNumDescriptors = 0x1,
	.desc[0] = {
		.bDescriptorType = HID_DT_REPORT,
		.wDescriptorLength = cpu_to_le16(sizeof(sensorReportDesc)),
	}
};

static const struct usb_endpoint_descriptor fs_hid_source_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_SENSOR_ENDPOINT_SOURCE | USB_DIR_IN,
	.bmAttributes = USB_ENDPOINT_XFER_INT,
	.wMaxPacketSize = cpu_to_le16(64),
	.bInterval = 1, /* 1ms */
};

static const struct usb_endpoint_descriptor hs_hid_source_desc = {
	.bLength = USB_DT_ENDPOINT_SIZE,
	.bDescriptorType = USB_DT_ENDPOINT,
	.bEndpointAddress = MORPHEUS_SENSOR_ENDPOINT_SOURCE | USB_DIR_IN,
	.bmAttributes = USB_ENDPOINT_XFER_INT,
	.wMaxPacketSize = cpu_to_le16(64),
	.bInterval = 3, /* 500uS */
};

static struct usb_descriptor_header *fs_hid_descs[] = {
	(struct usb_descriptor_header *)&hid_intf,
	(struct usb_descriptor_header *)&hid_desc,
	(struct usb_descriptor_header *)&fs_hid_source_desc,
	NULL,
};

static struct usb_descriptor_header *hs_hid_descs[] = {
	(struct usb_descriptor_header *)&hid_intf,
	(struct usb_descriptor_header *)&hid_desc,
	(struct usb_descriptor_header *)&hs_hid_source_desc,
	NULL,
};

struct mbridge_hid_interface
{
	struct usb_function interface;

	struct usb_ep *source_ep;
	struct usb_request *requests[SENSOR_ENDPOINT_SOURCE_NUM_REQUESTS];
	unsigned long req_avail;
	unsigned long state;
#define _HID_READY  (0)
#define _EP_ENABLED (1)
};

static int mbridge_hid_bind(struct usb_configuration *c,
			    struct usb_function *f);
static int mbridge_hid_set_alt(struct usb_function *f, unsigned intf,
			       unsigned alt);
static void mbridge_hid_disable(struct usb_function *f);
static int mbridge_hid_setup(struct usb_function *f,
			     const struct usb_ctrlrequest *ctrl);

static struct mbridge_hid_interface sHid = {
	.interface = {
		.name = "hid",
		.fs_descriptors = fs_hid_descs,
		.hs_descriptors = hs_hid_descs,
		.bind = mbridge_hid_bind,
		.set_alt = mbridge_hid_set_alt,
		.disable = mbridge_hid_disable,
		.setup = mbridge_hid_setup,
	},
};
static struct mbridge_sensor_stat sStat = {0};

void mbridge_sensor_get_stat(struct mbridge_sensor_stat* stat)
{
	*stat = sStat;
}
EXPORT_SYMBOL(mbridge_sensor_get_stat);

int mbridge_hid_send(const void *buf, size_t buflen)
{
	int i, ret;

	if (SENSOR_ENDPOINT_SOURCE_REQUEST_SIZE < buflen) {
		sStat.send_error_inval++;
		return -EINVAL;
	}

	if (!test_bit(_HID_READY, &sHid.state)) {
		sStat.send_error_again++;
		return -EAGAIN;
	}

	// find first empty request
	for (i = 0; i < SENSOR_ENDPOINT_SOURCE_NUM_REQUESTS; i++)
		if (test_and_clear_bit(i, &sHid.req_avail))
			break;
	// see if requests run out of
	if (i == SENSOR_ENDPOINT_SOURCE_NUM_REQUESTS) {
		sStat.send_error_noent++;
		return -ENOENT;
	}

	memcpy(sHid.requests[i]->buf, buf, buflen);
	sHid.requests[i]->length = buflen;
	ret = usb_ep_queue(sHid.source_ep, sHid.requests[i], GFP_ATOMIC);
	if (ret)
		set_bit(i, &sHid.req_avail);
	else
		ret = 0;

	if (ret) {
		if (ret == -EINVAL)
			sStat.send_error_inval++;
		else if (ret == -ENODEV)
			sStat.send_error_nodev++;
		else if (ret == -EAGAIN)
			sStat.send_error_again++;
		else if (ret == -ENOENT)
			sStat.send_error_noent++;
		else
			sStat.send_error_others++;
	} else {
		sStat.request_avails = sHid.req_avail;
		sStat.sent++;
	}
	return 0;
}
EXPORT_SYMBOL(mbridge_hid_send);

static void mbridge_hid_complete(struct usb_ep *ep, struct usb_request *req)
{
	unsigned int ix;

	ix = (unsigned int)req->context;
	switch (req->status) {
	case 0:
		if (req->actual == 0)
			sStat.complete_zero++;
		else
			sStat.complete++;
		set_bit(ix, &sHid.req_avail);
		break;
	default:
		sStat.complete_error++;
		set_bit(ix, &sHid.req_avail);
		break;
	case -ECONNRESET: /* request dequeued */
	case -ESHUTDOWN: /* disconnect from host */
		sStat.complete_error++;
		if (req->buf) {
			kfree(req->buf);
			req->buf = NULL;
		}
		clear_bit(ix, &sHid.req_avail);
		sHid.requests[ix] = NULL;
		usb_ep_free_request(ep, req);
		break;
	}
	sStat.request_avails = sHid.req_avail;
}

static int mbridge_hid_bind(struct usb_configuration *c, struct usb_function *f)
{
	struct mbridge_hid_interface *hid =
		container_of(f, struct mbridge_hid_interface, interface);

	/* Configure endpoint from the descriptor. */
	hid->source_ep = usb_ep_autoconfig(c->cdev->gadget,
					   (struct usb_endpoint_descriptor *)
					   &fs_hid_source_desc);
	if (!hid->source_ep) {
		pr_err("%s:can't autoconfigure sensor endpoint\n", __func__);
		return -ENODEV;
	}

	BUG_ON((hid->source_ep->address & USB_ENDPOINT_NUMBER_MASK) !=
	       MORPHEUS_SENSOR_ENDPOINT_SOURCE);

	/* Need to set this during setup only. */
	hid->source_ep->driver_data = c->cdev;

	usb_assign_descriptors(f, fs_hid_descs, hs_hid_descs, NULL);

	return 0;
}

static int mbridge_hid_enable(struct usb_function *f)
{
	struct mbridge_hid_interface *hid =
		container_of(f, struct mbridge_hid_interface, interface);
	int ret, i;

	/* Enable the endpoint. */
	ret = config_ep_by_speed(f->config->cdev->gadget, f, hid->source_ep);
	if (ret) {
		pr_err("%s: fail to configure EP (%d)\n", __func__, ret);
		return ret;
	}
	ret = usb_ep_enable(hid->source_ep);
	if (ret) {
		pr_err("%s: fail to enable EP (%d)\n", __func__, ret);
		return ret;
	}
	set_bit(_EP_ENABLED, &hid->state);

	BUILD_BUG_ON((sizeof(sHid.req_avail) * 8) < SENSOR_ENDPOINT_SOURCE_NUM_REQUESTS);

	// Prepare requests
	for (i = 0; i < SENSOR_ENDPOINT_SOURCE_NUM_REQUESTS; i++) {
		hid->requests[i] = usb_ep_alloc_request(hid->source_ep, GFP_ATOMIC);
		if (!hid->requests[i]) {
			pr_info("%s: failed to allocate EP \n", __func__);
			usb_ep_disable(hid->source_ep);
			clear_bit(_EP_ENABLED, &hid->state);
			goto rewind;
		}

		hid->requests[i]->buf = kmalloc(SENSOR_ENDPOINT_SOURCE_REQUEST_SIZE,
						GFP_ATOMIC);
		if (!hid->requests[i]->buf) {
			pr_info("%s: faile to allocate EP buffer\n", __func__);
			goto rewind;
		}
		set_bit(i, &hid->req_avail);
		// remember index
		hid->requests[i]->context = (void *)i;
		hid->requests[i]->complete = mbridge_hid_complete;
	}

	set_bit(_HID_READY, &hid->state);
	sStat.request_avails = sHid.req_avail;

	return 0;
rewind:
	for (i = 0; i < SENSOR_ENDPOINT_SOURCE_NUM_REQUESTS; i++) {
		if (!hid->requests[i])
			continue;

		clear_bit(i, &hid->req_avail);
		if (hid->requests[i]->buf)
			kfree(hid->requests[i]->buf);

		usb_ep_free_request(hid->source_ep, hid->requests[i]);
		hid->requests[i] = NULL;
	}
	sStat.request_avails = sHid.req_avail;
	return -ENOMEM;
} // mbridge_hid_enable

static void mbridge_hid_disable(struct usb_function *f)
{
	struct mbridge_hid_interface *hid =
		container_of(f, struct mbridge_hid_interface, interface);
	int i;

	clear_bit(_HID_READY, &hid->state);
	if (test_and_clear_bit(_EP_ENABLED, &hid->state))
		usb_ep_disable(hid->source_ep);

	for (i = 0; i < SENSOR_ENDPOINT_SOURCE_NUM_REQUESTS; i++) {
		if (!hid->requests[i])
			continue;

		clear_bit(i, &hid->req_avail);
		if (hid->requests[i]->buf)
			kfree(hid->requests[i]->buf);

		usb_ep_free_request(hid->source_ep, hid->requests[i]);
		hid->requests[i] = NULL;
	}
	sStat.request_avails = sHid.req_avail;
}

static int mbridge_hid_set_alt(struct usb_function *f, unsigned intf, unsigned alt)
{
	mbridge_hid_disable(f);
	return mbridge_hid_enable(f);
}

static int mbridge_hid_setup(struct usb_function *f, const struct usb_ctrlrequest *ctrl)
{
	struct usb_composite_dev *cdev = f->config->cdev;
	struct usb_request *req = cdev->req;
	__u16 value, length;

	value = __le16_to_cpu(ctrl->wValue);
	length = __le16_to_cpu(ctrl->wLength);

	switch ((ctrl->bRequestType << 8) | ctrl->bRequest) {
	case ((USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8 |
	      HID_REQ_GET_REPORT):
		/* send empty report */
		length = 0;
		break;
	case ((USB_DIR_IN | USB_TYPE_STANDARD | USB_RECIP_INTERFACE) << 8 |
	      USB_REQ_GET_DESCRIPTOR):
		switch (value >> 8) {
		case HID_DT_HID:
			length = min_t(__u16, length, hid_desc.bLength);
			memcpy(req->buf, &hid_desc, length);
			break;
		case HID_DT_REPORT:
			length = min_t(__u16, length, sizeof(sensorReportDesc));
			memcpy(req->buf, sensorReportDesc, length);
			break;
		default:
			goto stall;
			break;
		}
		break;
	case ((USB_DIR_IN | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8 |
	      HID_REQ_GET_PROTOCOL):
	case ((USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8 |
	      HID_REQ_SET_REPORT):
	case ((USB_DIR_OUT | USB_TYPE_CLASS | USB_RECIP_INTERFACE) << 8 |
	      HID_REQ_SET_PROTOCOL):
		goto stall;
		break;
	default:
		pr_info("%s:Unknown request 0x%x\n", __func__, ctrl->bRequest);
		goto stall;
		break;
	}

	req->zero = 0;
	req->length = length;
	return usb_ep_queue(cdev->gadget->ep0, req, GFP_ATOMIC);

stall:
	return -EOPNOTSUPP;
}

int mbridge_hid_is_ready(void)
{
	if (test_bit(_HID_READY, &sHid.state))
		return 1;
	else
		return 0;
}
EXPORT_SYMBOL(mbridge_hid_is_ready);

int mbridge_hid_init(struct usb_composite_dev *cdev, struct usb_function **interface)
{
	int id;

	id = usb_string_id(cdev);
	if (id < 0)
		return id;

	strings_dev[STRING_SENSOR_IDX].id = id;
	hid_intf.iInterface = id;

	*interface = &sHid.interface;
	return 0;
}

void mbridge_hid_destroy(struct usb_function *f)
{
	clear_bit(_HID_READY, &sHid.state);
}
